<?php
/*
Plugin Name: Custom New User Email
Description: Changes the copy in the email sent out to new users
*/

// Redefine user notification function
if ( !function_exists('wp_new_user_notification') ) {
    function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
        $user = get_userdata( $user_id );

        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        $headers = array(
            'From: ' . $blogname . ' <' . get_option('admin_email') . '>',
            'Content-type: text/html'
        );

        /*$message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n";
        $message .= sprintf(__('Name: %s'), $user->first_name . ' ' . $user->last_name) . "\r\n";
        $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";
        $message .= sprintf(__('Company: %s'), get_user_meta($user_id, 'company', true)) . "\r\n";
        $message .= sprintf(__('Phone: %s'), get_user_meta($user_id, 'phone', true)) . "\r\n";*/
        
        //$message = '';
        $admin_email = get_option('admin_email');
        $message  = 'New user registration on your site '. $blogname .'<br/>Here are details: <br/><br/>Username: ' . $user->user_login . '<br/>' . 'Name: ' . $user->first_name . ' ' . $user->last_name . '<br/>Email: ' . $user->user_email . '<br/>Company: ' . get_user_meta($user_id, 'company', true) . '<br/>Phone: ' . get_user_meta($user_id, 'phone', true);        
        @wp_mail($admin_email, 'New User Registration', $message, $headers);


        if ( empty($plaintext_pass) ) {
            return;
        }

        $message  = /*__(*/'Thank you for registering an account with us, here are you log in details: <br/><br/>Username: ' . $user->user_login . '<br/>' . 'Password: ' . $plaintext_pass . '<br/><br/>To access your account please visit <a href="http://www.modularmanagement.com">modularmanagement.com</a>, and if you have any questions please do not hesitate to contact us on <a href="mailto:info@modularmanagement.com">info@modularmanagement.com</a><br/><br/>Best regards, <br/>Modular Management';

        /*$message .= __('Username: ' . $user->user_login) . "\r\n";
        $message .= __('Password: ' . $plaintext_pass) . "\r\n\r\n";*/

        /*$message .= __('To access your account please visit <a href="http://www.modularmanagement.com">modularmanagement.com</a>, and if you have any questions please do not hesitate to contact us on <a href="mailto:info@modularmanagement.com">info@modularmanagement.com</a>');

        $message .= __('Best regards,');
        $message .= __('Modular Management');*/

        wp_mail($user->user_email, __('Welcome to Modular Management!'), $message, $headers);
    }
}

?>
