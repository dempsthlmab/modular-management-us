<?php
/*
Plugin Name: 404page - your smart custom 404 error page
Plugin URI: http://smartware.cc/free-wordpress-plugins/404page/
Description: Custom 404 the easy way! Set any page as custom 404 error page. No coding needed. Works with (almost) every Theme.
Version: 2.1
Author: smartware.cc, Peter's Plugins
Author URI: http://smartware.cc
Text Domain: 404page
License: GPL2
*/

/*  Copyright 2016 Peter Raschendorfer (email : sw@smartware.cc)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! defined( 'WPINC' ) ) {
	die;
}

class Smart404Page {
  public $plugin_name;
  public $plugin_slug;
  public $version;
  private $wp_url;
  private $my_url;
  private $dc_url;
  public $settings;
  private $template;
  private $postid;
  
	public function __construct() {
		$this->plugin_name = '404page';
    $this->plugin_slug = '404page';
		$this->version = '2.1';
    $this->get_settings();
    $this->init();
	} 
  
  // get all settings
  private function get_settings() {
    $this->settings = array();
    $this->settings['404page_page_id'] = $this->get_404page_id();
    $this->settings['404page_hide'] = $this->get_404page_hide();
    $this->settings['404page_fire_error'] = $this->get_404page_fire_error();
    $this->settings['404page_method'] = $this->get_404page_method();
  }
 
  // do plugin init
  private function init() {
    
    register_activation_hook( __FILE__, array( $this, 'init_admin_notice' ) );
    
    if ( !is_admin() ) {
      
      add_action( 'init', array( $this, 'set_mode' ) );
      
    } else {
      
      add_action( 'admin_init', array( $this, 'admin_init' ) );
      add_action( 'admin_menu', array( $this, 'admin_menu' ) );
      add_action( 'admin_head', array( $this, 'admin_css' ) );
      add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'add_settings_link' ) ); 
      
      if ( $this->settings['404page_hide'] and $this->settings['404page_page_id'] > 0 ) {
        add_action( 'pre_get_posts' ,array ( $this, 'exclude_404page' ) );
      }
      
      if ( 'NOT_DISMISSED' == get_option( '404page_notice_dismissed', 'NOT_DISMISSED' ) )  {
        add_action( 'admin_notices', array( $this, 'admin_notice' ) );
        add_action( 'wp_ajax_nopriv_404page_dismiss_admin_notice', array( $this, 'dismiss_admin_notice' ) );
        add_action( 'wp_ajax_404page_dismiss_admin_notice', array( $this, 'dismiss_admin_notice' ) );
      }
    }
    
  }
  
  // init filters 
  function set_mode() {
    
    if ( defined( 'CUSTOMIZR_VER' ) ) {
      
      // Customizr Compatibility Mode 
     
      add_filter( 'tc_404_header_content', array( $this, 'show404title_customizr_mode' ), 999 );
      add_filter( 'tc_404_content', array( $this, 'show404_customizr_mode' ), 999 );
      add_filter( 'tc_404_selectors', array( $this, 'show404articleselectors_customizr_mode' ), 999 );
      
    } else {
      
      if ( $this->settings['404page_method'] != 'STD' ) {
        
        // Compatibility Mode
        add_filter( 'posts_results', array( $this, 'show404_compatiblity_mode' ), 999 );
        
      } else {
        
        // Standard Mode
        add_filter( '404_template', array( $this, 'show404_standard_mode' ), 999 );
        if ( $this->settings['404page_fire_error'] ) {
          add_action( 'template_redirect', array( $this, 'do_404_header_standard_mode' ) );
        }
        
      }
    }
    
  }
  
  // show 404 page - Standard Mode
  function show404_standard_mode( $template ) {
    
    global $wp_query;
    $pageid = $this->settings['404page_page_id'];
    if ( $pageid > 0 ) {
      $wp_query = null;
      $wp_query = new WP_Query();
      $wp_query->query( 'page_id=' . $pageid );
      $wp_query->the_post();
      $template = get_page_template();
      rewind_posts();
      add_filter( 'body_class', array( $this, 'add_404_body_class' ) );
      $this->do_404page_action();
    }
    return $template;
    
  }
  
  // show 404 page - Compatibility Mode
  function show404_compatiblity_mode( $posts ) {
    
    // remove the filter so we handle only the first query - no custom queries
    remove_filter( 'posts_results', array( $this, 'show404_compatiblity_mode' ), 999 ); 
    
    $pageid = $this->settings['404page_page_id'];
    if ( 0 != $pageid ) {
      if ( empty( $posts ) && is_main_query() && !is_robots() && !is_home() && !is_feed() && !is_search() &&( !defined('DOING_AJAX') || !DOING_AJAX ) ) {
        // we need to get the 404 page
        
        if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
          
          // WPML is active
          $pageid = apply_filters( 'wpml_object_id', $pageid, 'page', true );
          
        } elseif ( defined( 'POLYLANG_VERSION' ) ) {
          
          // Polylang is active
          $translatedpageid = pll_get_post( $pageid );
          if ( !empty( $translatedpageid ) && 'publish' == get_post_status( $translatedpageid ) ) {
            $pageid = $translatedpageid;
          }
        }
        
        // as of v2.1 we do not alter the posts argument here because this does not work with SiteOrigin's Page Builder Plugin, template_include filter introduced
        $this->postid = $pageid;
        $this->template = get_page_template_slug( $pageid );
        if ( $this->template == '' ) {
          $this->template = get_page_template();
        }
        add_action( 'wp', array( $this, 'do_404_header' ) );
        add_filter( 'body_class', array( $this, 'add_404_body_class' ) );
        
        $posts[] = get_post( $pageid );
        
        $this->do_404page_action();
        
      } elseif ( 1 == count( $posts ) && 'page' == $posts[0]->post_type ) {
        
        // Do a 404 if the 404 page is opened directly
        if ( $this->settings['404page_fire_error'] ) {
          $curpageid = $posts[0]->ID;
          
          if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
           
           // WPML is active - get the post ID of the default language
            global $sitepress;
            $curpageid = apply_filters( 'wpml_object_id', $curpageid, 'page', $sitepress->get_default_language() );
            $pageid = apply_filters( 'wpml_object_id', $pageid, 'page', $sitepress->get_default_language() );
            
          } elseif ( defined( 'POLYLANG_VERSION' ) ) {
            
            // Polylang is active - get the post ID of the default language
            $curpageid = pll_get_post( $curpageid, pll_default_language() );
            $pageid = pll_get_post( $pageid, pll_default_language() );
          
          }
          
          if ( $pageid == $curpageid ) {
            add_action( 'wp', array( $this, 'do_404_header' ) );
            add_filter( 'body_class', array( $this, 'add_404_body_class' ) );
            $this->do_404page_action();
          }
        }
        
      }
    }
    return $posts;
  }
  
  // send a 404 HTTP header - Standard Mode
  function do_404_header_standard_mode() {
    if ( is_page() && get_the_ID() == $this->settings['404page_page_id'] && !is_404() ) {
      status_header( 404 );
      nocache_headers();
      $this->do_404page_action();
    }
  }
  
  // send a 404 HTTP header - Compatibility Mode
  function do_404_header() {
    // remove the action so we handle only the first query - no custom queries
    remove_action( 'wp', array( $this, 'do_404_header' ) );
    status_header( 404 );
    nocache_headers();
  }
  
  // adds the error404 class to the body classes  
  function add_404_body_class( $classes ) {
    $classes[] = 'error404';
    return $classes;
  }
  
  // show title - Customizr Compatibility Mode
  function show404title_customizr_mode( $title ) {
    return '<h1 class="entry-title">' . get_the_title( $this->settings['404page_page_id'] ) . '</h1>';
  }
  
  // show content - Customizr Compatibility Mode
  function show404_customizr_mode( $content ) {
    return '<div class="entry-content">' . apply_filters( 'the_content', get_post_field( 'post_content', $this->settings['404page_page_id'] ) ) . '</div>';
    $this->do_404page_action();
  }
  
  // change article selectors - Customizr Compatibility Mode
  function show404articleselectors_customizr_mode( $selectors ) {
    return 'id="post-' . $this->settings['404page_page_id'] . '" ' . 'class="' . join( ' ', get_post_class( 'row-fluid', $this->settings['404page_page_id'] ) ) . '"';
  }
  
  // init the admin section
  function admin_init() {
    $this->wp_url = 'https://wordpress.org/plugins/' . $this->plugin_slug;
    $this->my_url = 'http://smartware.cc/free-wordpress-plugins/' . $this->plugin_slug;
    $this->dc_url = 'http://smartware.cc/docs/' . $this->plugin_slug;
    load_plugin_textdomain( '404page' );
    add_settings_section( '404page-settings', null, null, '404page_settings_section' );
    register_setting( '404page_settings', '404page_page_id' );
    register_setting( '404page_settings', '404page_hide' );
    register_setting( '404page_settings', '404page_method' );
    register_setting( '404page_settings', '404page_fire_error' );
    add_settings_field( '404page_settings_404page', __( 'Page to be displayed as 404 page', '404page' ) . '&nbsp;<a class="dashicons dashicons-editor-help" href="' . $this->dc_url . '/#settings_select_page"></a>' , array( $this, 'admin_404page' ), '404page_settings_section', '404page-settings', array( 'label_for' => '404page_page_id' ) );
    add_settings_field( '404page_settings_hide', __( 'Hide 404 page', '404page' ) . '&nbsp;<a class="dashicons dashicons-editor-help" href="' . $this->dc_url . '/#settings_hide_page"></a>' , array( $this, 'admin_hide' ), '404page_settings_section', '404page-settings', array( 'label_for' => '404page_hide' ) );
    add_settings_field( '404page_settings_fire', __( 'Fire 404 error', '404page' ) . '&nbsp;<a class="dashicons dashicons-editor-help" href="' . $this->dc_url . '/#settings_fire_404"></a>' , array( $this, 'admin_fire404' ), '404page_settings_section', '404page-settings', array( 'label_for' => '404page_fire_error' ) );
    add_settings_field( '404page_settings_method', __( 'Operating Method', '404page' ) . '&nbsp;<a class="dashicons dashicons-editor-help" href="' . $this->dc_url . '/#settings_operating_method"></a>' , array( $this, 'admin_method' ), '404page_settings_section', '404page-settings', array( 'label_for' => '404page_method' ) );
  }
  
  // add css
  function admin_css() {
    echo '<style type="text/css">#select404page {width: 100%; }';
    if ( $this->settings['404page_page_id'] > 0 ) {
      echo ' #the-list #post-' . $this->settings['404page_page_id'] . ' .column-title {min-height: 32px; background-position: left top; background-repeat: no-repeat; background-image: url(' . plugins_url( 'pluginicon.png', __FILE__ ) . '); padding-left: 40px;}';
    }
    echo '</style>';
  }
  
  // handle the settings field page id
  function admin_404page() {
    if ( $this->settings['404page_page_id'] < 0 ) {
      echo '<div class="error form-invalid" style="line-height: 3em">' . __( 'The page you have selected as 404 page does not exist anymore. Please choose another page.', '404page' ) . '</div>';
    }
    wp_dropdown_pages( array( 'name' => '404page_page_id', 'id' => 'select404page', 'echo' => 1, 'show_option_none' => __( '&mdash; NONE (WP default 404 page) &mdash;', '404page'), 'option_none_value' => '0', 'selected' => $this->settings['404page_page_id'] ) );
    echo '<div id="404page_edit_link" style="display: none">' . get_edit_post_link( $this->get_404page_id() )  . '</div>';
    echo '<div id="404page_test_link" style="display: none">' . get_site_url() . '/' . md5( rand() ) . '/' . md5( rand() ) . '/' . md5( rand() ) . '</div>';
    echo '<div id="404page_current_value" style="display: none">' . $this->get_404page_id() . '</div>';
    echo '<p class="submit"><input type="button" name="edit_404_page" id="edit_404_page" class="button secondary" value="' . __( 'Edit Page', '404page' ) . '" />&nbsp;<input type="button" name="test_404_page" id="test_404_page" class="button secondary" value="' . __( 'Test 404 error', '404page' ) . '" /></p>';
  }
  
  // handle the settings field hide
  function admin_hide() {
    echo '<p><input type="checkbox" id="404page_hide" name="404page_hide" value="1"' . checked( true, $this->settings['404page_hide'], false ) . '/>';
    echo '<label for="404page_hide">' . __( 'Hide the selected page from the Pages list', '404page' ) . '</label></p>';
    echo '<p><span class="dashicons dashicons-info"></span>&nbsp;' . __( 'For Administrators the page is always visible.', '404page' ) . '</p>';
  }
  
  // handle the settings field fire 404 error
  function admin_fire404() {
    echo '<p><input type="checkbox" id="404page_fire_error" name="404page_fire_error" value="1"' . checked( true, $this->settings['404page_fire_error'], false ) . '/>';
    echo '<label for="404page_fire_error">' . __( 'Send an 404 error if the page is accessed directly by its URL', '404page' ) . '</label></p>';
    echo '<p><span class="dashicons dashicons-info"></span>&nbsp;' . __( 'Uncheck this if you want the selected page to be accessible.', '404page' ) . '</p>';
    if ( function_exists( 'wpsupercache_activate' ) ) {
      echo '<p><span class="dashicons dashicons-warning"></span>&nbsp;<strong>' . __( 'WP Super Cache Plugin detected', '404page' ) . '</strong>. ' . __ ( 'If the page you selected as 404 error page is in cache, always a HTTP code 200 is sent. To avoid this and send a HTTP code 404 you have to exlcude this page from caching', '404page' ) . ' (<a href="' . admin_url( 'options-general.php?page=wpsupercache&tab=settings#rejecturi' ) . '">' . __( 'Click here', '404page' ) . '</a>).<br />(<a href="' . $this->dc_url . '/#wp_super_cache">' . __( 'Read more', '404page' ) . '</a>)</p>';
    }
  }
  
  // handle the settings field method
  function admin_method() {
    if ( defined( 'CUSTOMIZR_VER' ) || defined( 'ICL_SITEPRESS_VERSION' ) || defined( 'POLYLANG_VERSION' ) || defined( 'SITEORIGIN_PANELS_VERSION' ) || class_exists( 'bbPress' ) ) {
      $dis = ' disabled="disabled"';
    } else {
      $dis = '';
    }
    echo '<p><input type="radio" id="404page_settings_method_standard" name="404page_method" value="STD"' . checked( 'STD', $this->settings['404page_method'], false ) . $dis . ' />';
    echo '<label for="404page_settings_method_standard">' . __( 'Standard Mode', '404page' ) . '</label></p>';
     
    echo '<p><input type="radio" id="404page_settings_method_compatibility" name="404page_method" value="CMP"' . checked( 'CMP', $this->settings['404page_method'], false ) . $dis . '/>';
    echo '<label for="404page_settings_method_compatibility">' . __( 'Compatibility Mode', '404page' ) . '</label></p>';
    
    if ( defined( 'CUSTOMIZR_VER' ) ) {
      echo '<p><span class="dashicons dashicons-warning"></span>&nbsp;' . __( 'Customizr Theme detected. The 404page Plugin works in Customizr Compatibility Mode.', '404page' ) . ' (<a href="' . $this->dc_url . '/#customizr_mode">' . __( 'Read more', '404page' ) . '</a>)</p>';
    } elseif ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
      echo '<p><span class="dashicons dashicons-warning"></span>&nbsp;' . __( 'WPML Plugin detected. Operating Method set to Compatibility Mode automatically.', '404page' ) . ' (<a href="' . $this->dc_url . '/#compatibilty_mode">' . __( 'Read more', '404page' ) . '</a>)</p>';
    } elseif ( defined( 'POLYLANG_VERSION' ) ) {
      echo '<p><span class="dashicons dashicons-warning"></span>&nbsp;' . __( 'Polylang Plugin detected. Operating Method set to Compatibility Mode automatically.', '404page' ) . ' (<a href="' . $this->dc_url . '/#compatibilty_mode">' . __( 'Read more', '404page' ) . '</a>)</p>';
    } elseif ( defined( 'SITEORIGIN_PANELS_VERSION' ) ) {
      echo '<p><span class="dashicons dashicons-warning"></span>&nbsp;' . __( 'Page Builder by SiteOrigin Plugin detected. Operating Method set to Compatibility Mode automatically.', '404page' ) . ' (<a href="' . $this->dc_url . '/#compatibilty_mode">' . __( 'Read more', '404page' ) . '</a>)</p>';
    } elseif ( class_exists( 'bbPress' ) ) {
      echo '<p><span class="dashicons dashicons-warning"></span>&nbsp;' . __( 'bbPress Plugin detected. Operating Method set to Compatibility Mode automatically.', '404page' ) . ' (<a href="' . $this->dc_url . '/#compatibilty_mode">' . __( 'Read more', '404page' ) . '</a>)</p>';
    } else {
      echo '<p><span class="dashicons dashicons-info"></span>&nbsp;' . __( 'Standard Mode uses the WordPress Template System and should work in most cases. If the 404page plugin does not work properly, probably you are using a theme or plugin that modifies the WordPress Template System. In this case the Compatibility Mode maybe can fix the problem, although it cannot be guaranteed that every possible configuration can be handled by Compatibility Mode. Standard Mode is the recommended method, only switch to Compatibility Mode if you have any problems.', '404page' ) . '</p>';
    }
  }
  
  // this function hides the selected page from the list of pages 
  function exclude_404page( $query ) {
    global $pagenow;
    if( 'edit.php' == $pagenow && ( get_query_var( 'post_type' ) && 'page' == get_query_var( 'post_type' ) ) && !current_user_can( 'create_users' ) ) {
      $query->set( 'post__not_in', array( $this->settings['404page_page_id'] ) );
    }
    return $query;
  }
  
  // adds the options page to admin menu
  function admin_menu() {
    $page_handle = add_theme_page ( __( '404 Error Page', "404page" ), __( '404 Error Page', '404page' ), 'manage_options', '404pagesettings', array( $this, 'admin_page' ) );
    add_action( 'admin_print_scripts', array( $this, 'admin_js' ) );
  }
  
  // adds javascript to the 404page settings page
  function admin_js() {
    wp_enqueue_script( '404pagejs', plugins_url( '/404page.js', __FILE__ ), 'jquery', $this->version, true );
  }
 
  // creates the options page
  function admin_page() {
    if ( !current_user_can( 'manage_options' ) )  {
      wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    ?>    
    <div class="wrap">
      <?php screen_icon(); ?>
      <h2 style="min-height: 32px; line-height: 32px; padding-left: 40px; background-image: url(<?php echo plugins_url( 'pluginicon.png', __FILE__ ); ?>); background-repeat: no-repeat; background-position: left center"><a href="' . $this->my_url . '">404page</a> <?php echo __( 'Settings', '404page' ); ?></h2>
      <?php settings_errors(); ?>
      <hr />
      <p>Plugin Version: <?php echo $this->version; ?> <a class="dashicons dashicons-editor-help" href="<?php echo $this->wp_url; ?>/changelog/"></a></p>
      <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
          <div id="post-body-content">
            <div class="meta-box-sortables ui-sortable">
              <form method="post" action="options.php">
                <div class="postbox">
                  <div class="inside">
                    <?php 
                      settings_fields( '404page_settings' );
                      do_settings_sections( '404page_settings_section' ); 
                      submit_button(); 
                    ?>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <?php { $this->show_meta_boxes(); } ?>
        </div>
        <br class="clear">
      </div>    
    </div>
    <?php
  }
  
  // show a dismissible notice
  function admin_notice() {
    if ( current_user_can( 'manage_options' ) ) {
      echo '<div class="notice notice-info is-dismissible" id="404page_admin_notice"><p><strong>404page Plugin:</strong> ';
      if ( defined( 'CUSTOMIZR_VER' ) ) {
        // Customizr Theme
        _e( 'Customizr Theme detected. The 404page Plugin works in Customizr Compatibility Mode.', '404page' );
      } elseif ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
        // WPML
        _e( 'WPML Plugin detected. Operating Method is set to Compatibility Mode automatically to make the 404page Plugin work with WPML.', '404page' );
      } elseif ( defined( 'POLYLANG_VERSION' ) ) {
        // Polylang
        _e( 'Polylang Plugin detected. Operating Method is set to Compatibility Mode automatically to make the 404page Plugin work with Polylang.', '404page' );
      } elseif ( defined( 'SITEORIGIN_PANELS_VERSION' ) ) {
        // Page Builder by SiteOrigin
        _e( 'Page Builder by SiteOrigin Plugin detected. Operating Method is set to Compatibility Mode automatically to make the 404page Plugin work with Page Builder by SiteOrigin.', '404page' );
      } elseif ( class_exists( 'bbPress' ) ) {
        // bbPress
        _e( 'bbPress Plugin detected. Operating Method is set to Compatibility Mode automatically to make the 404page Plugin work with bbPress.', '404page' );
      } elseif ( function_exists( 'wpsupercache_activate' ) ) {
        // WP Super Cache
        _e( 'WP Super Cache Plugin detected. Please check Settings!', '404page' );
      } else {
        _e( 'Please check Operating Method.', '404page' );
      }
      echo '<br /><a href="' . admin_url( 'themes.php?page=404pagesettings' ) . '">' . __( 'Settings', '404page' ) . '</a>';
      echo '</p></div>';
    }
  }
  
  // dismiss the notice (AJAX)
  function dismiss_admin_notice() {
    update_option( '404page_notice_dismissed', 'DISMISSED' );
  }
  
  // Reset dismiss state for notice on plugin activation
  function init_admin_notice( $network_wide ) {
    global $wpdb;
    if ( is_multisite() && $network_wide ) {
      $current_blog = $wpdb->blogid;
      $blog_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
      foreach ( $blog_ids as $blog_id ) {
        switch_to_blog( $blog_id );
        delete_option( '404page_notice_dismissed' );
      }
      switch_to_blog( $current_blog );
    } else {
      delete_option( '404page_notice_dismissed' );
    }
  }
  
  // returns the id of the 404 page if one is defined, returns 0 if none is defined, returns -1 if the defined page id does not exist
  private function get_404page_id() {  
    $pageid = get_option( '404page_page_id', 0 );
    if ( $pageid != 0 ) {
      $page = get_post( $pageid );
      if ( !$page || $page->post_status != 'publish' ) {
        $pageid = -1;
      }
    }
    return $pageid;
  }
  
  // returns the selected method
  private function get_404page_method() {
    if ( defined( 'ICL_SITEPRESS_VERSION' ) || defined( 'POLYLANG_VERSION' ) || defined( 'SITEORIGIN_PANELS_VERSION' ) || class_exists( 'bbPress' ) ) {
      // WPML or bbPress is active
      return 'CMP';
    } else {
      return get_option( '404page_method', 'STD' );
    }
  }
  
  // should we hide the selected 404 page from the page list?
  private function get_404page_hide() {
    return (bool)get_option( '404page_hide', false );
  }
  
  // should we fire an 404 error if the selected page is accessed directly?
  private function get_404page_fire_error() {
    return (bool)get_option( '404page_fire_error', true );
  }
  
  // make plugin expandable
  function do_404page_action() {
    do_action( '404page_after_404' );
  }
  
  // show meta boxes
  function show_meta_boxes() {
    ?>
    <div id="postbox-container-1" class="postbox-container">
      <div class="meta-box-sortables">
        <div class="postbox">
          <h3><span><?php _e( 'Like this Plugin?', '404page' ); ?></span></h3>
          <div class="inside">
            <ul>
              <li><div class="dashicons dashicons-wordpress"></div>&nbsp;&nbsp;<a href="<?php echo $this->wp_url; ?>/"><?php _e( 'Please rate the plugin', '404page' ); ?></a></li>
              <li><div class="dashicons dashicons-admin-home"></div>&nbsp;&nbsp;<a href="<?php echo $this->my_url; ?>/"><?php _e( 'Plugin homepage', '404page'); ?></a></li>
              <li><div class="dashicons dashicons-admin-home"></div>&nbsp;&nbsp;<a href="http://smartware.cc/"><?php _e( 'Author homepage', '404page' );?></a></li>
              <li><div class="dashicons dashicons-googleplus"></div>&nbsp;&nbsp;<a href="http://g.smartware.cc/"><?php _e( 'Authors Google+ Page', '404page' ); ?></a></li>
              <li><div class="dashicons dashicons-facebook-alt"></div>&nbsp;&nbsp;<a href="http://f.smartware.cc/"><?php _e( 'Authors facebook Page', '404page' ); ?></a></li>
            </ul>
          </div>
        </div>
        <div class="postbox">
          <h3><span><?php _e( 'Need help?', '404page' ); ?></span></h3>
          <div class="inside">
            <ul>
              <li><div class="dashicons dashicons-book-alt"></div>&nbsp;&nbsp;<a href="<?php echo $this->dc_url; ?>"><?php _e( 'Take a look at the Plugin Doc', '404page' ); ?></a></li>
              <li><div class="dashicons dashicons-wordpress"></div>&nbsp;&nbsp;<a href="<?php echo $this->wp_url; ?>/faq/"><?php _e( 'Take a look at the FAQ section', '404page' ); ?></a></li>
              <li><div class="dashicons dashicons-wordpress"></div>&nbsp;&nbsp;<a href="http://wordpress.org/support/plugin/<?php echo $this->plugin_slug; ?>/"><?php _e( 'Take a look at the Support section', '404page'); ?></a></li>
              <li><div class="dashicons dashicons-admin-comments"></div>&nbsp;&nbsp;<a href="http://smartware.cc/contact/"><?php _e( 'Feel free to contact the Author', '404page' ); ?></a></li>
            </ul>
          </div>
        </div>
        <div class="postbox">
          <h3><span><?php _e( 'Translate this Plugin', '404page' ); ?></span></h3>
          <div class="inside">
            <p><?php _e( 'It would be great if you\'d support the 404page Plugin by adding a new translation or keeping an existing one up to date!', '404page' ); ?></p>
            <p><a href="https://translate.wordpress.org/projects/wp-plugins/<?php echo $this->plugin_slug; ?>"><?php _e( 'Translate online', '404page' ); ?></a></p>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
  
  // add a link to settings page in plugin list
  function add_settings_link( $links ) {
    return array_merge( $links, array( '<a href="' . admin_url( 'themes.php?page=404pagesettings' ) . '">' . __( 'Settings', '404page' ) . '</a>') );
  }
  
  // uninstall plugin
  function uninstall() {
    if( is_multisite() ) {
      $this->uninstall_network();
    } else {
      $this->uninstall_single();
    }
  }
  
  // uninstall network wide
  function uninstall_network() {
    global $wpdb;
    $activeblog = $wpdb->blogid;
    $blogids = $wpdb->get_col( esc_sql( 'SELECT blog_id FROM ' . $wpdb->blogs ) );
    foreach ($blogids as $blogid) {
      switch_to_blog( $blogid );
      $this->uninstall_single();
    }
    switch_to_blog( $activeblog );
  }
  
  // uninstall single blog
  function uninstall_single() {
    foreach ( $this->settings as $key => $value) {
      delete_option( $key );
    }
  }

}

$smart404page = new Smart404Page();
?>