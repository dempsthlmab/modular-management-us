<?php

/**
 * Adds shortcode ui
 */
add_action( 'init','mm_shortcode_ui_reg');

/**
 *
 * LOOK HERE. HELLOOOOOO
 *
 *
 * 
 * All shortcodes needs to be in this file
 *
 * To define a new shortcode, use standard wordpress method to add a shortcode.
 * However, this only adds shortcodes to wordpress for interpretation.
 *
 * To make the shortcodes visual in the editor we use shortcode_ui_register_for_shortcode() in 
 * the bottom of this file. So first add shortcode, then register_ui.
 */

function mm_article( $atts, $content = "" ) {
    $atts = shortcode_atts( array(
        'image' => '',
        'bkr' => '',
        'text_color' => '',
        'title' => '',
        'image_pos' => 'left',
        'read_more' => ''

    ), $atts, 'article' );

    $image = wp_get_attachment_image_src($atts["image"], 'article');

    return '
        <div class="article '.$atts['image_pos'].'" style="background:'.$atts["bkr"].'">
            <a class="article-link" href="'.$atts['url'].'">
            <div class="article-image">
                <img src="'.$image[0].'">
            </div>
            <div class="article-content '.$atts['image_pos'].'" style="color: '.$atts['text_color'].'">
                <strong>'.$atts['title'].'</strong>
                '.$content.'
                <span class="readmore">Read more <span>></span></span>
            </div>
            <div class="clear"></div>
            </a>
        </div>';
}

add_shortcode( 'article', 'mm_article' );

/**
 * MM Quote
 */
function mm_quote($atts, $content = "" ){
    $atts = shortcode_atts( array(
        'title' => '',
        'italic' => ''

    ), $atts, 'quote' );

    return 
    '
    <div class="qoute-section">
        <div class="inner-section">
            <p class="qoute-title">
                '.$atts['title'].'
            </p>

            <p class="italic">'.$atts['italic'].'</p>

            <p class="text">
                '.$content.'
            </p>
        </div>
    </div>


    ';
}

//Just in case
remove_shortcode('quote');
add_shortcode('quote','mm_quote');

/**
 * This only accept one argument from custom post type slider
 */
function mm_slider_shortcode($atts){

    $atts = shortcode_atts( array(
        'slider_id' => '',

    ), $atts, 'mm_slider' );

    $sliderid = $atts['slider_id'];

    //$sliders = get_posts('post_type=slider&post_id='.$sliderid);

    $html = '<div class="rslides_container">
                        <ul class="rslides">    ';

    if( have_rows('slider',$sliderid) ):

        while ( have_rows('slider',$sliderid) ) : the_row();

            $image = wp_get_attachment_url(get_sub_field('image'));

            $html .= '
                <li style="background: url('. $image .') no-repeat; background-size: cover; ">
                    <img src="'. $image.'" alt="slider-second" />
                    <div class="caption-wrapper">
                    <div class="slider-title">'.get_sub_field('title').'</div>
                    <p class="slider-italic">'.get_sub_field('italic').'</p>
                    <a class="cta-button" href="'.get_sub_field('button_url').'">'.get_sub_field('button_title').'</a>
                    </div>
                </li>';

        endwhile; 
    endif;

    $html .= '</ul>
            </div>
        <div class="clear"></div>';

    

    return $html;
}

/**
 * Shortcodes
 */
add_shortcode('mm_slider','mm_slider_shortcode');

/**
 * Contact shortcode
 */

function mm_contact_shortcode(){
    return '<div class="form-article">
            <div class="inner">
                <div class="text-content">
                    <div class="form-title">Contact</div>
                    <div class="form-italic">Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.</div>

                    <div class="form-text">
                        You can also contact us by sending an e-mail to:
                        <a href="mailto:contact@modularmanagement.com">contact@modularmanagement.com</a>
                    </div>
                </div>
                <div class="form-content">
                    <form>
                        <div class="top-input-wrapper">
                            <div class="top-input">
                                <input type="text" placeholder="Name">
                            </div>
                            <div class="top-input">
                                <input type="text" placeholder="Phone">
                            </div>
                            <div class="clear"></div>
                        </div>  
                        <div class="email-wrapper">
                            <input type="email" placeholder="Email">
                        </div>
                        <div class="submit-wrapper">    
                            <button type="submit">SEND</button>
                        </div>
                    </form>
                </div>
                <div class="clear"></div>
            </div>
        </div>';
}

add_shortcode('contact','mm_contact_shortcode');

/**
 * Case shortcode
 */
function mm_case_shortcode($atts){

    $atts = shortcode_atts( array(
        'id' => '',

    ), $atts, 'case' );

    $id = $atts['id'];

    $case = get_posts('post_type=case&post_id'.$id);
    $caseid = $case[0]->ID;

    $field = get_fields($caseid);

    //If case shortcode id is empty, go for the title
    if(empty($field['div_id'])){
        $field['div_id'] = sanitize_title($case[0]->post_title);
    }

   


    return '
    <div class="case-article left gray">
        <div class="article">
            <div class="article-image">
                <img src="'.$field['small_image'].'">
            </div>

            <div class="article-content">
                <strong>'.$case[0]->post_title.'</strong>
                <p class="italic">'.$field['sub_title'].'</p>
                
                <!-- 
                    data id on button to know wich article to open
                -->

                <span class="cta-button case-button" data-id="'.$field['div_id'].'">Read more</span>
            </div>

            <div class="clear"></div>
        </div>
    
        <!-- Expanded article with id -->

        <div class="full-case" id="'.$field['div_id'].'">
            <div class="left-column">
                
                <div>
                    <img src="'.$field['expanded_image'].'">
                </div>
                
                <div class="left-case-desc">
                    <p class="title">'.__('Company Description').'</p>

                        <p>
                            '.$field['company_description'].'
                        </p>
                
                </div>

            </div>

            <div class="right-column">
                
                <p class="case-title italic">'.$field['expanded_title'].'</p>

                <div class="right-case-desc">
                    
                    <div class="ul-wrap">
                        <p class="title">'.__('Business Situation').'</p>
                        <ul>
                            '.newline_to_li($field['business_situation']).'
                        </ul>
                    </div>

                    <div class="ul-wrap">
                        <p class="title">'.__('Modularity Results').'</p>
                        <ul>
                            '.newline_to_li($field['modular_results']).'
                        </ul>
                    </div>

                    <div class="ul-wrap">
                        <p class="title">'.__('Measurable Improvements').'</p>
                        <ul>
                            '.newline_to_li($field['measurable_improvements']).'
                        </ul>
                    </div>
                </div>

                <div class="case-footer">
                    <span class="open-overlay">'.__('Download the full case story (requires free registration >)').'</span>

                    <div class="close-article">
                        <img src="'. get_template_directory_uri().'/img/arrow-up.png">
                    </div>

                </div>

            </div>
            <div class="clear"></div>

        </div>
    </div>

    ';
}

add_shortcode('case','mm_case_shortcode');

/**
 * Big article shortcode
 */
function mm_big_article_shortcode($atts,$content=''){

    $atts = shortcode_atts( array(
        'intro' => '',
        'italic-title' => '',
        'image' => '',
        'quote' => '',
        'show_contact_form' => ''


    ), $atts, 'big_article' );

    $image = wp_get_attachment_image_src($atts["article-image"], 'article');


    $html .= '
        <div class="article right" style="background: #fff;">
                    
            <div class="article-text">
                <div class="text-block">
                    <p>'.$atts['intro'].'</p>
                </div>

                <div class="text-block">
                    <div class="italic-title">'.$atts['italic-title'].'</div>

                    '.$content.'

                </div>
    
            </div>

            <div class="article-image">
                <img src="'.$image[0].'">
            </div>';

            if($atts['show_contact_form'] == 'true'){

                $html .= '<div class="form-column">

                    <div class="form-article">
                        <div class="inner">
                            <div class="text-content">
                                <div class="form-title">Contact</div>
                                <div class="form-italic">Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.</div>

                            
                            </div>
                            <div class="form-content">
                                <form>
                                    <div class="top-input-wrapper">
                                        <div class="top-input">
                                            <input type="text" placeholder="Name">
                                        </div>
                                        <div class="top-input">
                                            <input type="text" placeholder="Phone">
                                        </div>
                                        <div class="clear"></div>
                                    </div>  
                                    <div class="email-wrapper">
                                        <input type="email" placeholder="Email">
                                    </div>
                                    <div class="submit-wrapper">    
                                        <button type="submit">SEND</button>
                                    </div>
                                </form>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                  </div>';

            }else{
                $html .= '<div class="article-small-quote">
                    <div class="italic-bold-title">
                        '.$atts['quote'].'
                    </div>
                </div>';
            }

    $html .='


            <div class="clear"></div>
           
        </div>
    ';

    return $html;
}

add_shortcode('big_article','mm_big_article_shortcode');

/**
 * Office shortcode
 */
function mm_office_shortcode($atts){

    $atts = shortcode_atts( array(
        'office_1' => '',
        'office_2' => '',
        'office_3' => '',
        'office_4' => ''
       
    ), $atts, 'office' );

    $ids = array($atts['office_1'],$atts['office_2'],$atts['office_3'],$atts['office_4']);
    $defaultlocation = 'stockholm-sweden'; //For navigation, make this dynamic later

    $html = '<div id="offices">';

    $offices = get_posts(array( 'post__in' => $ids, 'post_type' => 'office'));

    foreach($offices as $o){

        $display = '';
        if(sanitize_title(get_field('location',$o->ID)) == $defaultlocation){
            $display = 'display: block;';
        }

        $html .= '

            <div id="'.sanitize_title(get_field('location',$o->ID)).'-office" style="'.$display.'" class="office-wrapper">
                <div class="office-hero">
                    <img src="'.get_field('office_hero',$o->ID).'">
                </div>


                <div class="office-manager">

                    <div class="rounded-image-wrapper">
                        <img src="'.get_field('contact_person_image',$o->ID).'">
                    </div>

                    <p class="bold-italic">'.get_field('office_manager',$o->ID).'</p>
                    <p class="light">Office Manager</p>
                
                </div>

                <div class="office-contact-details">

                    <p class="title">'.__('Head Office').'</p>
                    <p class="italic-title">'.get_field('location',$o->ID).'</p>

                    <div class="adress">
                        <div class="list-image">
                            <img src="'.get_template_directory_uri().'/img/icons/pin.png">
                        </div>
                        <ul>
                            <li>Address:</li>
                            '.newline_to_li(get_field('address',$o->ID)).'
                        </ul>
                    </div>

                    <div class="phone">
                        <div class="list-image">
                            <img src="'.get_template_directory_uri() .'/img/icons/phone.png">
                        </div>
                        <ul>
                            <li>Tel: <a href="">'.get_field('tel',$o->ID).'</a></li>
                            <li>Fax: <a href="">'.get_field('fax',$o->ID).'</a></li>   
                        </ul>
                    </div>


                    <div class="mail">
                        <div class="list-image">
                            <img src="'.get_template_directory_uri() .'/img/icons/letter.png">
                        </div>
                        <ul>
                            <li>E-mail: <a href="mailto:'.get_field('email',$o->ID).'">'.get_field('email',$o->ID).'</a></li>
                        </ul>
                    </div>



                </div>
                <div class="clear"></div>
            </div>
        ';
        
    }
    $html .= '</div>';

    $html .= '<div id="office-nav">';

    //Navigation
    foreach($offices as $o){

        $active = '';
        if(sanitize_title(get_field('location',$o->ID)) == $defaultlocation){
            $active = 'active';
        }

        $html .= '
        <div class="office-navlink '.$active .'" data-id="'.sanitize_title(get_field('location',$o->ID)).'-office">
            <img src="'.get_field('location_icon',$o->ID).'">
        </div>';
    }
        
    $html .=  '</div>';

    return $html;

}

add_shortcode('office','mm_office_shortcode');

/**
 * Executive board shortcode
 */
function mm_executive_board_shortcode($atts){

    $atts = shortcode_atts( array(
        'image-1' => get_template_directory_uri() .'/img/sthlm-office.png',
        'name-1'  => 'Lars Kylberg',
        'title-1' => 'Chairman',
        'image-2' => get_template_directory_uri() .'/img/sthlm-office.png',
        'name-2'  => 'Christer Mellvé',
        'title-2' => 'Board member',
        'image-3' => get_template_directory_uri() .'/img/sthlm-office.png',
        'name-3'  => 'Alex von Yxkull',
        'title-3' => 'President and CEO'

    ), $atts, 'executive_board' );



    return 
    '
        <div id="board-nav">

            <p class="title">'.__('Executive Board').'</p>

            <div class="board-member">
                <div class="rounded-image-wrapper">
                    <img src="'.$atts['image-1'].'">
                </div>
                <p class="board-name">'.$atts['name-1'].'</p>
                <p class="board-title">'.$atts['title-1'].'</p>
            </div>

            <div class="board-member">
                <div class="rounded-image-wrapper">
                    <img src="'.$atts['image-2'].'">
                </div>
                <p class="board-name">'.$atts['name-2'].'</p>
                <p class="board-title">'.$atts['title-2'].'</p>
            </div>

            <div class="board-member">
                <div class="rounded-image-wrapper">
                    <img src="'.$atts['image-3'].'"">
                </div>
                <p class="board-name">'.$atts['name-3'].'</p>
                <p class="board-title">'.$atts['title-3'].'</p>
            </div>
        </div>
    ';


}

add_shortcode('executive_board','mm_executive_board_shortcode');

/**
 * Hero shortcode
 */
function mm_hero($atts){

    $atts = shortcode_atts( array(
        'image' => '',

    ), $atts, 'hero' );

    $image = wp_get_attachment_image_src($atts["image"], 'header-full');

    return '<div id="hero-image" style="background: url(\''.$image[0].'\'); background-size: cover;">
            <img src="'.$image[0].'">
        </div>';
}

add_shortcode('hero','mm_hero');

/**
 * Static Case
 */
function mm_static_case(){

    $atts = shortcode_atts( array(
        'image' => '',

    ), $atts, 'static_case' );

}

add_shortcode('static_case','mm_static_case');

/**
 * Three Icons shortcode
 */
function mm_three_icons(){

    return '<div id="top-articles">

            <div id="top-article-title">
                '.__('Modular Management Enables:').'
            </div>

            <a class="top-link" href="" id="opx">
                <div class="top-link-img-wrap">
                    <img src="'.get_template_directory_uri() .'/img/top-links/oe.png">
                </div>
                <p class="link-title">'.__('Operational Excellence').'</p>
                
                <p>'.__('Achieving high productivity and quality').'</p>

                <div class="quotes">
                    <div id="opx-1">
                        '.__('Do you want to:').'<br>
                        '.__('Achieve economies of scale?').'
                    </div>
                    <div id="opx-2">
                        '.__('Do you want to:').'<br>
                        '.__('Stabilize your processes?').'
                    </div>
                    <div id="opx-3">
                        '.__('Do you want to:').'<br>
                        '.__('Improve quality?').'
                    </div>
                </div>

            </a>

            <a class="top-link middle-link" href="" id="cui">

                <div class="top-link-img-wrap">
                    <img src="'.get_template_directory_uri(). '/img/top-links/ci.png">
                </div>
                <p class="link-title">'.__('Customer Intimacy').'</p>

                <p>'.__('Increasing market share and customer loyalty').'</p>

                <div class="quotes">
                    <div id="cui-1">
                        '.__('Do you want to:').'<br>
                        '.__('Widen your assortment?').'
                    </div>
                    <div id="cui-2">
                        '.__('Do you want to:').'<br>
                        '.__('Accelerate customer adaptation?').'
                    </div>
                    <div id="cui-3">
                        '.__('Do you want to:').'<br>
                        '.__('Increase precision in your<br>product offering?').'
                    </div>
                </div>

                <img src="'.get_template_directory_uri().'/img/icons/plus.png" class="plus right">
                <img src="'.get_template_directory_uri() .'/img/icons/plus.png" class="plus left">

            </a>

            <a class="top-link" href="" id="pld">
                <div class="top-link-img-wrap">
                    <img src="'.get_template_directory_uri() .'/img/top-links/pl.png">
                </div>
                <p class="link-title">'.__('Product Leadership').'</p>

                <p>'.__('Creating superior products and brand image').'</p>

                <div class="quotes">
                    <div id="pld-1">
                        '.__('Do you want to:').'<br>
                        '.__('Control R&D spending?').'
                    </div>
                    <div id="pld-2">
                        '.__('Do you want to:').'<br>
                        '.__('Reduce time to market?').'
                    </div>
                    <div id="pld-3">
                        '.__('Do you want to:').'<br>
                        '.__('Improve performance of your<br>product offering?').'
                    </div>
                </div>

            </a>
            
        </div>';
}

add_shortcode('three_icons','mm_three_icons');

/**
 * Adds all the functionality for the shortcodes so they are usable in the admin interface
 * @return [type] [description]
 */
function mm_shortcode_ui_reg(){

    /**
     * Register a UI for the Shortcode.
     * Pass the shortcode tag (string)
     * and an array or args.
     */
    shortcode_ui_register_for_shortcode(
        'article',
        array(

            // Display label. String. Required.
            'label' => 'Article',

            // Icon/image for shortcode. Optional. src or dashicons-$icon. Defaults to carrot.
            'listItemImage' => 'dashicons-editor-quote',
            'inner_content' => array(
                'label' => 'Article'
            ),

            // Available shortcode attributes and default values. Required. Array.
            // Attribute model expects 'attr', 'type' and 'label'
          // Supported field types:** text, checkbox, textarea, radio, select, email, url, number, and date.  
            'attrs' => array(
                array(
                    'label' => 'Image Pos',
                    'attr'  => 'image_pos',
                    'options' => array('left' => 'left', 'right' => 'right'),
                    'type'  => 'select',
                ),
                array(
                    'label' => 'title',
                    'attr' => 'title',
                    'type' => 'text'
                ),
                array(
                    'label' => 'Image src',
                    'attr'  => 'image',
                    'type'  => 'attachment',
                ),
                array(
                    'label' => 'Background color',
                    'attr'  => 'bkr',
                    'type'  => 'text',
                ),
                array(
                    'label' => 'Text color',
                    'attr'  => 'text_color',
                    'type'  => 'text',
                ),
                array(
                    'label' => 'Read more url',
                    'attr'  => 'read_more',
                    'type'  => 'text',
                    'placeholder' => 'http://example.com/link'
                )
            ),
        )
    );

    /**
     * Quote
     */
    shortcode_ui_register_for_shortcode(
        'quote',
        array(
            'label' => 'Quote',
            'listItemImage' => 'dashicons-editor-quote',
            'inner_content' => array(
                'label' => 'Content'
            ),
            'attrs' => array(
                array(
                    'label' => 'Title',
                    'attr'  => 'title',
                    'type'  => 'text',
                ),
                 array(
                    'label' => 'Italic',
                    'attr'  => 'italic',
                    'type'  => 'textarea',
                ),
            ),
        )       
    );

    /**
     * Slider
     */

    $sliders = get_posts('post_type=slider');

    $slider_options = array();
    foreach($sliders as $s){
        $slider_options[$s->ID] = $s->post_title;
    }

    shortcode_ui_register_for_shortcode(
        'mm_slider',
        array(
            'label' => 'Slider',
            'listItemImage' => 'dashicons-editor-quote',
            'attrs' => array(
                array(
                    'label' => 'Slider',
                    'attr'  => 'slider_id',
                    'options' => $slider_options,
                    'type'  => 'select',
                ),
            ),
        )
    );

    /**
     * Contact form
     */
    
    shortcode_ui_register_for_shortcode(
        'contact',
        array(
            'label' => 'Contact',
            'listItemImage' => 'dashicons-editor-quote',
        )
    );

    /**
     * Case
     */

    $cases = get_posts('post_type=case');

    $cases_options = array();
    foreach($cases as $c){
        $cases_options[$c->ID] = $c->post_title;
    }

    shortcode_ui_register_for_shortcode(
        'case',
        array(
            'label' => 'Case',
            'listItemImage' => 'dashicons-editor-quote',
            'attrs' => array(
                'label' => 'Select which case to include',
                'attr' => 'id',
                'options' => $cases_options,
                'type' => 'select'          
            ),
        )
        
    );

    /**
     * Big Article
     */
    
    shortcode_ui_register_for_shortcode(
        'big_article',
        array(
            'label' => 'Big Article',
            'listItemImage' => 'dashicons-editor-quote',
            'inner_content' => array(
                'label' => 'Article Content'
            ),
            'attrs' => array(
                array(
                    'label' => 'Intro',
                    'attr' => 'intro',
                    'type' => 'textarea'
                ),
                array(
                    'label' => 'Title',
                    'attr' => 'italic-title',
                    'type' => 'text'
                ),
                array(
                    'label' => 'image',
                    'attr' => 'image',
                    'type' => 'attachment'
                ),
                array(
                    'label' => 'Quote',
                    'attr' => 'quote',
                    'type' => 'textarea'
                ),
                array(
                    'label' => 'Show Contact Form',
                    'attr' => 'show_contact_form',
                    'description' => 'Will replace Quote',
                    'options' => array('false' => 'false','true' => 'true'),
                    'type' => 'select'
                ),
            ),
        )
        
    );

    //$settings = array( 'textarea_name' => 'inner_content' );

    //wp_editor( '', 'inner_content', $settings );

    /**
     * Office
     */
    
    $offices = get_posts('post_type=office');

    $o_selects = array();
    $o_selects[0] = 'None';
    foreach($offices as $o){

        $o_selects[$o->ID] = $o->post_title;
    }
    
    shortcode_ui_register_for_shortcode(
        'office',
        array(
            'label' => 'Office',
            'listItemImage' => 'dashicons-editor-quote',
            'attrs' => array(
                array(
                    'label' => 'Office 1',
                    'attr'  => 'office_1',
                    'options' => $o_selects,
                    'type'  => 'select',
                ),
                array(
                    'label' => 'Office 2',
                    'attr'  => 'office_2',
                    'options' => $o_selects,
                    'type'  => 'select',
                ),
                array(
                    'label' => 'Office 3',
                    'attr'  => 'office_3',
                    'options' => $o_selects,
                    'type'  => 'select',
                ),
                array(
                    'label' => 'Office 4',
                    'attr'  => 'office_4',
                    'options' => $o_selects,
                    'type'  => 'select',
                ),
            ),
        )
        
    );

    /**
     * Board members
     */
    
    shortcode_ui_register_for_shortcode(
        'executive_board',
        array(
            'label' => 'Executive board members',
            'listItemImage' => 'dashicons-editor-quote',
            'attrs' => array(
                array(
                    'label' => 'Image 1',
                    'attr' => 'image-1',
                    'type' => 'attachment'
                ),
                array(
                    'label' => 'Name 1',
                    'attr' => 'name-1',
                    'type' => 'text'
                ),
                array(
                    'label' => 'Title 1',
                    'attr' => 'title-1',
                    'type' => 'text'
                ),
                array(
                    'label' => 'Image 2',
                    'attr' => 'image-2',
                    'type' => 'attachment'
                ),
                array(
                    'label' => 'Name 2',
                    'attr' => 'name-2',
                    'type' => 'text'
                ),
                array(
                    'label' => 'Title 2',
                    'attr' => 'title-2',
                    'type' => 'text'
                ),
                array(
                    'label' => 'Image 3',
                    'attr' => 'image-3',
                    'type' => 'attachment'
                ),
                array(
                    'label' => 'Name 3',
                    'attr' => 'name-3',
                    'type' => 'text'
                ),
                array(
                    'label' => 'Title 3',
                    'attr' => 'title-3',
                    'type' => 'text'
                ),
              
            ),
        )
        
    );
    
    /**
     * Hero
     */
    shortcode_ui_register_for_shortcode(
        'hero',
        array(
            'label' => 'Hero',
            'listItemImage' => 'dashicons-editor-quote',
            'attrs' => array(
                array(
                    'label' => 'Image 1',
                    'attr' => 'image',
                    'type' => 'attachment'
                ),
              
            ),
        )
        
    );

    /**
     * Three icons
     */
    shortcode_ui_register_for_shortcode(
        'three_icons',
        array(
            'label' => 'Three Icons',
            'listItemImage' => 'dashicons-editor-quote',
            'attrs' => array(
                
              
            ),
        )
        
    );


}




?>