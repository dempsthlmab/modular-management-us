jQuery(document).ready(function( $ ) {
    //console.log('rdy')

function stickyHeader(){
  	var position = $(window).scrollTop();

		if(position > 40){
    		$('#header').addClass('fixed');
    	} else {
    		$('#header').removeClass('fixed');
    	}

}

function showTransparentbg(){
    var $li = $('#top-nav li');

        if($li.hasClass('active')){
            $('#header').find('.subheader').css('opacity', 1);

            $li.on('mouseenter', function(event) {
                $('#header').find('.subheader').css('opacity', 1);
            });

            $li.on('mouseleave', function(event) {
                $('#header').find('.subheader').css('opacity', 1);
            });

        } else {
            $('#header').find('.subheader').css('opacity', 0);

            $li.on('mouseenter', function(event) {
                $('#header').find('.subheader').css('opacity', 1);
            });

            $li.on('mouseleave', function(event) {
                $('#header').find('.subheader').css('opacity', 0);
            });
        }
}

function centerArticleContent(){
	var wrapper = $('.article');

	wrapper.each(function(index) {

	  var height = $(this).height();
	  var innerheight = $(this).find('.article-content').height();
      var x = height - innerheight;

	  $(this).find('.article-content').css('padding-top', x/2);

	});

}

function centerQuote(){

    var bigwrapper = $('.article');

    bigwrapper.each(function(index) {

      var height = $(this).height();
      var innerheight = $(this).find('.article-small-quote').height();
      var imageheight = $(this).find('.article-image').height();

      var y = height - imageheight;
      var x = y - innerheight;



      $(this).find('.article-small-quote').css('padding-top', x/2);
      $(this).find('.article-small-quote').css('padding-bottom', x/2);

    });

}

function centerForm(){

    var bigwrapper = $('.article');

    bigwrapper.each(function(index) {

      var height = $(this).height();
      var innerheight = $(this).find('.form-column').height();
      var imageheight = $(this).find('.article-image').height();

      var y = height - imageheight;
      var x = y - innerheight;

      $(this).find('.form-column').css('padding-top', x/2);
      $(this).find('.form-column').css('padding-bottom', x/2);

    });

}

function reverseUl(){
/*
        var wrapper = document.getElementById("left-footer");
        var divs = wrapper.getElementsByClassName("uls");
        for (var i = divs.length - 1; i >= 0; i--) {
            wrapper.appendChild(divs[i]);
        }

        var rightdiv = document.getElementById("right-uls");
        var rightuls = rightdiv.getElementsByTagName("ul");
        for (var i = rightuls.length - 1; i >= 0; i--) {
            rightdiv.appendChild(rightuls[i]);
        }

        var leftdiv = document.getElementById("left-uls");
        var leftuls = leftdiv.getElementsByTagName("ul");
        for (var i = leftuls.length - 1; i >= 0; i--) {
            leftdiv.appendChild(leftuls[i]);
        }
*/
}

function mobileMenu(){

    if($(window).width() < 769){

        var nav = document.getElementById("top-nav");
        var navList = nav.getElementsByTagName("li");
        for (var i = navList.length - 1; i >= 0; i--) {
            nav.appendChild(navList[i]);
        }
/*
        var wrapper = document.getElementById("left-footer");
        var divs = wrapper.getElementsByClassName("uls");
        for (var i = divs.length - 1; i >= 0; i--) {
            wrapper.appendChild(divs[i]);
        }

        var rightdiv = document.getElementById("right-uls");
        var rightuls = rightdiv.getElementsByTagName("ul");
        for (var i = rightuls.length - 1; i >= 0; i--) {
            rightdiv.appendChild(rightuls[i]);
        }

        var leftdiv = document.getElementById("left-uls");
        var leftuls = leftdiv.getElementsByTagName("ul");
        for (var i = leftuls.length - 1; i >= 0; i--) {
            leftdiv.appendChild(leftuls[i]);
        }
*/
    }

	var $burger = $('#burger-bg');

    $burger.on('click', function(){

        if($('#burger-toggle').hasClass('active')){
            $burger.find('#burger-toggle').removeClass('active');
            $('#nav').find('#top-nav').slideUp(500);

                setTimeout(function(){
                    $('#lang-cta').fadeOut();
                    $('#login-btn').fadeOut();
                }, 300);

        } else {

            $('#lang-cta').fadeIn();
            $('#login-btn').fadeIn();

                setTimeout(function(){
                    $burger.find('#burger-toggle').addClass('active');
                    $('#nav').find('#top-nav').slideDown(500);
                }, 300);
        }

    });


    $('.mobile-href').on('click', function(){

        var id = $(this).data('id');
        var parent = $('#'+id).parent();
        parent.toggleClass('current');
        $('#'+id).slideToggle();
    });
}

/*
function openCaseArticle(){
	var $openButton = $('.case-article').find('.case-button');

    $openButton.on('click', function(){
    	var $this = $(this);
    	var $parent = $this.parent().parent().offset();
    	var height = $this.parent().parent().height();
    	var id = $this.data('id');

    	var $wrapper = $this.parent().parent().parent();
    	var $closeButton = $wrapper.find('.full-case').find('.right-column').find('.close-article');

    	if($this.hasClass('active')){
    		$this.find('.read-more-t').show();
            $this.find('.close-t').hide();
    		$this.removeClass('active');
        	$('#'+id).slideUp().removeClass('active');
    	} else {
    		$this.find('.close-t').show();
            $this.find('.read-more-t').hide();
    		$this.addClass('active');
        	$('#'+id).slideDown().addClass('active');


    	}

    	setTimeout(function(){
    		$('html, body').animate({ scrollTop: $parent.top -75 + height + 'px'}, 500);
    	}, 300);

    	$closeButton.on('click', function(){
    		$this.find('.read-more-t').show();
            $this.find('.close-t').hide();
        	$('#'+id).slideUp().removeClass('active');
    	});

    	$('.open-overlay').on('click', function(){

            if(Modernizr.touch && $(window).width() < 769){
                $('html, body').animate({ scrollTop: 0 }, 300);
                setTimeout(function(){
                    $('#overlay').addClass('active');
                },300);

            } else {
                $('#overlay').addClass('active');
            }

            $('#close-form').on('click', function(){
                $('#overlay').removeClass('active');
            });
    	});
	});
}
*/
function openoverlay(){
    $('.open-overlay').on('click', function(){
      //console.log('open overlay');

        $('.overlay-login').removeClass('active');


    if(Modernizr.touch && $(window).width() < 769){
        $('html, body').animate({ scrollTop: 0 }, 300);
        setTimeout(function(){
            $('#overlay').addClass('active');
        },300);

    } else {
        $('#overlay').addClass('active');
    }

    $('#close-form').on('click', function(){
        $('#overlay').removeClass('active');
    });

});
}

function openoverlayLogin(){
    //console.log('openoverlayLogin');


    $('.open-overlay-login-btn').on('click', function(event){
      //console.log(event.target);
      if($(event.target).hasClass('logout')) {
        return;
      }

      //console.log('open login??');

      $('#loginform').parent().find('.login-error-msg').css('display', 'none'); // Hide login error

      $('#overlay').removeClass('active');

      login = $('.overlay-login');

      if(Modernizr.touch && $(window).width() < 769){
          $('html, body').animate({ scrollTop: 0 }, 300);
          setTimeout(function(){
              login.addClass('active');
          },300);

      } else {
          login.addClass('active');
      }

      $('.close-form').on('click', function(){
          login.removeClass('active');
      });

    });


}

function openoverlaychange(){
    $('.open-overlay-change').on('click', function(){
        //console.log('openoverlaychange');
        var change = $('#overlay');

        if(Modernizr.touch && $(window).width() < 769){
            $('html, body').animate({ scrollTop: 0 }, 300);
            setTimeout(function(){
                change.addClass('active');
            },300);

        } else {
            change.addClass('active');
        }

        $('#close-form').on('click', function(){
            change.removeClass('active');
        });
    });
}

function openPublication(){
    var $openButton = $('.publication-content').find('.case-button');

    $openButton.on('click', function(){

        var $this = $(this);
        var $parent = $this.parent().parent().offset();
        var height = $this.parent().parent().height();
        var id = $this.data('id');
        var $wrapper = $this.parent().parent();
        var $closeButton = $wrapper.find('.full-publication').find('.full-publication-footer').find('.close-article');

        //console.log($closeButton)

        if($this.hasClass('active')){
            $this.find('.read-more-t').show();
            $this.find('.close-t').hide();
            $this.removeClass('active');
            $('#'+id).slideUp().removeClass('active');
        } else {
            $this.find('.read-more-t').hide();
            $this.find('.close-t').show();
            $this.addClass('active');
            $('#'+id).slideDown().addClass('active');
        }

        $closeButton.on('click', function(){
            $this.removeClass('active');
            $('#'+id).slideUp().removeClass('active');
        });

    });

}

function officeNavigation(){
    //$('#offices').find('.office-wrapper').first().css('display', 'block');
    var $officeButton = $('.office-navlink');
    //var wrapperHeight = $('#offices').height();

    //$('#offices').css('height', wrapperHeight);
    //$('#offices').find('.office-wrapper').css('height', wrapperHeight);

    $officeButton.on('click', function(){
        var $this = $(this);
        var id = $this.data('id');
        $('.office-navlink').removeClass('active');
        $this.addClass('active');

        $('#office-nav').removeClass().addClass(id);;

            $('.office-wrapper').fadeOut(400);

            setTimeout(function(){
                $('#'+id).fadeIn();
            },500)
    });
}

function mobileTT(){
    $('.navlink-quote').on('click', function(){
        var $this = $(this);
        var id = $this.data('id');
        $('.quotes').removeClass('active');
        $('#'+id).addClass('active');
    });
}

$.fn.hasAttr = function(name) {
    return this.attr(name) !== undefined;
};

function getSelectVal(){

    $('#country.country').on('change',function(){

        var number = $(this).val();
        $('#default-value span').text(number); // Could this set a value on 2 places? It looks like it exists on 2 places in the code
    });

}

// FORM STUFF
function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);

    if (score > 65){
        $('#strength_score').addClass('strong');
        $('#strength_score').removeClass('weak good');
        return "Strong";
    }

    if (score > 40){
        $('#strength_score').addClass('good');
        $('#strength_score').removeClass('weak strong');
        return "Good";
    }

    if (score >= 5){
        $('#strength_score').addClass('weak');
        $('#strength_score').removeClass('good strong');
        return "Weak";
    }

    $('#strength_score').removeClass('weak good strong');
    return "Password Strength";
}

function checkPasswordStrength(){
    $("#password").on("input", function() {
        var pass = $(this).val();
        //$("#strength_human").text(checkPassStrength(pass));
        //$("#strength_score").text(scorePassword(pass));
    	$("#strength_score p").text(checkPassStrength(pass));
    });
}

function contactValidation(){

    /*var $areaSelect = $('#search-options'),
    $print = $('#select-box-wrapper').find('.def-value');

    $areaSelect.on('change', function(){
        var selected = $(this).val();
        alert(selected);
        $print.text(selected);
    });*/


    $('#signup-form').submit( function(e) {
        e.preventDefault();


        //var $form = $(this);
        //var $captcha = $form.find('#captchatext').val();
        // console.log('captcha: ');
        // console.log($captcha);
        // console.log(ajax_captcha_object.ajaxurl);

        var $form = $(this),
            $email = $form.find('input[name="user_email"]'),
            $password = $form.find('input[name="user_password"]'),
            $repeatPassword = $form.find('input[name="repeat-password"]'),
            $fname = $form.find('input[name="first_name"]'),
            $lname = $form.find('input[name="last_name"]'),
            $company = $form.find('input[name="company"]'),
            /*$phone = $form.find('input[name="phone"]'),*/
            $phone = $form.find('input[name="phone"]'),
            $captcha = $form.find('#captchatext'),


            emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            emailRegex.test($email.val()) ? $email.parent().removeClass('error') : $email.parent().addClass('error');
            $fname.val() ? $fname.parent().removeClass('error') : $fname.parent().addClass('error');
            $lname.val() ? $lname.parent().removeClass('error') : $lname.parent().addClass('error');
            $company.val() ? $company.parent().removeClass('error') : $company.parent().addClass('error');
            $phone.val() ? $phone.parent().removeClass('error') : $phone.parent().addClass('error');
            $password.val() ? $password.parent().removeClass('error') : $password.parent().addClass('error');
            $repeatPassword.val() ? $repeatPassword.parent().removeClass('error') : $repeatPassword.parent().addClass('error');


            // Check captcha
            //console.log('captcha: ');
            //console.log($captcha.val());

            $.ajax({
               type: 'POST',
               dataType: 'json',
               url: ajax_captcha_object.ajaxurl,
               data: {
                   'captcha_code': $captcha.val(),
               },
               success: function(data){
                   //console.log('data.captcha:');
                   //console.log(data.captcha);

                   if(data.captcha == true) {
                     $captcha.parent().removeClass('error');

                     // Continue only if we've had a successful captcha check
                     var errors = $form.find('.error').length;

                     if(errors == 0) {
                       // Captcha and all others are good
                       $form.unbind('submit').submit();
                     } else {
                       // If something else went wrong
                       $('.signup-new-captcha-image').trigger('click');
                       $captcha.val('');
                       //console.log(errors + ' errors have been detected');
                     }
                   } else {
                     $captcha.parent().addClass('error');
                     $captcha.val('');
                     $('.signup-new-captcha-image').trigger('click');

                   }
               }, error: function(data) {
                 //console.log('error in captcha check:');
                 //console.log(data);
                 $captcha.addClass('error');
               }
           });


      /*      if ($password.val() !== $repeatPassword.val() || $repeatPassword.val() == '' && $password.val() == ''){
                $repeatPassword.parent().addClass('error');
                $password.parent().addClass('error');
            } else {
                $repeatPassword.parent().removeClass('error');
                $password.parent().removeClass('error');
            }
        */


    });

}

function pageFormValidation(){

    //remove span on focus
    $('input').on('focus', function(){
     $(this).parent().find('span').remove();
    });

    $('#page-form').on('submit', function(e) {
        e.preventDefault();

        var $form = $(this),
            $name = $form.find('input[name="name"]'),
            $phone = $form.find('input[name="phone"]'),
            $email = $form.find('input[name="email"]'),

            emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            emailRegex.test($email.val()) ? $email.parent().removeClass('error') : $email.parent().addClass('error');
            $name.val() ? $name.parent().removeClass('error') : $name.parent().addClass('error');
            $phone.val() ? $phone.parent().removeClass('error') : $phone.parent().addClass('error');

            var errors = $form.find('.error').length;
            if(!errors) {
                $.get($form.attr('action'), {}, function(data){

                    alert('Thank you!');


                });
            }
    });
}

function showSubmenu(){

    /*var $submenuLink = $('#menu-item-42').find('a');

    if($submenuLink.hasClass('active')){
        $submenuLink.parents('li:eq(-1)').addClass('active');
    }

    var $submenuLink2 = $('#menu-item-39').find('a');

    if($submenuLink2.hasClass('active')){
        $submenuLink2.parents('li:eq(0)').addClass('active');
    }*/

    // Hide any active sub menu when hovering over an item that does not have a menu
    /*$('#top-nav li').on('mouseenter', function (event) {
        if (!$(this).hasClass('has-menu')) {
            $('#top-nav li.active').find('.submenu').css('opacity', 0);
        }
    }).on('mouseleave', function (event) {
        if (!$(this).hasClass('has-menu')) {
            $('#top-nav li.active').find('.submenu').css('opacity', '');
        }
    });*/

    $('#top-nav li .submenu').hover(function(e){
        $(this).closest('li').find('.desktop-href').toggleClass('active');
    });

}

function displayVideo(){
    $('#display-video').on('click', function(){
        $('#static-video-content').fadeOut();

//        $('.video-background').css('display', 'block');
        });
}

function login(){
    // Perform AJAX login on form submit
    /*$('form#ajaxlogin').on('submit', function(e){
        $('form#ajaxlogin p.status').show().html(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#ajaxlogin #username').val(),
                'password': $('form#ajaxlogin #password').val(),
                'security': $('form#ajaxlogin #security').val() },
            success: function(data){
                $('form#ajaxlogin p.status').html(data.message);
                if (data.loggedin == true){
                    document.location.href = ajax_login_object.redirecturl;
                }
            },
        });
        e.preventDefault();
    });*/

    $('#loginform').on('submit',function(e){
        //e.preventDefault();
        //console.log('login submit 2');
        //console.log(ajax_login_object.ajaxurl);
        //console.log(ajax_login_object.loadingmessage);



         $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('#loginform #user_login').val(),
                'password': $('#loginform #user_pass').val(),
                'security': $('#loginform #security').val(),
                /*'captcha_code' : $('#loginform #captchatext').val(),*/
            },
            success: function(data){
                //console.log('login response:');
                //console.log(data);

                //console.log('1: ' + ajax_login_object.redirecturl);
                //console.log('2: ' + ajax_login_object.path);

                var url;
                // Only add the path if we're on a start page for a country. The path is set to "account" in functions.php. Otherwise, redirect to the page where the user logged in from.
                if (data.loggedin == true){
                  if(
                    ajax_login_object.redirecturl == '' || 
                    ajax_login_object.redirecturl == '/' || 
                    ajax_login_object.redirecturl == '/sv/' || 
                    ajax_login_object.redirecturl == '/de/' || 
                    ajax_login_object.redirecturl == '/us/' 
                  ) {
                    url = ajax_login_object.redirecturl + ajax_login_object.path;
                  } else {
                    url = ajax_login_object.redirecturl;
                  }

                  //console.log('redirecting to ' + url);

                  document.location.href = url;

                } else {
                    //$('.login-new-captcha-image').trigger('click');
                    //console.log('loggedin is false');
                    $('#loginform').parent().find('.login-error-msg').text(data.message);
                    $('#loginform').parent().find('.login-error-msg').css('display', 'inline-block');
                }
            },
        });
        e.preventDefault();

    });
}

function loginformbuttons(){
    $('.login-submit .button-primary').after($('#login-form-buttons'));
}


var player;
function onYouTubePlayerAPIReady() {
    player = new YT.Player('video', {
      events: {
        'onReady': onPlayerReady
      }
    });
}
function onPlayerReady(event) {
  //event.target.playVideo();
    $("#play-video").on('click', function() {
      player.playVideo();
    });
    $("#pausa").on('click', function() {
      player.stopVideo();
    });
}

//READY

$(window).scroll(function (event){
    if(!Modernizr.touch && $(window).width() > 769){
        setTimeout(function(){
            stickyHeader();
        }, 40);
    }
});

$(window).resize(function(){
    if($(window).width() > 769){
        centerArticleContent();
        centerQuote();
        centerForm();
    } else {
        $('.article-content').css('padding-top', 40)
    }
});

if($(window).width() > 769){
    centerArticleContent();
    centerQuote();
    centerForm();
    stickyHeader();
}

if($(window).width() < 769){
    mobileMenu();
} else {
    reverseUl();
}

$('.dropdown-list .item .title').on('click', function() {
    $(this).parent().toggleClass("collapsed");
});

$('.expandable').each(function() {
    var parent = $(this);
    var collapsible = parent.find('.collapse');

    parent.find('.close').on('click', function() {
        var button = $(this);

        var scrollTo;
        if(!collapsible.is(":visible")) {
            var top = parent.find('.top');

            scrollTo = top.offset().top - 75 + top.height();
        }
        else {
            scrollTo = parent.offset().top - 75;
        }

        $('html, body').animate({ scrollTop: scrollTo + 'px'}, 500);

        collapsible.slideToggle(500, function () {
            var visible = collapsible.is(":visible");
            
            if(visible)
            {
                collapsible.find('.up-arrow').show();
                parent.find('.close.button').text('Close');
            }
            else
            {
                collapsible.find('.up-arrow').hide();
                parent.find('.close.button').text('Read more');
            }
        });
    });
});

    //console.log('run ')
showSubmenu();
//openCaseArticle();
checkPasswordStrength();
openPublication();
officeNavigation();
contactValidation();
pageFormValidation();
showTransparentbg();
displayVideo();
mobileTT();
getSelectVal();
openoverlay();
openoverlayLogin();
openoverlaychange();
login();
loginformbuttons();

if (document.location.hash) {

    var target = document.location.hash;
    var $target = $(target);

    if($target.length){

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top-96
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    }
}

var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

});
