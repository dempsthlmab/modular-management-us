
jQuery(document).ready(function( $ ) {
    var tag = 'ga-listen';

    function send(category, action, label) {
        ga('send', {
            hitType: 'event',
            eventCategory: category,
            eventAction: action,
            eventLabel: label
        });
    }

    function register($e, category, action, label) {
        $e.each(function() {
            $this = $(this);
            if($this.hasClass(tag)) {
                return;
            }

            var l = label;
            if(typeof label === 'function') {
                l = label($this);
            }

            $this.addClass(tag);
            $this.attr('ga-category', category);
            $this.attr('ga-action', action);
            $this.attr('ga-label', l);
        });
    }

    function niceString(str) {
        var value = str.trim().replace(/(\r\n|\n|\r)/gm," ");
        if(value.length > 40) {
            value = value.substring(0, 40);
        }

        return value;
    }

    function getText($e) {
        return niceString($e.text());
    }

    function getHref($e) {
        return niceString($e.attr('href'));
    }

    function bindAnalytics() {
        register($('.submit-wrapper > button'), 'Contact Form', 'Submit', 'Send');
        register($('#top-nav a'), 'Top Nav', 'Click', getText);
        register($('#lang-cta a'), 'Language Bar', 'Click', getHref);
        register($('.content-row a'), 'Link', 'Click', getText);

        if(location.pathname.indexOf('story') > 0) {
            var header = document.title;
            var split = header.split(' ');
            if(split.length > 1) {
                header = split[0];
            }

            $('#footer form button[type=submit]').attr('ga-category', 'Contact Form ' + header);
        }

        $(document).on('mouseup', '.' + tag, function(e) {
            if(e.which === 3) {
                return;
            }

            var $this = $(this);

            send(
                $this.attr('ga-category') || '',
                $this.attr('ga-action') || '',
                $this.attr('ga-label') || ''
            );
        });

        $('#searchform').on('submit', function() {
            send('Search', 'Submit', $(this).find('#s').val());
        });

        ga('send', 'pageview', location.pathname + location.search  + location.hash);
    }
    bindAnalytics();
});