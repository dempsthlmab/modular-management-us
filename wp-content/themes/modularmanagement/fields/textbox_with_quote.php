<?php
	$text = get_sub_field('text');
	$quote = get_sub_field('quote');
	$color = get_sub_field('background_color');
?>

<div class="textbox">
	<div class="cell text">
		<?php echo $text ?>
	</div>

	<div class="cell quote" style="background: <?php echo $color ?>">
		<?php echo $quote ?>
	</div>
</div>