<?php

$content = get_sub_field('content');
$quote = get_sub_field('quote');
$image = get_sub_field('image');
$img_pos = get_sub_field('img_pos');
$show_contact_form = get_sub_field('show_contact_form');
$contactform = get_sub_field('contact_form');

$image_position = get_sub_field('image_position');
$divider_on_bottom = get_sub_field('divider_on_bottom');

$ctitle = get_field('contact_form_title','option');
$citalic = get_field('contact_form_italic','option');
$ccontent = get_field('contact_form_content','option');

$form_title = (!empty($ctitle) ? get_field('contact_form_title','option') : 'Contact');
$form_italic = (!empty($citalic) ? get_field('contact_form_italic','option') : 'Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.');
$form_content = (!empty($ccontent) ? get_field('contact_form_content','option') : 'You can also contact us by sending an e-mail to:<a href="mailto:info@'.current_domain().'">info@'.current_domain().'</a>');

$border = '';
if($divider_on_bottom == true){
    $border = 'border';
}

$background = '#FAFAFA';
if($show_contact_form == 'true'){
    $background = '#494949';
}

 ?>

    <div class="article <?php echo $img_pos . ' '. $border;?>" style="background: <?php echo $background; ?>;">

        <div class="article-text big-article" style="background: #fff;">
            <div class="text-block">

                <?php echo $content; ?>

            </div>
        </div>

        <?php if(!empty($image) && $image_position == 'top'): ?>
        <div style="height: auto;" class="article-image">
            <img style="position: relative; top: auto;" src="<?php echo $image; ?>">
        </div>
        <?php endif; ?>



        <?php if(!empty($quote)): ?>
            <div class="article-small-quote">
                <div class="italic-bold-title">
                    <?php echo $quote; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if($show_contact_form == 'true'): ?>

        <div class="form-column">

            <div class="form-article">
                <div class="inner">
                    <div class="text-content">
                        <div class="form-title"><?php echo $form_title; ?></div>
                        <div class="form-italic"><?php echo $form_italic; ?></div>
                    </div>
                    <div class="form-content">

                        <?php echo do_shortcode('[contact-form-7 id="'.$contactform->ID.'"]'); ?>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
          </div>
        <?php endif; ?>

        <?php if(!empty($image) && $image_position == 'bottom'): ?>
            <div class="article-image">
                <img src="<?php echo $image; ?>">
            </div>
        <?php endif; ?>


        <div class="clear"></div>

    </div>
