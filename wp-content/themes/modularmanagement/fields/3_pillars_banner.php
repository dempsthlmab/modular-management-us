<?php
/**
 * The content here is from advanced custom fields and are available in the ui under "options" in the sidebar
 *
 * The fields codes and names are available in acf.php the current theme folder.
 */
?>

<div id="top-articles">

    <div id="top-article-title">
        <?php the_field('pillar_title','option'); ?>
    </div>

    <?php $count = 0; ?>
    <?php while( have_rows('pillars', 'option') ): the_row(); ?>

        <div class="top-link <?php echo ($count == 1 ? 'middle-link' : ''); ?>">
            <div class="navlink-quote" data-id="quotes-<?php echo $count; ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/img/tt.png">
            </div>
            <a class="top-link" href="<?php the_sub_field("page"); ?>">

                <div class="top-link-img-wrap">
                    <img src="<?php the_sub_field('image'); ?>" />
                </div>
                <p class="link-title"><?php the_sub_field('title'); ?></p>

                <p class="blue"><?php the_sub_field('sub_title'); ?></p>

                <div class="quotes" id="quotes-<?php echo $count; ?>">

                    <img src="<?php echo get_template_directory_uri(); ?>/img/question.png">
                    <span class="bold"><?php the_sub_field('question_title'); ?></span>
                    <div>
                        <?php the_sub_field('question_1'); ?>
                    </div>
                    <div>
                        <?php the_sub_field('question_2'); ?>
                    </div>
                    <div>
                        <?php the_sub_field('question_3'); ?>
                    </div>
                </div>  

                <?php if($count == 1): ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/plus.png" class="plus right">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/plus.png" class="plus left">
                <?php endif; ?>
            </a>
        </div>
    <?php $count++; ?>
<?php endwhile; ?>


    

    <?php 
    /*
        This is the original content. Currently not in use
    
   ?>
    <div class="top-link">
        <div class="navlink-quote" data-id="quotes-2">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tt.png">
        </div>
        <a class="top-link middle-link" href="<?php echo get_sub_field("link_2"); ?>" id="cui">

            <div class="top-link-img-wrap">
                <img src="<?php echo get_template_directory_uri(); ?>/img/top-links/ci.png">
            </div>
            <p class="link-title"><?php _e('Customer Intimacy'); ?></p>

            <p class="blue"><?php _e('Increasing market share and customer loyalty'); ?></p>

            <div class="quotes" id="quotes-2">

                <img src="<?php echo get_template_directory_uri(); ?>/img/question.png">
                <span class="bold"><?php _e('Do you want to:'); ?></span>
                <div id="cui-1">
                    <?php _e('Widen your assortment?'); ?>
                </div>
                <div id="cui-2">
                    <?php _e('Accelerate customer adaptation?'); ?>
                </div>
                <div id="cui-3">
                    <?php _e('Increase precision in your<br>product offering?'); ?>
                </div>
            </div>


     
        </a>
    </div>

    <div class="top-link middle-link">
        <div class="navlink-quote" data-id="quotes-3">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tt.png">
        </div>
        <a class="top-link" href="<?php echo get_sub_field("link_3"); ?>" id="pld">
            <div class="top-link-img-wrap">
                <img src="<?php echo get_template_directory_uri(); ?>/img/top-links/pl.png">
            </div>
            <p class="link-title"><?php _e('Product Leadership'); ?></p>

            <p class="blue"><?php _e('Creating superior products and brand image'); ?></p>

            <div class="quotes" id="quotes-3">
                <img src="<?php echo get_template_directory_uri(); ?>/img/question.png">
                <span class="bold"><?php _e('Do you want to:'); ?></span>
                <div id="pld-1">
                    <?php _e('Control R&D spending?'); ?>
                </div>
                <div id="pld-2">
                    <?php _e('Reduce time to market?'); ?>
                </div>
                <div id="pld-3">
                    <?php _e('Improve performance of your<br>product offering?'); ?>
                </div>
            </div>

            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/plus.png" class="plus right">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/plus.png" class="plus left">


        </a>
    </div>

    <div class="top-link">
        <div class="navlink-quote" data-id="quotes-1">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tt.png">
        </div>
        <a class="top-link" href="<?php echo get_sub_field("link_1"); ?>" id="opx">
          
            <div class="top-link-img-wrap">
                <img src="<?php echo get_template_directory_uri(); ?>/img/top-links/oe.png">
            </div>
            <p class="link-title"><?php _e('Operational Excellence'); ?></p>
            
            <p class="blue"><?php _e('Achieving high productivity and quality'); ?></p>

            <div class="quotes" id="quotes-1">
                <img src="<?php echo get_template_directory_uri(); ?>/img/question.png">
                <span class="bold"><?php _e('Do you want to:'); ?></span>
                <div id="opx-1">
                    <?php _e('Achieve economies of scale?'); ?>
                </div>
                <div id="opx-2">
                    <?php _e('Stabilize your processes?'); ?>
                </div>
                <div id="opx-3">
                    <?php _e('Improve quality?'); ?>
                </div>
            </div>

        </a>
    </div>
    */ ?>


</div>