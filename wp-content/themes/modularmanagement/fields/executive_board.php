<div id="board-nav">

<p class="title"><?php _e('Executive Board','modman'); ?></p>

<?php 

$bm = get_sub_field('board_members'); 

foreach($bm as $b):
 ?>                   

    <div class="board-member">
        <div class="rounded-image-wrapper">
            <img src="<?php echo $b['image']; ?>">
        </div>
        <p class="board-name"><?php echo $b['name']; ?></p>
        <p class="board-title"><?php echo $b['title']; ?></p>
    </div>

<?php endforeach; //End boardmembers ($bm) ?>

</div>