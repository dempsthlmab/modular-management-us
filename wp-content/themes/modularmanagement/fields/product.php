<?php 

$logo = get_sub_field('logo');
$text_pos = get_sub_field('text_position');
$list = get_sub_field('list');
$list_background = get_sub_field('list_background');
$list_or_logo = get_sub_field('list_or_logo');

if($list_or_logo == 'logo'){
    $list_background = '#fff';
}
//var_dump($logo);

?>

<div class="article <?php echo $text_pos; ?>" style="background: <?php echo $list_background; ?>">
                    
    <div class="article-text blue-top">
        <div class="product-title"><?php echo get_sub_field('title'); ?></div>
        <div class="italic-title"><?php echo get_sub_field('slogan'); ?></div>

        <?php echo get_sub_field('product_description'); ?>
    </div>

    <?php if($list_or_logo == 'logo'): ?>

        <div class="article-content palma">

            <img class="palma-logo" src="<?php echo $logo['sizes']['thumbnail']; ?>">
            <p class="italic-bold-title"><?php echo get_sub_field('quote'); ?></p>
        </div>
    <?php else: ?>
         <div class="article-content product-list palma">

            <?php echo newline_to_li(get_sub_field('list')); ?>
        </div>
    <?php endif; ?>

    <div class="clear"></div>

</div>