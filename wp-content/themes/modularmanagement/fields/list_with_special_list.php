<?php
	$title = get_sub_field('title');
	$icon = get_sub_field('icon');
	$title2 = get_sub_field('special_list_title');
?>

<div class="list right">
	<div class="cell content">
		<div class="title">
			<div class="icon">
				<?php echo wp_get_attachment_image($icon['id'], 'icon') ?>
			</div>

			<div>
				<?php echo $title ?>
			</div>
		</div>

		<ul>
		<?php if( have_rows('items') ): while(have_rows('items')): the_row(); ?>
			<li>
				<?php the_sub_field('text') ?>
			</li>
		<?php endwhile; endif ?>
		</ul>
	</div>

	<div class="cell special">
		<div class="title">
			<?php echo $title2 ?>
		</div>

		<table>
		<?php if( have_rows('special_list_content') ): while(have_rows('special_list_content')): the_row(); ?>
			<tr>
				<th class="left">
					<?php the_sub_field('text_left') ?>
				</th>

				<td class="right">
					<?php the_sub_field('text_right') ?>
				</td>
			</tr>
		<?php endwhile; endif ?>
			<div class="clear"></div>
		</table>
	</div>
</div>