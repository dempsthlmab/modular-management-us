<?php
$title = get_sub_field('title');
$intro = get_sub_field('introduction');
$image = get_sub_field('image');
$case = get_sub_field('case');
?>
<div class="static-case" style="background: url('<?php echo $image; ?>'); background-size: cover; background-position: center center;">
	<div class="static-case-content">
		<div class="inner">
			<p class="case-title"><?php echo $title; ?></p>
			<p class="case-italic"><?php echo $intro; ?></p>
			<a href="<?php echo get_site_url().'/track-record/#' . sanitize_title($case->post_title); ?>" class="cta-button">
				<span class="read-more-t"><?php _e('Read more','modman'); ?></span>
			</a>
		</div>
	</div>

</div>