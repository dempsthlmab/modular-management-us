<?php
	$title = get_sub_field('title');
	$icon = get_sub_field('icon');
	$title2 = get_sub_field('title2');
	$icon2 = get_sub_field('icon2');
?>

<div class="list right">
	<div class="cell content">
		<div class="title">
			<div class="icon">
				<?php echo wp_get_attachment_image($icon['id'], 'icon') ?>
			</div>

			<div>
				<?php echo $title ?>
			</div>
		</div>

		<ul>
		<?php if( have_rows('items') ): while(have_rows('items')): the_row(); ?>
			<li>
				<?php the_sub_field('text') ?>
			</li>
		<?php endwhile; endif ?>
		</ul>
	</div>

	<div class="cell content">
		<div class="title">
			<div class="icon">
				<?php echo wp_get_attachment_image($icon2['id'], 'icon') ?>
			</div>

			<div>
				<?php echo $title2 ?>
			</div>
		</div>

		<ul>
		<?php if( have_rows('items2') ): while(have_rows('items2')): the_row(); ?>
			<li>
				<?php the_sub_field('text') ?>
			</li>
		<?php endwhile; endif ?>
		</ul>
	</div>
</div>