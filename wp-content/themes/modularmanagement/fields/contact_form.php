<?php $field = get_sub_field('contact_form'); ?>

<?php
    $form_title_field = get_field('contact_form_title','option');
    $form_title = (empty($form_title_field)) ? 'Contact' : $form_title_field;
    
    $form_italic_field = get_field('contact_form_italic','option');
    $form_italic = (empty($form_italic_field)) ? 'Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.' : $form_italic_field;

    $form_content_field = get_field('contact_form_content','option');
    $form_content = (empty($form_content_field)) ? 'You can also contact us by sending an e-mail to:<a href="mailto:info.americas@modularmanagement.com">info.americas@modularmanagement.com</a>' : $form_content_field; 
?>

<div class="form-article">
    <div class="inner">
        <div class="text-content">
            <div class="form-title"><?php echo $form_title; ?></div>
            <div class="form-italic"><?php echo $form_italic; ?></div>

            <div class="form-text">
                <?php echo $form_content; ?>
            </div>
        </div>
        <div class="form-content">

            <?php echo do_shortcode('[contact-form-7 id="'.$field->ID.'"]'); ?>
            <!--<form>

                <div class="top-input-wrapper">
                    <div class="top-input">
                        <input type="text" placeholder="Name">
                    </div>
                    <div class="top-input">
                        <input type="text" placeholder="Phone">
                    </div>
                    <div class="clear"></div>
                </div>  
                <div class="email-wrapper">
                    <input type="email" placeholder="Email">
                </div>
                <div class="submit-wrapper">    
                    <button type="submit">SEND</button>
                </div>
            </form>-->
        </div>
        <div class="clear"></div>
    </div>
</div>