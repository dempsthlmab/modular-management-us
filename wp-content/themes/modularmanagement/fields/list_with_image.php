<?php
	$title = get_sub_field('title');
	$icon = get_sub_field('icon');
	$left = get_sub_field('left');
	$image = get_sub_field('image');
?>

<?php if($left): ?>
	<div class="list left">
		<div class="cell">
			<?php echo wp_get_attachment_image($image['id'], 'icon') ?>
		</div>
<?php else: ?>
	<div class="list right">
<?php endif ?>
		<div class="cell content">
			<div class="title">
				<div class="icon">
					<?php echo wp_get_attachment_image($icon['id'], 'icon') ?>
				</div>

				<div>
					<?php echo $title ?>
				</div>
			</div>

			<ul>
			<?php if( have_rows('items') ): while(have_rows('items')): the_row(); ?>
				<li>
					<?php the_sub_field('text') ?>
				</li>
			<?php endwhile; endif ?>
			</ul>
		</div>
<?php if(!$left): ?>
		<div class="cell">
			<?php echo wp_get_attachment_image($image['id'], 'large') ?>
		</div>
<?php endif ?>
	</div>
