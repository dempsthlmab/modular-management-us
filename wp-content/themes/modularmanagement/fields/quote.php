<?php

$title = get_sub_field('title');
$italic = get_sub_field('italic');
$quote = get_sub_field('quote');

?>

 <div class="qoute-section">
    <div class="inner-section">
        <?php if(!empty($title)): ?>
            <h1 class="qoute-title">
                <?php echo $title; ?>
            </h1>
        <?php endif ?>

        <?php if(!empty($italic)): ?>
            <p class="italic"><?php echo $italic; ?></p>
        <?php endif ?>

        <?php if(!empty($quote)): ?>
        <p class="text">
            <?php echo $quote; ?>
        </p>
        <?php endif ?>
    </div>
</div>