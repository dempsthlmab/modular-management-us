<?php

$publications = get_sub_field('pick_publications');

//Divide them
$articles = array();
$books = array();
$research = array();
$ex_research = array();

foreach($publications as $p){

    if($p['publication']->post_type == 'article'){
        $articles[] = $p['publication'];
    }

    if($p['publication']->post_type == 'book'){
        $books[] = $p['publication'];
    }

    if($p['publication']->post_type == 'research'){
        $research[] = $p['publication'];
    }

    if($p['publication']->post_type == 'ex_research'){
        $ex_research[] = $p['publication'];
    }
}

?>

<div id="publications">

    <div class="left-column">

        <div class="publication">
            <div class="publication-content">                
                
                <strong><?php _e('Articles'); ?></strong>
                <p class="italic"><?php _e('- Articles by Modular Management associates.','modman'); ?></p>
                
                <!-- 
                    data id on button to know wich article to open
                -->

                <span class="cta-button case-button" data-id="article">
                    <span class="read-more-t"><?php _e('Read more','modman'); ?></span>
                    <span class="close-t"><?php _e('Close','modman'); ?></span>
                </span>

                <div class="full-publication" id="article">

                    <?php foreach($articles as $a): ?>

                    <?php //var_dump($a); ?>

                        <div class="pdf-publication">

                            <p class="bold"><?php echo $a->post_title; ?></p>
                            <p class="light"><?php _e('By'); ?> <?php echo get_field('author',$a->ID); ?></p>
                            <p class="light">
                                <?php echo $a->post_excerpt; ?>
                            </p>

                            <?php if(!is_user_logged_in() && $a->post_status == 'private'): ?>
                                 <span class="open-overlay"><?php _e(str_replace(' >','','Download the full article (requires free registration >)'),'modman'); ?> > </span>
                            <?php else: ?>
                               
                                <p>                 
                                    <a href="<?php echo get_post_permalink($a->ID); ?>" target="_blank"><?php _e('Read full article','modman'); ?> </a> 

                                    <?php if(get_field('pdf',$a->ID)): ?>
                                        or <a href="<?php echo get_field('pdf',$a->ID); ?>" target="_blank"><?php _e('Download','modman'); ?></a>
                                    <?php endif; ?> 
                                </p>
                            <?php endif; ?>

                        </div>
                    
                    <?php endforeach; ?>
                        <div class="full-publication-footer">
                            <div class="close-article">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
                            </div>
                        </div>

                </div>
                
            </div>

        </div>

        <div class="publication grey">
            <div class="publication-content">                
                
                <strong><?php _e('Research'); ?></strong>
                <p class="italic"><?php _e('- Research papers and theses by Modular Management associates.','modman'); ?></p>
                
                <!-- 
                    data id on button to know wich article to open
                -->

                <span class="cta-button case-button" data-id="research">
                    <span class="read-more-t"><?php _e('Read more','modman'); ?></span>
                    <span class="close-t"><?php _e('Close','modman'); ?></span>
                </span>

                <div class="full-publication" id="research">

                    <?php foreach($research as $a): ?>

                    <div class="pdf-publication">

                            <p class="bold"><?php echo $a->post_title; ?></p>
                            <p class="light"><?php _e('By'); ?> <?php echo get_field('author',$a->ID); ?></p>
                            <p class="light">
                                <?php echo $a->post_excerpt; ?>
                            </p>

                            <?php if(!is_user_logged_in() && $a->post_status == 'private'): ?>
                                 <span class="open-overlay"><?php _e(str_replace(' >','','Download the full research (requires free registration >)'),'modman'); ?> > </span>
                            <?php else: ?>
                               
                                <p>
                                    
                                    <a href="<?php echo get_post_permalink($a->ID); ?>" target="_blank"><?php _e('Read full research','modman'); ?> </a> 
                                    <?php if(get_field('pdf',$a->ID)): ?>
                                        or <a href="<?php echo get_field('pdf',$a->ID); ?>" target="_blank"><?php _e('Download','modman'); ?></a>
                                    <?php endif; ?> 
                                </p>
                            <?php endif; ?>

                        </div>
                    
                    <?php endforeach; ?>
                        <div class="full-publication-footer">
                            <div class="close-article">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
                            </div>
                        </div>

                </div>
                
            </div>

        </div>

    </div>

    <div class="right-column">
        
        <div class="publication grey">
            <div class="publication-content">                
                
                <strong><?php _e('External Research'); ?></strong>
                <p class="italic"><?php _e('- External Research papers and theses connected to Modular Management associates.','modman'); ?></p>
                
                <!-- 
                    data id on button to know wich article to open
                -->

                <span class="cta-button case-button" data-id="ex_research">
                    <span class="read-more-t"><?php _e('Read more','modman');?></span>
                    <span class="close-t"><?php _e('Close','modman'); ?></span>
                </span>

                <div class="full-publication" id="ex_research">

                    <?php foreach($ex_research as $a): ?>

                    <div class="pdf-publication">

                            <p class="bold"><?php echo $a->post_title; ?></p>
                            <p class="light"><?php _e('By','modman'); ?> <?php echo get_field('author',$a->ID); ?></p>
                            <p class="light">
                                <?php echo $a->post_excerpt; ?>
                            </p>

                            <?php if(!is_user_logged_in() && $a->post_status == 'private'): ?>
                                 <span class="open-overlay"><?php _e(str_replace(' >','','Download the full external research (requires free registration >)'),'modman'); ?> > </span>
                            <?php else: ?>
                               
                                <p>
                                    
                                    <a href="<?php echo get_post_permalink($a->ID); ?>" target="_blank"><?php _e('Read research','modman'); ?> </a> 
                                    <?php if(get_field('pdf',$a->ID)): ?>
                                        or <a href="<?php echo get_field('pdf',$a->ID); ?>" target="_blank"><?php _e('Download','modman'); ?></a>
                                    <?php endif; ?> 
                                </p>
                            <?php endif; ?>

                        </div>
                    
                    <?php endforeach; ?>
                        <div class="full-publication-footer">
                            <div class="close-article">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
                            </div>
                        </div>

                </div>
                
            </div>

        </div>

        <div class="publication">
            <div class="publication-content">                
                
                <strong><?php _e('Books'); ?></strong>
                <p class="italic"><?php _e('- Books recommended by Modular Management associates.'); ?></p>
                
                <!-- 
                    data id on button to know wich article to open
                -->

                <span class="cta-button case-button" data-id="book">
                    <span class="read-more-t"><?php _e('Read more','modman'); ?></span>
                    <span class="close-t"><?php _e('Close','modman'); ?></span>
                </span>

                <div class="full-publication" id="book">

                    <?php foreach($books as $a): ?>

                    <div class="pdf-publication">

                        <p class="bold"><?php echo $a->post_title; ?></p>
                        <p class="light"><?php _e('By','modman'); ?> <?php echo get_field('author',$a->ID); ?></p>
                        <p class="light">
                            <?php echo $a->post_excerpt; ?>
                        </p>

                        <?php if(!is_user_logged_in() && $a->post_status == 'private'): ?>
                             <span class="open-overlay"><?php _e(str_replace(' >','','Download the full book (requires free registration >)'),'modman'); ?> > </span>
                        <?php else: ?>
                           
                            <p>
                                
                                <a href="<?php echo get_post_permalink($a->ID); ?>" target="_blank"><?php _e('Read about the book','modman'); ?> </a>
                                <?php if(get_field('pdf',$a->ID)): ?>
                                    or <a href="<?php echo get_field('pdf',$a->ID); ?>" target="_blank"><?php _e('Download','modman'); ?></a>
                                <?php endif; ?> 
                            </p>
                        <?php endif; ?>

                    </div>
                    
                    <?php endforeach; ?>
                        <div class="full-publication-footer">
                            <div class="close-article">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
                            </div>
                        </div>

                </div>
                
            </div>

        </div>
    </div>

    

    <div class="clear"></div>

</div>