<?php 
$img_pos = get_sub_field('img_pos');
$image = get_sub_field('image');
$background_color = get_sub_field('background_color');
$white_text = get_sub_field('white_text');
$read_more = get_sub_field('read_more');
$content = get_sub_field('content');

$dark_background = $white_text ? 'dark' : '';
?>
<div class="article <?= $img_pos; ?>" style="background:<?= $background_color; ?>">
    <div class="article-link">
        <div class="article-image">
            <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
        </div>
        <div class="article-content <?= $img_pos; ?> <?= $dark_background; ?>">
            <?= $content; ?>
            <a class="readmore" href="<?= $read_more; ?>"><div class="read-more-t"><?php _e('Read more','modman'); ?> <span>></span></div></a>
        </div>
        <div class="clear"></div>
    </div>
</div>