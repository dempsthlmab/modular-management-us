<?php 

$youtube_id = get_sub_field('youtube_id');
$title = get_sub_field('title');
$sub_title = get_sub_field('sub_title');
$image = get_sub_field('image');

?>

<div class="static-case" id="video-module">
    
    <div class="video-background">
        <div class="video-wrapper">
            <iframe type="text/html"
            src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>?enablejsapi=1&rel=0&showinfo=0"frameborder="0" allowfullscreen></iframe>
        </div>
    </div>        

    <div class="static-case-content" id="static-video-content" style="background: url('<?php echo $image; ?> background-size: cover; background-position: center center;">
        <div class="inner">
            <p class="case-title"><?php echo $title; ?></p>
            <p class="case-italic"><?php echo $sub_title; ?></p>
            <span class="cta-button" id="display-video"><?php _e('Watch the Video','modman'); ?></span>
        </div>
    </div>

</div>