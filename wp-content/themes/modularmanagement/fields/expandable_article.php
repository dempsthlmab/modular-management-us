<?php
$title = get_sub_field('title');
$content = get_sub_field('content');
$image = get_sub_field('image');
$quote = get_sub_field('quote');
$image_quote = get_sub_field('image_or_quote');
$ex_title = get_sub_field('expandable_title');
$ex_content = get_sub_field('expandable_content');
$ex_image_top = get_sub_field('expandable_image_top');
$ingress = get_sub_field('ingress');
$ex_image_bottom = get_sub_field('expandable_image_bottom');
$always_open = get_sub_field('always_open');

$active = '';
$display = '';
if($always_open){
    $active = 'active';
    $display = 'style="display: block;"';
}

?>

<div class="case-article">
<div class="article right">

    <div class="article-content">
        <strong style="margin-top: 7%;"><?php echo $title; ?></strong>
         <div style="background: #fff; min-height: 100%;">
            <?php echo $content; ?>
        </div>

        <!--
            data id on button to know wich article to open
        -->
        <?php if(!$always_open): ?>
            <span class="cta-button case-button" data-id="ex-article-<?php echo sanitize_title($title); ?>">
                <div class="read-more-t"><?php _e('Read more','modman'); ?></div>
                <div class="close-t"><?php _e('Close','modman'); ?></div>
            </span>
        <?php endif; ?>
    </div>

    <?php if($image_quote == 'quote'): ?>

        <div class="article-small-quote">
            <div class="italic-bold-title">
                <?php echo $quote; ?>
            </div>
        </div>

    <?php else: ?>
        <div class="article-image">
            <img src="<?php echo $image; ?>">
        </div>
    <?php endif; ?>




    <div class="clear"></div>
</div>

<!-- Expanded article with id -->

<div class="full-case <?php echo $active; ?>" id="ex-article-<?php echo sanitize_title($title); ?>" <?php echo $display; ?>>
    <div class="left-column">

        <div>
            <img src="<?php echo $ex_image_top; ?>">
        </div>

        <div class="left-case-desc">
            <?php echo $ingress; ?>
        </div>

        <div>
            <img src="<?php echo $ex_image_bottom; ?>">
        </div>

    </div>

    <div class="right-column">

        <p class="case-title italic"><?php echo $ex_title; ?></p>

        <div class="right-case-desc">

            <?php echo $ex_content; ?>
        </div>

        <div class="case-footer">

        <?php if(!$always_open): ?>
            <div class="close-article">
                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
            </div>
        <?php endif; ?>

        </div>

    </div>
    <div class="clear"></div>

</div>
</div>
