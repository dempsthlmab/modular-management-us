<?php
	$text = get_sub_field('text');
?>

<div class="<?php echo $classname ?> textbox padded">
	<?php echo $text ?>
</div>