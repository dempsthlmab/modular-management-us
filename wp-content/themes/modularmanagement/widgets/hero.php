<?php
	$image_src = get_sub_field('image');
	$image = wp_get_attachment_image_src($image_src['id'], 'Header Full');
?>

<div class="hero-image" style="background: url('<?php echo $image[0]; ?>'); background-size: cover; background-position: center center;">
    <img style="width: 100%; height: auto;" src="<?php echo $image[0]; ?>" alt="<?php echo $image_src['alt']; ?>">
</div>
