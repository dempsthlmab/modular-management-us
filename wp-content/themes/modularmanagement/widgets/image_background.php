<?php
	$image = get_sub_field('image');
	$resize= get_sub_field('resize');
?>

<div class="<?php echo $classname ?> image background" style="background: transparent url('<?php echo wp_get_attachment_image_src($image['id'], $resize)[0] ?>'); background-size: cover; background-position: center center">
	<img src="<?php echo wp_get_attachment_image_src($image['id'], $resize)[0] ?>" alt="<?php echo $image['alt'] ?>">
</div>