<?php
$case = get_sub_field('case');
$field = get_fields($case->ID);

$image = $field['image'];
$image2 = $field['left_image'];

$story = $field['full_case'];
?>

<div class="business-story story expandable" id="<?= sanitize_title($case->post_title) ?>">
	<div class="story top">
		<div class="cell item-1 top-image">
			<img src="<?= wp_get_attachment_image_src($image['id'], 'article')[0] ?>" alt="<?= $image['alt'] ?>">
		</div>

		<div class="cell item-2">
			<h2><?= $case->post_title ?></h2>
			<div class="subtitle"><?= $field['subtitle'] ?></div>

			<div class="button-group">
				<div class="close button">Read More</div>

				<?php if($story): ?>
					<a href="<?= get_post_permalink($story->ID) ?>">
						<div class="button">Read full article</div>
					</a>
				<?php endif ?>
			</div>

			<div class="content">
				<p class="title"><?php _e('Measurable Improvements','modman'); ?></p>
				<ul>
					<?= newline_to_li($field['modularity_results']); ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="story bottom">
		<div class="collapse" style="display: none">
			<div class="cell item-1">
				<?php if($image2): ?>
					<img class="bottom-image" src="<?= wp_get_attachment_image_src($image2['id'], 'article')[0] ?>" alt="<?= $image2['alt'] ?>">
				<?php endif ?>
			</div>

			<div class="cell item-2">
				<div class="content">
					<div class="content-title subtitle desktop"><?= $field['content_header'] ?></div>

					<p class="title"><?php _e('Business Situation','modman'); ?></p>
					<ul>
                        <?= newline_to_li($field['measurable_improvements']); ?>
                    </ul>

					<p class="title"><?php _e('Modularity Results','modman'); ?></p>
					<ul>
                        <?= newline_to_li($field['business_situation']); ?>
                    </ul>
				</div>
			</div>

			<div class="content">
				<p class="title"> <?php _e('Company Description','modman'); ?></p>
				<?= $field['company_description'] ?>
				<div class="content-title subtitle mobile" style="margin-top: 1em"><?= $field['content_header'] ?></div>
				<div class="hr"></div>
			</div>

			<div class="close up-arrow" style="display: none">
				<div class="arrow"></div>
			</div>
		</div>
	</div>
</div>