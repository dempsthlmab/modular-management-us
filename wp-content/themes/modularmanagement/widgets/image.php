<?php
	$image = get_sub_field('image');
	$width = get_sub_field('width');
	$resize= get_sub_field('resize');
?>

<div class="<?php echo $classname ?> image">
	<img src="<?php echo wp_get_attachment_image_src($image['id'], $resize)[0] ?>" style="<?php if($width): ?> width: <?php echo $width ?>% <?php endif ?>" alt="<?php echo $image['alt'] ?>">
</div>