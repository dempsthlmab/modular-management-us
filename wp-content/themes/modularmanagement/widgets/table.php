<?php
	$title = get_sub_field('title');
	$theme = get_sub_field('theme');
?>

<div class="<?php echo $classname?> <?php echo $theme ?> table padded">
	<div class="title">
		<?php echo $title ?>
	</div>

	<table>
	<?php if( have_rows('items') ): while(have_rows('items')): the_row(); ?>
		<tr>
			<th class="left"><?php the_sub_field('left_text') ?></th>
			<td class="right"><?php the_sub_field('right_text') ?></td>
		</tr>
	<?php endwhile; endif ?>
	</table>
</div>