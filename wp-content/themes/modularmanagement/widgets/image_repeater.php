<?php
	$images = get_sub_field('images');
	$width = 100 / $children / count($images);
?>

<?php foreach($images as $value): ?>
	<?php $image = $value['image'] ?>
	<div class="<?php echo $classname ?> image repeater" style="width: <?php echo $width ?>%">
		<img src="<?php echo $image['sizes']['medium'] ?>" alt="<?php echo $image['alt'] ?>">
	</div>
<?php endforeach ?>