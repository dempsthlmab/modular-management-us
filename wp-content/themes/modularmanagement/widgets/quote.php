<?php
	$title = get_sub_field('title');
	$italic = get_sub_field('italic');
	$quote = get_sub_field('quote');
	$centered = get_sub_field('centered') ? "centered" : "";
	$theme = get_sub_field('theme');
?>

 <div class="<?php echo $classname ?> <?php echo $centered ?> <?php echo $theme ?> quote padded">
 	<div class="inner-section">
		<?php if(!empty($title)): ?>
			<h1 class="quote-title">
				<?php echo $title; ?>
			</h1>
		<?php endif ?>

		<?php if(!empty($italic)): ?>
			<p class="italic"><?php echo $italic; ?></p>
		<?php endif ?>

		<?php if(!empty($quote)): ?>
			<div class="text"><?php echo $quote; ?></div>
		<?php endif ?>
	</div>
</div>