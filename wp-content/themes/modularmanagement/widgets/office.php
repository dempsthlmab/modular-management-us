<?php 

    $offices = get_sub_field('offices');

    $defaultlocation = 'sweden-stockholm-office'; //For navigation, make this dynamic later

    $path = 'us';//str_replace('/','',get_blog_details(get_current_blog_id())->path);

    if($path == 'de'){
        $defaultlocation = 'germany-frankfurt-office';
    }elseif($path == 'us'){
        $defaultlocation ='usa-bloomington-office';
    }elseif($path == 'zh'){
        //$defaultlocation = 'beijing-china-office';
        $defaultlocation = 'taiwan-taipei-office';
    }

  

    $office_maps = array( 
        'usa-bloomington-office',
        'sweden-stockholm-office',    
        'germany-frankfurt-office',
        //'beijing-china-office',
        'taiwan-taipei-office',
        'shanghai-china-office',
    );

    $count = 0;

?>

<div id="offices">
    <?php foreach($offices as $o): ?>

       <?php $display = '';

       $o = $o['office'];
       
        $san_title = sanitize_title(get_field('location',$o->ID));

        if($office_maps[$count] == $defaultlocation){
            $display = 'display: block;';
        }

        ?>

        <div id="<?php echo $office_maps[$count]; ?>" style="<?php echo $display; ?>" class="office-wrapper">
            <div class="office-hero">
                <img src="<?php echo get_field('office_hero',$o->ID); ?>">
            </div>

            <div class="office-column">
                <div class="office-manager">

                    <div class="rounded-image-wrapper">
                        <img src="<?php echo get_field('contact_person_image',$o->ID); ?>">
                    </div>

                    <p class="bold-italic"><?php echo get_field('contact_person_name',$o->ID); ?></p>
                    <p class="light"><?php echo get_field('contact_person_title',$o->ID); ?></p>
                
                </div>

                <div class="office-contact-details">

                    <p class="title"><?php echo $o->post_title; ?></p>
                    <p class="italic-title"><?php echo get_field('location',$o->ID); ?></p>

                    <div class="adress">
                        <div class="list-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/pin.png">
                        </div>
                        <ul>
                            <li>Address:</li>
                            <a target="_blank" href="<?php echo get_field('map_url',$o->ID); ?>">
                                <?php echo newline_to_li(get_field('address',$o->ID)); ?>
                            </a>
                        </ul>
                    </div>

                    <div class="phone">
                        <div class="list-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/phone.png">
                        </div>
                        <ul>
                            <li>Tel: <a href="tel:<?php echo get_field('tel',$o->ID); ?>"><?php echo get_field('tel',$o->ID); ?></a></li>
                        </ul>
                    </div>


                    <div class="mail">
                        <div class="list-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/letter.png">
                        </div>
                        <ul>
                            <li>E-mail: <a href="mailto:<?php echo get_field('email',$o->ID); ?>"><?php echo get_field('email',$o->ID); ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--<div class="clear"></div>-->
        </div>
        <?php $count++; ?>
        
    <?php endforeach; ?>
    </div>

    <div id="office-nav" class="<?php echo $defaultlocation; ?>">

    <?php 
    //Navigation
    $count2 = 0;
    foreach($offices as $o): ?>

        <?php 

        $o = $o['office'];
        $active = '';
        if($office_maps[$count2] == $defaultlocation){
            $active = 'active';
        }

        ?>

        <div class="office-navlink <?php echo $active; ?>" data-id="<?php echo $office_maps[$count2]; ?>">
            <img src="<?php echo get_field('location_icon',$o->ID); ?>">
        </div>
        <?php $count2++; ?>
    <?php endforeach; ?>
        
    </div>