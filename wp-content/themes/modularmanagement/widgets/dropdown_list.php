<?php

?>

<div class="<?php echo $classname ?> dropdown-list">
	<?php if( have_rows('list_items') ): while(have_rows('list_items')): the_row(); ?>
		<div class="item collapsible collapsed">
			<div class="title noselect close"><?php the_sub_field('title') ?> <p class="arrow"></p></div>

			<div class="content">
				<?php the_sub_field('content') ?>
			</div>
		</div>
	<?php endwhile; endif ?>
</div>