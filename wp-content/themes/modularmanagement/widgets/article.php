<?php $article_id = get_sub_field('article'); ?>
<?php if(empty($article_id)): ?>
<div class="business-article <?php echo $classname ?>">
	<div class="article">
	</div>
</div>
<?php else: ?>
	<?php $image = get_field('article_image', $article_id); ?>
<div class="business-article <?php echo $classname ?>" id="<?php echo sanitize_title(get_the_title($article_id)) ?>">
	<div class="article">
		<div>
			<div class="image">
				<img src="<?php echo wp_get_attachment_image_src($image['id'], 'article')[0] ?>" alt="<?php echo $image['alt'] ?>">
			</div>

			<div class="content">
				<h2><?php echo get_the_title($article_id) ?></h2>
				<div class="subtitle"><?php echo get_field('subtitle', $article_id) ?></div>
			</div>
		</div>

		<div class="content description">
			<?php echo get_field('description', $article_id) ?>
			
			<a href="<?php echo get_permalink($article_id) ?>" target="_blank" class="ga-listen" ga-category="Articles" ga-action="Click" ga-label="Read full article">
				<div class="button">Read full article</div>
			</a>
		</div>
	</div>
</div>
<?php endif ?>