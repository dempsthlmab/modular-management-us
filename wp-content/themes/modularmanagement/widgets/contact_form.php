<?php
	$field = get_sub_field('contact_form');

    $form_title_field = get_field('contact_form_title','option');
    $form_title = (empty($form_title_field)) ? 'Contact' : $form_title_field;
    
    $form_italic_field = get_field('contact_form_italic','option');
    $form_italic = (empty($form_italic_field)) ? 'Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.' : $form_italic_field;

    $form_content_field = get_field('contact_form_content','option');
    $form_content = (empty($form_content_field)) ? 'You can also contact us by sending an e-mail to:<a href="mailto:info@'.current_domain().'">info@'.current_domain().'</a>' : $form_content_field; 
?>

<div class="form-article">
    <div class="inner">
        <div class="text-content">
            <div class="form-title"><?php echo $form_title; ?></div>
            <div class="form-italic"><?php echo $form_italic; ?></div>

            <div class="form-text">
                <?php echo $form_content; ?>
            </div>

            <a href="https://www.linkedin.com/company/modular-management?trk=hb_tab_compy_id_443462" target="_blank" class="linkedin ga-listen" ga-category="Contact Form" ga-action="Navigate" ga-label="LinkedIn">
                <img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png">
                <span style="color: white"><?php _e('Follow us on Linkedin','modman'); ?></span>
            </a>
        </div>
        <div class="form-content">
            <?php echo do_shortcode('[contact-form-7 id="'.$field.'"]'); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>