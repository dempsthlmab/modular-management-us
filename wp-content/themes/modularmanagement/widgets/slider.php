<?php
$slider = get_sub_field('slider_object');
$sliderid = $slider->ID;

?>

<div class="rslides_container">
    <ul class="rslides">
        <?php
        if( get_field('slider',$sliderid) ):

            while ( has_sub_field('slider',$sliderid) ) :
                $imagefield = get_sub_field('image');
                $image = wp_get_attachment_image_src($imagefield['id'],'header-full');
                $italic = get_sub_field('italic');
                ?>

                    <li style="background: url('<?php echo $image[0]; ?>') no-repeat; background-size: cover; background-position-x: 100%;">
                        <img src="<?php echo $image[0]; ?>" alt="<?php echo $imagefield['alt']; ?>" />
                        <div class="caption-wrapper">
                        <div class="slider-title"><?php echo get_sub_field('title'); ?></div>
                        <p class="slider-italic"><?php echo $italic; ?></p>
                        <a class="cta-button" href="<?php echo get_sub_field('link_url'); ?>"><?php echo get_sub_field('link_title'); ?></a>
                        </div>
                    </li>
            <?php
            endwhile;
        endif;
        ?>

    </ul>
</div>
<div class="clear"></div>
