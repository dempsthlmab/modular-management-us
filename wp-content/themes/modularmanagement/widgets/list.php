<?php
	$title = get_sub_field('title');
	$icon = get_sub_field('icon');
?>

<div class="<?php echo $classname ?> list padded">
	<div class="title">
		<div class="icon">
			<?php echo wp_get_attachment_image($icon['id'], 'icon') ?>
		</div>

		<div>
			<?php echo $title ?>
		</div>
	</div>

	<ul>
	<?php if( have_rows('items') ): while(have_rows('items')): the_row(); ?>
		<li>
			<?php the_sub_field('text') ?>
		</li>
	<?php endwhile; endif ?>
	</ul>
</div>