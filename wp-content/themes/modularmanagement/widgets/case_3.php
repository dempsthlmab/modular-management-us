<?php 
    $case = get_sub_field('case');
    $field = get_fields($case->ID);
?>

<div class="case-article left white expandable" id="<?php echo sanitize_title($case->post_title); ?>">
    <div class="article">
        <div class="article-image">
            <img src="<?php echo wp_get_attachment_image_src($image['id'], 'article')[0] ?>" alt="<?php echo $image['alt'] ?>">
        </div>

        <div class="article-content">
            <strong><?php echo $case->post_title; ?></strong>
            <p class="italic"><?php echo $field['subtitle']; ?></p>

			<div>
				<div class="close button">Read More</div>

				<a href="#">
					<div class="button">Read full article</div>
				</a>
			</div>
        </div>

        <div class="clear"></div>
    </div>

    <div class="full-case collapse" id="case-<?php echo $case->ID; ?>">
        <div class="left-column">
            <div>
				<?php if(image_left): ?>
					<img class="bottom-image" src="<?php echo wp_get_attachment_image_src($image_left['id'], 'article')[0] ?>" alt="<?php echo $image_left['alt'] ?>">
				<?php endif ?>
            </div>
            
            <div class="left-case-desc">
                <p class="title"> <?php _e('Company Description','modman'); ?></p>
                <p><?php echo $field['company_description']; ?></p>
            </div>
        </div>

        <div class="right-column">
            
            <p class="case-title italic"><?php echo $field['expanded_title']; ?></p>

            <div class="right-case-desc">
                
                <div class="ul-wrap">
                    <p class="title"><?php _e('Business Situation','modman'); ?></p>
                    <ul>
                        <?php echo newline_to_li($field['business_situation']); ?>
                    </ul>
                </div>

                <div class="ul-wrap">
                    <p class="title"><?php _e('Modularity Results','modman'); ?></p>
                    <ul>
                        <?php echo newline_to_li($field['modularity_results']); ?>
                    </ul>
                </div>

                <div class="ul-wrap">
                    <p class="title"><?php _e('Measurable Improvements','modman'); ?></p>
                    <ul>
                        <?php echo newline_to_li($field['measurable_improvements']); ?>
                    </ul>
                </div>
            </div>

            <div class="case-footer">
                <div class="close-article">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>