<?php
	$image = get_sub_field('image');
	$title = get_sub_field('title_text');
	$caption = get_sub_field('caption_text');
	$width = get_sub_field('width');
	$resize= get_sub_field('resize');
?>

<div class="<?php echo $classname ?> image">
	<?php if($title): ?>
		<div class="text" style="width: <?php echo $width ?>%">
			<?php echo $title ?>
		</div>
	<?php endif ?>

	<img src="<?php echo wp_get_attachment_image_src($image['id'], $resize)[0] ?>" style="width: <?php echo $width ?>%" alt="<?php echo $image['alt'] ?>">

	<?php if($caption): ?>
		<div class="text" style="width: <?php echo $width ?>%">
			<?php echo $caption ?>
		</div>
	<?php endif ?>
</div>