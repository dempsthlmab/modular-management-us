<?php
$text = get_sub_field('text');
$cta_name = get_sub_field('cta_label');
$cta_link = get_sub_field('cta_link');
?>

<div class="textbox padded cta <?= $classname ?>" id="<?= sanitize_title($cta_name) ?>">
    <?= $text ?>

    <a href="<?= $cta_link ?>"><?= $cta_name ?></a>
</div>