<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/modular.ico?v=3" type="image/x-icon" />

		<link rel="alternate" href="https://www.modularmanagement.com" hreflang="en-eu" />
		<link rel="alternate" href="https://www.modularmanagement.com/sv" hreflang="sv-se" />
		<link rel="alternate" href="https://www.modularmanagement.com/us" hreflang="en-us" />
		<link rel="alternate" href="https://www.modularmanagement.com/de" hreflang="de-de" />
		<link rel="alternate" href="https://www.modularmanagement.com/zh-hans" hreflang="zh-cn" /> <!-- NO HTTPS -->

        <?php wp_head(); ?>

    </head>
	
    <body <?php body_class(); ?>>
		<div id="container" class="container">
			<div id="header">
				<div id="burger-bg">
					<div id="burger-toggle">
						<span></span>
					</div>
				</div>

				<?php
					global $post;

					if($post !== null):
						$post->post_title;
				?>

					<p class="page-title"><?php echo $post->post_title ?></p>

				<?php endif ?>

				<div id="nav">
					<div class="navbar-row first">
						<?php get_search_form() ?>
						<!--<input type="text" class="input-search" id="search-header">-->
						<div>
							<div class="text">CONNECT WITH US:</div>
							
							<a class="email ga-listen" ga-category="Menu" ga-action="Click" ga-label="Mailto" href="mailto:info.americas@modularmanagement.com">E-mail</a> or phone <a class="phone ga-listen" ga-category="Menu" ga-action="Click" ga-label="Telephone" href="tel:+1 952 854 6800">+1 952 854 6800</a> or <a class="hero-linkedin ga-listen" ga-category="Menu" ga-action="Click" ga-label="LinkedIn" href="https://www.linkedin.com/company/modular-management?trk=hb_tab_compy_id_443462" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/header/linkedin.png" alt="Li">LinkedIn</a>
						</div>
						
						<div id="lang-cta">
							<ul id="lang-switch">
								<li>
									<p>Language: </p>
								</li>

								<?php $langs = array(
									array('flag' => 'EN','class' => 'eu','suburl' => '/eu'),
									array('flag' => 'US','class' => 'us active','suburl' => '/us'),
									array('flag' => 'SE','class' => 'sweden','suburl' => '/sv'),
									array('flag' => 'CM','class' => 'china','suburl' => '/zh-hans'),
									array('flag' => 'DE','class' => 'germany','suburl' => '/de'),
								); ?>

								<?php foreach($langs as $l): ?>

								<li>

									<a href="<?= 'https://modularmanagement.com' . $l['suburl'] ?>" class="<?= $l['class'] ?>"><?= $l['flag'] ?></a>
										
										<!--<img src="<?php echo get_template_directory_uri(); ?>/img/header/flags/<?php echo $l['flag']; ?>.png">-->
									<!--</a>-->
								</li>

								<?php endforeach; ?>
							</ul>
						</div>
					</div>

					<div class="navbar-row second">
						<div id="wide-logo">
							<a href="<?php echo home_url(); ?>" class="ga-listen" ga-category="Menu" ga-action="Click" ga-label="Logo">
								<img src="<?php echo get_template_directory_uri(); ?>/img/header/logo-wide.png">
							</a>
						</div>

						<?php mm_top_menu('primary'); ?>
					</div>

					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
