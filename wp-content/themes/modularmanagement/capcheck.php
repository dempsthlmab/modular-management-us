  <?php
    /**
    Maybe this isn't the best way to do thing, but it was the fastest.
    We have ajax powered login-functionallity, perhaps we should build the same for signup?

    */

    include_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins/si-captcha-for-wordpress/captcha/securimage.php');
    $securimage = new Securimage_Captcha_si();

    //error_log($_POST['captcha_code']);

    if ($securimage->check($_POST['captcha_code']) == false) {
      //error_log('error in captcha code');
      echo json_encode(array('captcha'=>false, 'message'=>'Incorrect CAPTCHA'));
    } else {
      //error_log('correct captcha');
      echo json_encode(array('captcha'=>true, 'message'=>'Correct CAPTCHA'));
    }
  ?>
