<?php
/*

*/
	get_header();
?>
	

	<!--
		FORM OVERLAY
	 -->

	 <div id="overlay" class="active">
	 	<div id="form-overlay">
	 		<div id="close-form">
	 			<img src="<?php echo get_template_directory_uri(); ?>/img/close-form.png">
	 		</div>
		 	<div class="inner">
		 		<div id="form-header">
		 			<p class="title">Register</p>
		 			<p class="italic">- Please Register to Access Even More Content</p>
		 		</div>
		 		<form id="signup-form">

		 			<div class="left-form-column">
		 				<span class="required">*Required</span>
			 			<div>
			 				<input name="email" type="email" placeholder="Email*">
			 			</div>
						<div>
							<input name="password" type="password" id="password" placeholder="Password*">
						</div>	 			
			 			<div>
			 				<input name="repeat-password" type="password" placeholder="Repeat Password*">
			 			</div>   
			 				
			 			<div class="figure" id="strength_score">
			 				<span></span>
			 				<p>Password strength</p>
			 			</div>
		 				
		 			</div>

		 			<div class="right-form-column">
			 			<div>
			 				<input name="fname" type="text" placeholder="First Name*">
			 			</div>
			 			<div>
			 				<input name="lname" type="text" placeholder="Last Name*">
			 			</div>
			 			<div>
			 				<input name="company" type="text" placeholder="Company*">
			 			</div>
			 			<div>
			 				<input name="telephone" type="text" placeholder="Telephone*">
			 			</div>
			 		</div>
			 		
			 		<div class="clear"></div>

			 		<div class="form-footer">
			 			<div id="select-wrapper">
							<div id="select-box-wrapper">
					 			<span class="def-value">How did you find us?</span>
					 			
					 			<div class="arrow-wrapper">
					 				<img src="<?php echo get_template_directory_uri(); ?>/img/select-arrow.png">
					 			</div>

					 			<!--<select id="search-options">
					 				<option>Google-1</option>
					 				<option>Google-2</option>
					 				<option>Google-3</option>
					 			</select>-->
				 			</div>
					 		
					 		<div class="check-wrapper">
					 			<input name="robot" type="checkbox" id="c1"/>
								<label for="c1"><span></span></label>
				 			</div>
				 			<span class="robot">I im not a robot*</span>
				 			<div class="clear"></div>
			 			</div>

			 			<div style="text-align: center;">
				 			<button type="submit" class="submit-button cta-button" id="create-account">
				 				create new account
				 			</button>
				 		</div>

			 		</div>

		 		</form>
		 	</div>
	 	</div>
	 </div>


	<div id="content">
		
		<div id="hero-image" style="background: url('<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg'); background-size: cover;">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg">
		</div>

	  	<!-- qoute section -->

	  	<div class="qoute-section">
	  		<div class="inner-section">
		  		<p class="qoute-title">
		  			A Solid Track Record
		  		</p>

		  		<p class="italic">- A Down To Earth Approach</p>

				<p class="text">		
					Modular Management has helped a diverse range of companies from white good manufacturers, through construction and transportation to telecommunications.
					To find out more about the benefits different companies have gained from modularization,
					<a>take a look at the following case stories:</a>
				</p>
			</div>
	  	</div>



	  	<!-- wrap cases -->
	  	<div id="case-articles">


	  		<!-- case -->
	  		<div class="case-article left gray">
	  			<div class="article">
				    <div class="article-image">
				        <img src="<?php echo get_template_directory_uri(); ?>/img/case-article-example.jpg">
				    </div>

				    <div class="article-content">
				    	<strong>Dynapac</strong>
				    	<p class="italic">- Boosts Market Share in Emerging Regions with Global Product Platforms.</p>
						
						<!-- 
							data id on button to know wich article to open
						-->

						<span class="cta-button case-button" data-id="dynapac">Read more</span>
				    </div>
    
			    	<div class="clear"></div>
    			</div>
	  		
	  			<!-- Expanded article with id -->

	  			<div class="full-case" id="dynapac">
	  				<div class="left-column">
	  					
	  					<div>
	  						<img src="<?php echo get_template_directory_uri(); ?>/img/expanded-case-article-example.jpg">
	  					</div>
	  					
	  					<div class="left-case-desc">
	  						<p class="title">Company Description</p>

		  						<p>
									Part of the Atlas Copco Corporation and one of the world’s 
									foremost manufacturers of compaction and paving equipment 
									for road construction. These complex vehicle systems that 
									are assembled from components from multiple suppliers. 
									Competitors include Bomag (Germany), Ingersoll Rand (Ireland) 
									and Caterpillar (USA).
		  						</p>
	  					
	  					</div>

	  				</div>

	  				<div class="right-column">
	  					
	  					<p class="case-title italic">Dynapac Construction Equipment</p>

	  					<div class="right-case-desc">
	  						
	  						<div class="ul-wrap">
	  							<p class="title">Business Situation</p>
	  							<ul>
	  								<li>Single most significant growth opportunity was in developing countries like China and India</li>
	  								<li>Equipment needed to be adapted to local construction techniques and practices</li>
	  								<li>Struggling to meet price points via independent local divisions and ownership of products</li>
	  							</ul>
	  						</div>

	  						<div class="ul-wrap">
	  							<p class="title">Modularity Results</p>
	  							<ul>
	  								<li>Established central product ownership</li>
	  								<li>Increased standard features and options</li>
	  								<li>Reduced time to market for local variants</li>
	  								<li>Company-wide implementation of technology</li>
	  							</ul>
	  						</div>

	 	  					<div class="ul-wrap">
	  							<p class="title">Measurable Improvements</p>
	  							<ul>
	  								<li>6% reduction in direct material cost</li>
	  								<li>30% reduction in total part numbers</li>
	  								<li>15% fewer parts per product</li>
	  								<li>30% reduction in assembly time</li>
	  							</ul>
	  						</div>
	  					</div>

	  					<div class="case-footer">
	  						<span class="open-overlay">Download the full case story (requires free registration >)</span>

	  						<div class="close-article">
	  							<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
	  						</div>

	  					</div>

	  				</div>
	  				<div class="clear"></div>

	  			</div>
	  		</div>
	  	

	  		<!-- case -->
	  		<div class="case-article left">
	  			<div class="article">
				    <div class="article-image">
				        <img src="<?php echo get_template_directory_uri(); ?>/img/case-article-example-2.png">
				    </div>

				    <div class="article-content">
				    	<strong>Guldmann</strong>
				    	<p class="italic">- Establishes a Product Foundation for 20 Years of Sustainable Growth.</p>
						
						<!-- 
							data id on button to know wich article to open
						-->

						<span class="cta-button case-button" data-id="guldmann">Read more</span>
				    </div>
    
			    	<div class="clear"></div>
    			</div>
	  		
	  			<!-- Expanded article with id -->

	  			<div class="full-case" id="guldmann">
	  				<div class="left-column">
	  					
	  					<div>
	  						<img src="<?php echo get_template_directory_uri(); ?>/img/expanded-case-article-example.jpg">
	  					</div>
	  					
	  					<div class="left-case-desc">
	  						<p class="title">Company Description</p>

		  						<p>
									Part of the Atlas Copco Corporation and one of the world’s 
									foremost manufacturers of compaction and paving equipment 
									for road construction. These complex vehicle systems that 
									are assembled from components from multiple suppliers. 
									Competitors include Bomag (Germany), Ingersoll Rand (Ireland) 
									and Caterpillar (USA).
		  						</p>
	  					
	  					</div>

	  				</div>

	  				<div class="right-column">
	  					
	  					<p class="case-title italic">Dynapac Construction Equipment</p>

	  					<div class="right-case-desc">
	  						
	  						<div class="ul-wrap">
	  							<p class="title">Business Situation</p>
	  							<ul>
	  								<li>Single most significant growth opportunity was in developing countries like China and India</li>
	  								<li>Equipment needed to be adapted to local construction techniques and practices</li>
	  								<li>Struggling to meet price points via independent local divisions and ownership of products</li>
	  							</ul>
	  						</div>

	  						<div class="ul-wrap">
	  							<p class="title">Modularity Results</p>
	  							<ul>
	  								<li>Established central product ownership</li>
	  								<li>Increased standard features and options</li>
	  								<li>Reduced time to market for local variants</li>
	  								<li>Company-wide implementation of technology</li>
	  							</ul>
	  						</div>

	 	  					<div class="ul-wrap">
	  							<p class="title">Measurable Improvements</p>
	  							<ul>
	  								<li>6% reduction in direct material cost</li>
	  								<li>30% reduction in total part numbers</li>
	  								<li>15% fewer parts per product</li>
	  								<li>30% reduction in assembly time</li>
	  							</ul>
	  						</div>
	  					</div>

	  					<div class="case-footer">
	  						<span class="open-overlay">Download the full case story (requires free registration >)</span>

	  						<div class="close-article">
	  							<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
	  						</div>

	  					</div>

	  				</div>
	  				<div class="clear"></div>

	  			</div>
	  		</div>


	  	</div>
	</div>
	

<?php	
	get_footer();
?>





































