<?php
/**
 * The template for displaying Search Results pages.
 */

    get_header();
?>

<div id="content">
	<div class="search-window">
		<div class="wrapped">
			<div class="search-field">
				<h1 style="display: none">Search</h1>

				<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
					<input type="text" class="field input-search" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="Search for posts on this site" />
				</form>
			</div>
	<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				include 'search-content.php';
			endwhile; 
		else:
	?>
		<div class="search-noresults">
			Your search - <br>
			<strong><?php echo esc_attr( get_search_query() ); ?></strong><br>
			- did not match anything on this site.<br>
			<br>
			Suggestions:
			<ul>
				<li>Make sure that all words are spelled correctly.</li>
				<li>Try different keywords.</li>
				<li>Try more general keywords.</li>
			</ul>
		</div>
	<?php
		endif;
	?>
		</div>
	</div>
<?php
    get_footer();
    mm_login();
?>