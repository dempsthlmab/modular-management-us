<?php
/*

*/
	get_header();
?>
	
	<div id="content">
		<!-- hero -->

		<div id="hero-image" style="background: url('<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg'); background-size: cover;">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg">
		</div>

		<div id="our-expertise">

			<!-- qoute section -->
		  	<div class="qoute-section">
		  		<div class="inner-section">
			  		<p class="qoute-title">
			  			Unmatched Experts in Modularity
			  		</p>

					<p class="text">		
						A modularization project is a huge undertaking for any organization. 
						Most companies will take on a major product platform development only once every few decades – 
						and their internal processes aren’t focused on modularity. At Modular Management, we do modular 
						product architecture every day. As your trusted partner, we can offer:
					</p>
				</div>
		  	</div>

		  	<!-- top-block -->

		  	<div class="article right" style="background: #fff;">
				    
				    <div class="article-text">
						<div class="text-block">
							<div class="italic-title">Unique Expertise</div>

							<p>
								Modular Management’s quality is unsurpassed in its field. Our methodology has been refined over 20 years of ongoing research and practical application. Our PALMA™ software supports the entire process, ensuring you the best result: a lasting product foundation.
							</p>
					
						</div>

						<div class="text-block">
							<div class="italic-title">Fast implementation</div>

							<p>
								We’ve got the tools and methods you need to implement modular product platforms. From concept to launch and all that comes after, we’ve got it covered. All critical data is captured in PALMA™, linking your company’s strategy to our methods in a single program. Our consultants work with you hand-in-hand to get your modular products to market quickly and smoothly. 
							</p>
							
						</div>

						<div class="text-block">
							<div class="italic-title">Minimized risk</div>

							<p>
								Our global consultants have held management positions in fields like product management, lean manufacturing, industrial product design, supply chain and modular design. With over 100 client programs and 300 years of accumulated experience, we can help you develop modular product platforms on time and on budget.  From there, PALMA™ helps your company to manage your product assortment and architecture over its lifecycle.
							</p>
							
						</div>
				    </div>
	
			    	<div class="article-image">
			            <img src="<?php echo get_template_directory_uri(); ?>/img/computer.jpg">
			        </div>

			        <div class="article-small-quote">
			       		<div class="italic-bold-title">
			       			“Over 100 client programs and 300 years of accumulated experience”
			       		</div>
			        </div>


			    	<div class="clear"></div>
			   
			    </div>

			   <!-- end top-block --> 

			   <!-- static case section -->

			   <div class="static-case" id="peab" style='background: url("<?php echo get_template_directory_uri(); ?>/img/slider/slider-first.png"); background-size: cover;'>
			   	
			   		<div class="static-case-content">
				   		<p class="case-title">PEAB Construction </p>
				   		<p class="case-italic">- Establishes A New Standard In The Pre-Fab Market With Broad Product Assortment</p>
				   		<a class="cta-button">Go to case</a>
				   	</div>

			

			   </div>

			   <!-- end static case section -->

			   <!-- bottom articles -->

			    <div class="article bottom-article left" style="background: #fafafa;">
			
				    <div class="article-content">
				    	<div class="italic-bold-title">“A Longer Quote About How We Work or a Graph”</div>
				    </div>

				    <div class="article-text">
						<div class="title">How We Work</div>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
						</p>

						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
						</p>
				    </div>

			    	<div class="clear"></div>
			    	
			    </div>

			   	<div class="article bottom-article left" style="background: #444;">
			
				    <div class="article-content">
				    	<ul class="palma-ul">
				    		<li>Capture all critical data on one billboard</li>
				    		<li>Share information in real time</li>
				    		<li>Configure your product offerings</li>
				    		<li>Trace decisions back to your strategy</li>
				    	</ul>
				    </div>

				    <div class="article-text blue-bottom" style="background: #2fbcd6;">
						<img src="<?php echo get_template_directory_uri(); ?>/img/palma-title.png">
					
						<div class="italic-title">- One System to Ensure the Right Product Offering</div>

						<p>
							Managing your product architecture is all about managing information. 
							Typically, many people in the organization will hold separate data on 
							the features and technical designs of your product assortment. Sometimes, 
							the information is managed in various documents like spreadsheets and manuals. 
							And sometimes, it’s simply inside the heads of experienced staff.  
							But for the long-term success of a modular product platform, 
							this simply won’t do. You’ll need to manage vast amounts of data that encompass a whole product assortment. 
						</p>

						<a class="cta-button blue">Read more</a>
				    </div>

			    	<div class="clear"></div>
			    	
			    </div>

		</div>	

	</div>


<?php	

	get_footer();
?>










