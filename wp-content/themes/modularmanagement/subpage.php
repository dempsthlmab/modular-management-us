<?php
/*

*/
	get_header();
?>
	
	<div id="content">
		<!-- hero -->

		<div id="hero-image" style="background: url('<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg'); background-size: cover;">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg">
		</div>

		<div id="subpage">

			<!-- 
				
					Article with image on right side, text on left

			-->

			<div class="article right" style="background: #fff;">
				    
			    <div class="article-text">
					<div class="text-block">
						<div class="italic-title">Unique Expertise</div>

						<p>
							Modular Management’s quality is unsurpassed in its field. Our methodology has been refined over 20 years of ongoing research and practical application. Our PALMA™ software supports the entire process, ensuring you the best result: a lasting product foundation.
						</p>
				
					</div>

					<div class="text-block">
						<div class="italic-title">Fast implementation</div>

						<p>
							We’ve got the tools and methods you need to implement modular product platforms. From concept to launch and all that comes after, we’ve got it covered. All critical data is captured in PALMA™, linking your company’s strategy to our methods in a single program. Our consultants work with you hand-in-hand to get your modular products to market quickly and smoothly. 
						</p>
						
					</div>

					<div class="text-block">
						<div class="italic-title">Minimized risk</div>

						<p>
							Our global consultants have held management positions in fields like product management, lean manufacturing, industrial product design, supply chain and modular design. With over 100 client programs and 300 years of accumulated experience, we can help you develop modular product platforms on time and on budget.  From there, PALMA™ helps your company to manage your product assortment and architecture over its lifecycle.
						</p>
						
					</div>
			    </div>

		    	<div class="article-image">
		            <img src="http://mm.dev/wp-content/themes/modularmanagement/img/computer.jpg">
		        </div>

		        <div class="article-small-quote">
		       		<div class="italic-bold-title">
		       			“Over 100 client programs and 300 years of accumulated experience”
		       		</div>
		        </div>


		    	<div class="clear"></div>
		   
			</div>


			<!-- static case section -->

  		    <div class="static-case" id="peab" style='background: url("<?php echo get_template_directory_uri(); ?>/img/slider/slider-first.png"); background-size: cover;'>
		   	
		   		<div class="static-case-content">
			   		<p class="case-title">PEAB Construction </p>
			   		<p class="case-italic">- Establishes A New Standard In The Pre-Fab Market With Broad Product Assortment</p>
			   		<a class="cta-button">Go to case</a>
			   	</div>

		    </div>



			<!-- 
				
					Article with image on left side, text on left

			-->

			<div class="article left" style="background: #fff;">
				    
			    <div class="article-text">
					<div class="text-block">
						<div class="italic-title">Unique Expertise</div>

						<p>
							Modular Management’s quality is unsurpassed in its field. Our methodology has been refined over 20 years of ongoing research and practical application. Our PALMA™ software supports the entire process, ensuring you the best result: a lasting product foundation.
						</p>
				
					</div>

					<div class="text-block">
						<div class="italic-title">Fast implementation</div>

						<p>
							We’ve got the tools and methods you need to implement modular product platforms. From concept to launch and all that comes after, we’ve got it covered. All critical data is captured in PALMA™, linking your company’s strategy to our methods in a single program. Our consultants work with you hand-in-hand to get your modular products to market quickly and smoothly. 
						</p>
						
					</div>

					<div class="text-block">
						<div class="italic-title">Minimized risk</div>

						<p>
							Our global consultants have held management positions in fields like product management, lean manufacturing, industrial product design, supply chain and modular design. With over 100 client programs and 300 years of accumulated experience, we can help you develop modular product platforms on time and on budget.  From there, PALMA™ helps your company to manage your product assortment and architecture over its lifecycle.
						</p>
						
					</div>
			    </div>

		    	<div class="article-image">
		            <img src="http://mm.dev/wp-content/themes/modularmanagement/img/computer.jpg">
		        </div>

		        <div class="article-small-quote">
		       		<div class="italic-bold-title">
		       			“Over 100 client programs and 300 years of accumulated experience”
		       		</div>
		        </div>


		    	<div class="clear"></div>
		   
			</div>
			

		</div>	

	</div>


<?php	

	get_footer();
?>










