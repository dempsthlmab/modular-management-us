<?php
    get_header();

    /**
     * This is the account page, feel free to play around
     */

    $options = get_fields('option');

    //var_dump($options);

    $contact_form_id = $options['contact_form']->ID;

    $header = $options['account_header'];
    $text = $options['account_text'];
    $background_color = '#fff';
    $download_link = $options['download_link'];
    $download_title = $options['download_title'];
    $other_downloads_title = $options['other_download_title'];


    /*$form_title = (!empty(get_field('contact_form_title','option')) ? get_field('contact_form_title','option') : 'Contact');
    $form_italic = (!empty(get_field('contact_form_italic','option')) ? get_field('contact_form_italic','option') : 'Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.');
    $form_content = (!empty(get_field('contact_form_content','option')) ? get_field('contact_form_content','option') : 'You can also contact us by sending an e-mail to:<a href="mailto:info@'.current_domain().'">info@'.current_domain().'</a>');
*/
    $form_title_field = get_field('contact_form_title','option');
    $form_title = (empty($form_title_field)) ? 'Contact' : $form_title_field;
    
    $form_italic_field = get_field('contact_form_italic','option');
    $form_italic = (empty($form_italic_field)) ? 'Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.' : $form_italic_field;

    $form_content_field = get_field('contact_form_content','option');
    $form_content = (empty($form_content_field)) ? 'You can also contact us by sending an e-mail to:<a href="mailto:info@'.current_domain().'">info@'.current_domain().'</a>' : $form_content_field; 

?>

    <div id="content">


        <?php if($_GET['password-reset'] == 'true'): ?>

            <div style="color: green;">
                <?php _e('Password updated', 'modman'); ?>
            </div>

            <div class="clear"></div>
        <?php endif; ?>

        <?php mm_error_messages(); ?>

         <div class="article left top" style="background:<?php echo $background_color; ?>">
            <div class="article-content <?php echo $img_pos; ?>  <?php echo $dark_background; ?>">
                <h1><?php echo $header; ?></h1>
                <div><?php echo $text; ?></div>

            </div>
            <?php

                $current_user = wp_get_current_user();

                $restrictedUser = $current_user->caps['restricted'];

                if(!$restrictedUser) {
            ?>
                    <div class="article-download">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/palmadownload.png" />
                        <a href="<?php bloginfo('url'); ?>/download/<?php echo $download_link->ID; ?>">

                            <h3><?php echo $download_title;?></h3>
                        </a>
                    </div>
            <?php
                }
            ?>

            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="article">
            <?php if(!empty($options['other_downloads'][0]['file']) && !$restrictedUser): ?>
            <div class="article-download-list">
                <h2><?php echo $other_downloads_title; ?></h2>
                <?php foreach($options['other_downloads'] as $d): ?>
                <div class="other-download">

                    <?php echo $d['file']->post_title; ?>
                    <a class="download" href="<?php bloginfo('url'); ?>/download/<?php echo $d['file']->ID; ?>"><?php _e('Download','modman'); ?></a>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="form-column">
                <div class="form-article">
                    <div class="inner">
                        <div class="text-content">
                            <div class="form-title"><?php echo $form_title; ?></div>
                            <div class="form-italic"><?php echo $form_italic; ?></div>

                        </div>
                         <div class="form-text">
                            <?php echo $form_content; ?>
                        </div>
                        <div class="form-content">

                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'"]'); ?>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php else: ?>

                <div class="form-article">
                    <div class="inner">
                        <div class="text-content">
                            <div class="form-title"><?php echo $form_title; ?></div>
                            <div class="form-italic"><?php echo $form_italic; ?></div>

                            <div class="form-text">
                                <?php echo $form_content; ?>
                            </div>
                        </div>
                        <div class="form-content">

                            <?php echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'"]'); ?>
                            <!--<form>

                                <div class="top-input-wrapper">
                                    <div class="top-input">
                                        <input type="text" placeholder="Name">
                                    </div>
                                    <div class="top-input">
                                        <input type="text" placeholder="Phone">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="email-wrapper">
                                    <input type="email" placeholder="Email">
                                </div>
                                <div class="submit-wrapper">
                                    <button type="submit">SEND</button>
                                </div>
                            </form>-->
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>


<?php
    mm_change_password();
    get_footer();

?>





































