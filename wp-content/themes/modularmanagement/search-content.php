<div class="search-result">
    <a class="title" href="<?php echo get_permalink() ?>"><?php the_title() ?></a>

    <div class="content">
    <?php
        $excerpt = get_the_excerpt();
        echo preg_replace( "/\[[^)]+\]/", "", $excerpt);
    ?>
    </div>
</div>