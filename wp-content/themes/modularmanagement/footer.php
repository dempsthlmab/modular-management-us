        <div id="footer" class="content-row item-count-1 centered">
			<div class="cell cell-item-1">
				<div class="form-article">
					<div class="inner">
						<div class="text-content">
							<div class="form-title">Contact</div>
							<div class="form-italic">Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.</div>

							<div class="form-text">
								<p>You can also contact us by sending an e-mail to:</p>
								<a nowrap href="mailto:info.americas@modularmanagement.com" style="white-space:nowrap" class="ga-listen" ga-category="Contact Form" ga-action="Click" ga-label="Mailto">info.americas@modularmanagement.com</a><br>
								<span>or call us on:</span> <a nowrap href="phone:+1 (952) 854 6800" style="white-space:nowrap" class="ga-listen" ga-category="Contact Form" ga-action="Click" ga-label="Telephone">+1 (952) 854 6800</a>
							</div>

							<a href="https://www.linkedin.com/company/modular-management?trk=hb_tab_compy_id_443462" target="_blank" class="linkedin ga-listen" ga-category="Contact Form" ga-action="Navigate" ga-label="LinkedIn">
								<img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png">
								<span style="color: white"><?php _e('Follow us on Linkedin','modman'); ?></span>
							</a>
						</div>
						<div class="form-content">
							<?php echo do_shortcode('[contact-form-7 id="183" title="Contact form"]'); ?>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<div class="bottom-footer">
				<span><?php _e('Copyright © Modular Management','modman'); ?> <?php echo date('Y'); ?></span>
			</div>
		</div>
				
		<?php wp_footer(); ?>

		<?php if(mm_ip_info() != 'CN'): ?>
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-5879700-2', 'auto');
		ga('send', 'pageview');
		</script>
		<?php endif; ?>
    </body>
</html>
