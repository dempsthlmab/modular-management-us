<?php
/*
*/
	get_header();
?>
	
	<div id="content">
		<!-- hero -->

		<div id="hero-image" style="background: url('<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg'); background-size: cover;">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg">
		</div>



			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	        
	       
			<?php the_content(); ?>

	        <?php endwhile; endif; ?>

			<!-- qoute section -->
		  	<div class="qoute-section">
		  		<div class="inner-section">
			  		<p class="qoute-title">
			  			We’ve Got the Method
			  		</p>

			  		<p class="italic">- You’ve Got the Potential.</p>

					<p class="text">		
						Modular Management makes the impossible possible:  We use modular product architecture to help you achieve high productivity, superior products and services, and increased market share – all at the same time.
					</p>
				</div>
		  	</div>

		  	<div class="article right" style="background: #fff;">
				    
				    <div class="article-text">
						<div class="text-block">
							<p>
								Using innovative tools and structured processes, we partner with your organization to uncover and exploit the potential of a modular product architecture that lies within your markets, products, technologies and operations.
							</p>
						</div>

						<div class="text-block">
							<div class="italic-title">Our Success Story</div>

							<p>
								In the early 1990s, I was part of a team of dedicated researchers at Stockholm’s Royal Institute of Technology and IVF.* We were investigating the drivers behind modularization and how it was being successfully employed in product design, development and marketing by a number of companies including Sony, Honda, and Scania. 
								Using our research, we built a working model for approaching and implementing modularization in a structured and effective way. We recognized that our model would have great value for any organization dealing with different or changing customer needs.  That led us to form Modular Management in 1996 as a product development consulting firm.
								In the years that followed, we developed two more methods: one to quantify the cost of product structure complexity and another to manufacture a modular product in the best possible way. These methods led to our growing reputation as experts in modularity. Today we use 20 methods and over 70 tools to give your company the best possible support for modularization.
								Modular Management Group is an international company based in Stockholm, Sweden, with subsidiaries in Sweden, U.S.A, Asia and Germany. 
								We’re proud of the results modularity has achieved for our customers – companies like Whirlpool, Ericsson, ABB and Volvo.  And thanks to them, we never stop growing and expanding our competence. We’re continuously challenged to improve and develop more methods and tools that make modularization faster, safer and more effective.							</p>
															
							<p>
								Alex von Yxkull, Founder
							</p>

							<p>
								*The research institute for Sweden’s engineering industry.
							</p>

						</div>
			
				    </div>
	
			    	<div class="article-image">
			            <img src="<?php echo get_template_directory_uri(); ?>/img/computer.jpg">
			        </div>

			        <div class="article-small-quote">
			       		<div class="italic-bold-title">
			       			“An international company based in Stockholm, Sweden”
			       		</div>
			        </div>


			    	<div class="clear"></div>
			   
		    </div>


			 	<div id="board-nav">

			 		<p class="title">Executive Board</p>

			    	<div class="rounded-image-wrapper">
						<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
					</div>

			    	<div class="rounded-image-wrapper">
						<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
					</div>

			    	<div class="rounded-image-wrapper">
						<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
					</div>
			    </div>


			    <div id="offices">

			    	<!-- -->

				    <div id="stockholm-office" class="office-wrapper">
				    	<div class="office-hero">
				    		<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
				    	</div>


							<div class="office-manager">

								<div class="rounded-image-wrapper">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
								</div>

								<p class="bold-italic">Contact Name</p>
								<p class="light">Office Manager</p>
							
							</div>

					    	<div class="office-contact-details">

					    		<p class="title">Head Office</p>
								<p class="italic-title">Stockholm, Sweden</p>

					    		<div class="adress">
					    			<div class="list-image">
					    				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/pin.png">
					    			</div>
					    			<ul>
					    				<li>Address:</li>
					    				<li>Kungsgatan 37</li>
					    				<li>111 56 Stockholm</li>
					    				<li>Sweden</li>
					    			</ul>
					    		</div>

								<div class="phone">
					    			<div class="list-image">
					    				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/phone.png">
					    			</div>
					    			<ul>
					    				<li>Tel: <a href="">+46 (0)8 456 35 00</a></li>
					    				<li>Fax: <a href="">+46 (0)8 456 35 00</a></li>   
					    			</ul>
					    		</div>


								<div class="mail">
					    			<div class="list-image">
					    				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/letter.png">
					    			</div>
					    			<ul>
					    				<li>E-mail: <a href="mailto:+46 (0)8 456 35 00">info@modularmanagement.com</a></li>
					    			</ul>
					    		</div>



					    	</div>
					    	<div class="clear"></div>

				    </div>

				    <!-- -->

				   	<div id="america-office" class="office-wrapper">
				    	<div class="office-hero">
				    		<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
				    	</div>


							<div class="office-manager">

								<div class="rounded-image-wrapper">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sthlm-office.png">
								</div>

								<p class="bold-italic">Contact Name</p>
								<p class="light">Office Manager</p>
							
							</div>

					    	<div class="office-contact-details">

					    		<p class="title">Head Office</p>
								<p class="italic-title">Stockholm, Sweden</p>

					    		<div class="adress">
					    			<div class="list-image">
					    				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/pin.png">
					    			</div>
					    			<ul>
					    				<li>Address:</li>
					    				<li>Kungsgatan 37</li>
					    				<li>111 56 Stockholm</li>
					    				<li>Sweden</li>
					    			</ul>
					    		</div>

								<div class="phone">
					    			<div class="list-image">
					    				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/phone.png">
					    			</div>
					    			<ul>
					    				<li>Tel: <a href="">+46 (0)8 456 35 00</a></li>
					    				<li>Fax: <a href="">+46 (0)8 456 35 99</a></li>   
					    			</ul>
					    		</div>


								<div class="mail">
					    			<div class="list-image">
					    				<img src="<?php echo get_template_directory_uri(); ?>/img/icons/letter.png">
					    			</div>
					    			<ul>
					    				<li>E-mail: <a href="mailto:info@modularmanagement.com">info@modularmanagement.com</a></li>
					    			</ul>
					    		</div>



					    	</div>
					    	<div class="clear"></div>

				    </div>
			    </div>

			    <div id="office-nav" class="stockholm-office">
			    	<div class="office-navlink" data-id="america-office">
			    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/of-america.png">
			    	</div>

			    	<div class="office-navlink" data-id="germany-office">
			    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/of-germany.png">
			    	</div>

			    	<div class="office-navlink active" data-id="stockholm-office">
			    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/of-sweden.png">
			    	</div>

			    	<div class="office-navlink" data-id="asia-office">
			    		<img src="<?php echo get_template_directory_uri(); ?>/img/icons/of-asia-2.png">
			    	</div>
			    </div>


	</div>


<?php	

	get_footer();
?>










