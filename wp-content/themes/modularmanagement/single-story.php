<?php
    get_header();
?>

<div id="content">

<?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
            $prev_post = get_previous_post();
            $next_post = get_next_post();

            $file_attachment = get_field('file_attachment');
?>
            <div class="story-navigator">
                <?php if($prev_post): ?>
                    <a href="<?php echo get_permalink($prev_post->ID) ?>" class="post prev ga-listen" ga-category="Story" ga-action="Navigate Prev" ga-label="<?php echo $prev_post->post_title ?>">
                        <?php echo $prev_post->post_title ?>
                    </a>
                <?php else: ?>
                    <div class="post prev"></div>
                <?php endif ?>

                <div class="post main">
                    <h1><?php the_title() ?></h1>

                    <span><?php echo get_field('subtitle') ?></span>

                    <?php if($prev_post): ?>
                        <p class="arrow left"></p>
                    <?php endif ?>

                    <?php if($next_post): ?>
                        <p class="arrow right"></p>
                    <?php endif ?>
                </div>

                <?php if($next_post): ?>
                    <a href="<?php echo get_permalink($next_post->ID) ?>" class="post next ga-listen" ga-category="Story" ga-action="Navigate Next" ga-label="<?php echo $next_post->post_title ?>">
                        <?php echo $next_post->post_title ?>
                    </a>
                <?php else: ?>
                    <div class="post prev"></div>
                <?php endif ?>
            </div>

            <?php if($file_attachment): ?>
                <div class="story-filedownload">
                    <a href="<?php echo $file_attachment['url'] ?>" download="<?php echo $file_attachment['filename'] ?>" class="ga-listen" ga-category="Click" ga-action="Download" ga-label="<?= $file_attachment['filename'] ?>" <?php if(strtolower($file_attachment['filename']) == 'mts-case-story.pdf'): ?>onclick="ga('send', 'event', 'Download', 'PDF', 'MTS-Case-Story.pdf');"<?php endif ?>>Download PDF</a>
                </div>
            <?php endif ?>
<?php
            mm_registration_form_fields();
        
            if( have_rows('content_rows') ) :
                while ( have_rows('content_rows') ) : the_row();
                    $content = get_sub_field('content');
                    if($content) :
                        $children = count($content);
                        $classes = "";
                        if(get_sub_field('centered'))
                            $classes .= " centered";
                        //if($children === 2)
                        //    $classes .= " checkered";

                        $count = 1;

                        echo '<div class="content-row story item-count-' . count($content) . $classes. '">';

                        while(has_sub_field('content')) :
                            
                            $classname = "cell item-" . $count;

                            switch(get_row_layout())
                            {
                                case "text_box":
                                    echo get_sub_field('text_box'); 
                                    break;
                                case "expanded_article":
                                    get_template_part('fields/expandable_article');
                                    break;
                                default:
                                    include(locate_template('widgets/' . get_row_layout() . '.php'));
                                    break;
                            }

                            $count++;
                        endwhile;
                        echo '</div>';
                    endif;
                endwhile;
            endif;
        endwhile; 
    endif;
      
    get_footer();
    mm_login();
?>
