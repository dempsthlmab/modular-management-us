<?php
/*

*/
	get_header();
?>
	
	<div id="content">
		<!-- hero -->

		<div id="hero-image" style="background: url('<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg'); background-size: cover;">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg">
		</div>

		<div id="our-expertise">

			<!-- Top-block -->
			    
			    <div class="article right" style="background: #fff;">
				    
				    <div class="article-text blue-top">
					<img src="<?php echo get_template_directory_uri(); ?>/img/palma-title.png">
					<div class="italic-title">- One System to Ensure the Right Product Offering</div>

					<p>
						Managing your product architecture is all about managing information. 
						Typically, many people in the organization will hold separate data on 
						the features and technical designs of your product assortment. Sometimes, 
						the information is managed in various documents like spreadsheets and manuals. 
						And sometimes, it’s simply inside the heads of experienced staff.  
						But for the long-term success of a modular product platform, 
						this simply won’t do. You’ll need to manage vast amounts of data that encompass a whole product assortment. 
					</p>

					<p>
						Our software PALMA™ helps you to do just that. 
						It enables you to manage your product platforms over their entire lifecycle. 
						With PALMA™, all the critical data you need is stored in one single program 
						to ensure quality and profitability over time.
					</p>
				    </div>

				    <div class="article-content palma">
				    	<img class="palma-logo" src="<?php echo get_template_directory_uri(); ?>/img/palma-logo.png">
						<p class="italic-bold-title">“The result is a quicker, smoother implementation and a lasting product foundation”</p>
				    </div>
			    
			    	<div class="clear"></div>
			   
			    </div>

			<!-- End top block -->

			<!-- Main block -->

				<div class="article right" style="background: #fff;">
				    <div class="article-text">

				    	<div class="text-block">
							<div class="italic-title">Real-time Information Sharing</div>

							<p>
								Like any other product development project, 
								a successful modularization project involves a cross-functional commitment. 
								This often leads to sending spreadsheets, manuals and other documents – 
								many of them outdated - back and forth between different departments. 
							</p>
							<p>
								PALMA™ provides one billboard where all critical product information is captured and made accessible in real time. 
								It gives a cross-functional view of your product assortment that delivers key information to sales, production, 
								and engineers throughout the MFD process and beyond.  Your staff can work with the same information in real time
								 which means they can specify, share and review product and technical requirements in one common format.
							</p>
						</div>

						<div class="text-block">
							<div class="italic-title">Cut Implementation Time</div>

							<p>
								Modularity can take years to implement because of poorly managed information.  
								PALMA™ provides a holistic and detailed view of customer needs as well as the product properties that meet them. 
								It also holds the technical solutions that comprise the product, specifications on modules, 
								interfaces and the configuration rules for your products.  All of this is done in a structured and coherent 
								way so that your engineers will know exactly what to do. The result? You avoid reworks in design at a later stage and 
								bring your products to market faster. And with easily-generated reports, you skip lengthy specifications with overlapping information. 
								All of this means that your modularity implementation could take months instead of years. 
							</p>
							
						</div>

						<div class="text-block">
							<div class="italic-title">Respond Quickly to Customer Requests</div>

							<p>
								Configuring a product offering for your customer requires understanding and managing information from many sources. 
								It can be time consuming to get to the right data, resulting in a lengthy and error-prone process. PALMA™’ has a 
								built-in configurator that manages the configuration of your product offerings for you. 
								Once in place, you can easily convert this into a configurator that can be used by your sales 
								staff to configure a product with just a few clicks.
							</p>
							
						</div>

						<div class="text-block">
							<div class="italic-title">Stay Aligned With Your Strategy</div>

							<p>
								The lifecycle of your product platform runs well over a decade. 
								Keeping track of different data and decisions made is essential to keeping in line with your strategy and market objectives. 
								PALMA™ is the tool to continuously ensure you have the right product offering. 
								It allows you to easily trace product and design decisions back to strategy to ensure a lasting product foundation.
							</p>
							
						</div>

				    </div>

					<div class="article-image">
			            <img src="<?php echo get_template_directory_uri(); ?>/img/computer.jpg">
			        </div>

			        <div class="form-column">

		        	  	<div class="form-article">
					  		<div class="inner">
						  		<div class="text-content">
						  			<div class="form-title">Contact</div>
									<div class="form-italic">Find out more about what modularity can do for you. Fill in your name and e-mail and we’ll contact you.</div>

								
						  		</div>
						  		<div class="form-content">
						  			<form id="page-form">
							  			<div class="top-input-wrapper">
								  			<div class="top-input">
								  				<input type="text" placeholder="name" name="name">
								  			</div>
								  			<div class="top-input">
								  				<input type="text" placeholder="phone" name="phone">
								  			</div>
								  			<div class="clear"></div>
								  		</div>	
								  		<div class="email-wrapper">
								  			<input type="email" placeholder="email" name="email">
								  		</div>
								  		<div class="submit-wrapper">	
								  			<button type="submit">SEND</button>
								  		</div>
								  	</form>
						  		</div>
						  		<div class="clear"></div>
					  		</div>
					  	</div>

					  </div>
				 
			    
			    	<div class="clear"></div>
			   
			    </div>
			
			
		</div>	









	</div>


<?php	

	get_footer();
?>










