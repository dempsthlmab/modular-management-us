<?php
    get_header();
?>

<div id="content">
<?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();

            mm_registration_form_fields();
        
            if( have_rows('content_rows') ) :
                while ( have_rows('content_rows') ) : the_row();
                    $content = get_sub_field('content');
                    if($content) :
                        $children = count($content);
                        $classes = "";

                        if(get_sub_field('centered'))
                            $classes .= " centered";
                        if($children === 2)
                            $classes .= " checkered";

                        $count = 1;

                        echo '<div class="content-row item-count-' . count($content) . $classes. '">';

                        while(has_sub_field('content')) :
                            
                            $classname = "cell item-" . $count;
                            $layout = get_row_layout();
                            switch($layout)
                            {
                                case "text_box":
                                    echo get_sub_field('text_box'); 
                                    break;
                                case "expanded_article":
                                    get_template_part('fields/expandable_article');
                                    break;
                                default:
                                    include(locate_template('widgets/' . $layout . '.php'));
                                    break;
                            }

                            $count++;
                        endwhile;
                        echo '</div>';
                    endif;
                endwhile;
            endif;
        endwhile; 
    endif;
      
    get_footer();
    mm_login();
?>
