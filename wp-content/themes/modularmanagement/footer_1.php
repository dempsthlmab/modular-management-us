        
        <!-- site map ? -->

        <div id="footer">
            <div class="wrapped">
                <div class="left-column" id="left-footer">

                    <?php mm_footer_menu('primary'); ?>
                    
                </div>

                <div class="right-column" id="right-footer">
                         
                    <?php dynamic_sidebar('footer-area'); ?>

                    <a href="https://www.linkedin.com/company/modular-management?trk=hb_tab_compy_id_443462" target="_blank" class="linkedin">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png">
                        <span><?php _e('Follow us on Linkedin','modman'); ?></span>
                    </a>

                </div>
                <div class="clear"></div>
            </div>
        </div>  
        <div class="bottom-footer">
                <span><?php _e('Copyright © Modular Management','modman'); ?> <?php echo date('Y'); ?></span>
            </div>
        </div>
        
        <?php wp_footer(); ?>

        <?php if(mm_ip_info() != 'CN'): ?>
        <script>
            <!--//--><![CDATA[//><!--
            var _gaq = _gaq || [];_gaq.push(["_setAccount", "UA-5879700-1"]);
            _gaq.push(["_setDomainName", "none"]);
            _gaq.push(["_setAllowLinker", true]);
            _gaq.push(["_trackPageview"]);(function() {var ga = document.createElement("script");
            ga.type = "text/javascript";ga.async = true;
            ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
            var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga, s);})();
        </script>
        <?php endif; ?>
        
    </body>
</html>
