<?php
    get_header();
?>

    <div id="content">
        <div id="subpage">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            
                <div class="article middle" style="background: #fff;">
                    <div class="article-text">
                        <div class="text-block">
                            <div class="italic-title"><?php the_title(); ?></div>
                            <?php the_content(); ?>
                        </div>
                    </div>
                    
                    <?php if(have_rows('images')): while(have_rows('images')): the_row(); ?>

                    <?php $image = get_sub_field('image'); ?>
                    <div class="article-image">
                        <img src="<?php echo $image['sizes']['article']; ?>">
                    </div>
                <?php endwhile; endif; ?>
                </div>

            <?php endwhile; endif; //WP loop ?>
        </div>
    </div>
    

<?php   
    get_footer();
?>
