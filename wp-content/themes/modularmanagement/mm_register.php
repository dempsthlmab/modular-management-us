<?php

/**
 * User registration
 * @link  https://pippinsplugins.com/creating-custom-front-end-registration-and-login-forms/
 */

function mm_change_password(){

    ?>
    <div id="overlay" class="overlay-change">
        <div id="form-overlay">
            <div id="close-form">
                <img src="<?php echo get_template_directory_uri(); ?>/img/close-form.png">
            </div>
            <div class="inner">
                <div id="form-header">
                    <p class="title"><?php _e('Change password','modman'); ?></p>
                </div>
                <?php mm_show_error_messages(); ?>
                <form id="change-form" action="" method="POST">

                    <div class="left-form-column">
                        <span class="required">*<?php _e('Required','modman'); ?></span>
                        <div>
                            <input name="old_password" type="password" placeholder="<?php _e('Old Password','modman'); ?>*">
                        </div>
                        <div>
                            <input name="new_password" type="password" id="password" placeholder="<?php _e('New Password','modman'); ?>*">
                        </div>

                        <div class="figure" id="strength_score">
                            <span></span>
                            <p><?php _e('Password strength'); ?></p>
                        </div>

                    </div>

                    <div class="right-form-column">
                        <p style="height: 58px;">

                        </p>
                        <div>
                            <input name="repeat_password" type="password" placeholder="<?php _e('Repeat Password','modman'); ?>*">
                        </div>

                    </div>

                    <div class="clear"></div>

                    <div class="form-footer">

                        <input name="robot_2" type="checkbox" id="c2" style="display: none;" />
                        <input type="hidden" name="mm_change_nonce" value="<?php echo wp_create_nonce('mm-password-nonce'); ?>"/>
                        <button type="submit" class="submit-button cta-button" id="change-password">
                            <?php _e('Change Password','modman'); ?>
                        </button>

                    </div>

                </form>
            </div>
        </div>
     </div>
     <?php
}


// registration form fields
function mm_registration_form_fields() {

 ?>
        <?php
        // show any error messages after form submission
        mm_show_error_messages(); ?>

       <div id="overlay">
        <div id="form-overlay">
            <div id="close-form">
                <img src="<?php echo get_template_directory_uri(); ?>/img/close-form.png">
            </div>
            <div class="inner">
                <div id="form-header">
                    <p class="title"><?php _e('Register','modman'); ?></p>
                    <p class="italic"><?php _e('- Please Register to Access Even More Content','modman'); ?></p>
                </div>
                <form id="signup-form" action="" method="POST">

                    <div class="left-form-column">
                        <span class="required">*<?php _e('Required','modman'); ?></span>
                        <div>
                            <input tabindex="1"  name="user_email" type="email" placeholder="<?php _e('Email','modman'); ?>*">
                        </div>
                        <div>
                            <input tabindex="2" name="user_password" type="password" id="password" placeholder="<?php _e('Password','modman'); ?>*">
                        </div>
                        <div>
                            <input tabindex="3" name="repeat-password" type="password" placeholder="<?php _e('Repeat Password','modman'); ?>*">
                        </div>

                        <div class="figure" id="strength_score">
                            <span></span>
                            <p><?php _e('Password strength'); ?></p>
                        </div>

                    </div>

                    <div class="right-form-column">
                        <div>
                            <input tabindex="4" name="first_name" type="text" placeholder="<?php _e('First Name','modman'); ?>*">
                        </div>
                        <div>
                            <input tabindex="5" name="last_name" type="text" placeholder="<?php _e('Last Name','modman'); ?>*">
                        </div>
                        <div>
                            <input tabindex="6" name="company" type="text" placeholder="<?php _e('Company','modman'); ?>*">
                        </div>
                        <div>
                          <?php $curr_cc = get_default_country_code(); ?>
                            <span id="default-value"><span><?php echo $curr_cc; ?> </span> <img id="default-arrow" src="<?php echo get_template_directory_uri(); ?>/img/arrow-down.png"></span>

                            <div class="phone-field">


                                <select name="country" class="country" id="country">


                                    <?php foreach(mm_countries() as $c): ?>
                                        <option value="<?php echo $c['d_code']; ?>"
                                          <?php if($c['d_code'] == $curr_cc) {
                                            echo 'selected';
                                          } ?>>
                                          <?php echo $c['name']; ?></option>
                                    <?php endforeach; ?>

                                </select>



                                <input tabindex="7" name="phone" class="phone" type="text" placeholder="<?php _e('Telephone*','modman'); ?>">
                                <div class="clear"></div>
                            </div>
                        </div>

                    </div>
                    <!-- <label class="login-captcha-label" for="captchatext"><?php _e('Enter CAPTCHA','modman'); ?></label> -->

                    <div class="signup-captcha">
                      <div class="signup-captcha-left">
                        <img class="signup-captcha-image" id="captcha" src="<?php echo bloginfo('url'); ?>/wp-content/plugins/si-captcha-for-wordpress/captcha/securimage_show.php" alt="CAPTCHA Image" />
                        <a class="signup-new-captcha-image" href="#" onclick="document.getElementById('captcha').src = '<?php echo bloginfo('url'); ?>/wp-content/plugins/si-captcha-for-wordpress/captcha/securimage_show.php?' + Math.random(); return false">New Image</a>
                        <!-- NOTE: I removed a "/" that used to be before the "?" in the "onclick url" above. Don't think it is needed, not sure if it was a typo -->
                      </div>
                      <div class="signup-captcha-right">
                        <input tabindex="8" placeholder="<?php _e('Enter CAPTCHA*','modman'); ?>" class="signup-captcha-text" id="captchatext" type="text" name="captcha_code" size="10" maxlength="6" />
                      </div>
                    </div>

                    <div class="clear"></div>

                    <div class="form-footer">

                        <div>
                            <input name="robot_2" type="checkbox" id="c2" style="display: none;" />
                            <input type="hidden" name="mm_register_nonce" value="<?php echo wp_create_nonce('mm-register-nonce'); ?>"/>
                            <button type="submit" class="submit-button cta-button" id="create-account">
                                <?php _e('create new account','modman'); ?>
                            </button>
                             <a href="#login" id="register-to-login" class="open-overlay-login-btn open-overlay-login overlay-link"><?php _e('Log in','modman'); ?></a>
                             <a class="overlay-link" href="<?php echo wp_lostpassword_url(); ?>"><?php _e('Lost your password?' , 'modman'); ?></a>
                        </div>

                    </div>


                </form>
            </div>
        </div>
     </div>
    <?php
}

function mm_login(){

    $args = array(
        'echo'           => true,
        'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        'form_id'        => 'loginform',
        'label_username' => __( 'Username' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in'   => __( 'Log In' ),
        'id_username'    => 'user_login',
        'id_password'    => 'user_pass',
        'id_remember'    => 'rememberme',
        'id_submit'      => 'wp-submit',
        'remember'       => true,
        'value_username' => '',
        'value_remember' => false
    ); ?>

        <?php
        // show any error messages after form submission
        //mm_show_error_messages(); ?>

       <div class="overlay-login">
        <div class="form-overlay">
            <div class="close-form">
                <img src="<?php echo get_template_directory_uri(); ?>/img/close-form.png">
            </div>
            <div class="inner">
                <div class="form-header">
                    <p class="title"><?php _e('Log in','modman'); ?></p>
                    <p class="italic"><?php _e('- Please Log in to Access Even More Content','modman'); ?></p>
                </div>
                <?php
                    //wp_login_form(); // We can't use this function anymore, since our hacks will be overwritten by each WP Update.
                ?>

                <form class="hej" name="loginform" id="loginform" action="<?php echo bloginfo('url'); ?>/wp-login.php" method="post">
                    <p class="login-username">
                        <label for="user_login"><?php _e('Email','modman'); ?></label>
                        <input type="text" name="log" id="user_login" class="input" value="" size="20">
                    </p>
                    <p class="login-password">
                        <label for="user_pass"><?php _e('Password','modman'); ?></label>
                        <input type="password" name="pwd" id="user_pass" class="input" value="" size="20">
                    </p>

                    <!-- <label class="login-captcha-label" for="captchatext"><?php _e('Enter CAPTCHA','modman'); ?></label> -->
                    <!--<div class="login-captcha">
                      <div class="login-captcha-left">
                        <img class="login-captcha-image" id="captcha" src="<?php echo bloginfo('url'); ?>/wp-content/plugins/si-captcha-for-wordpress/captcha/securimage_show.php" alt="CAPTCHA Image" />
                        <a class="login-new-captcha-image" href="#" onclick="document.getElementById('captcha').src = '<?php echo bloginfo('url'); ?>/wp-content/plugins/si-captcha-for-wordpress/captcha/securimage_show.php/?' + Math.random(); return false">New Image</a>
                      </div>
                      <div class="login-captcha-right">
                        <input placeholder="<?php _e('Enter CAPTCHA','modman'); ?>" class="login-captcha-text" id="captchatext" type="text" name="captcha_code" size="10" maxlength="6" />
                      </div>
                    </div>-->
                    <p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"> <?php _e('Remember Me','modman'); ?></label></p>
                    <p class="login-submit">
                        <input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Log In"><div id="login-form-buttons">
                            <!-- <a class="open-overlay overlay-link" href="#register">Register</a> -->
                            <!-- <a class="lost small overlay-link" href="https://modularmanagement.com/wp-login.php?action=lostpassword">Lost your password?</a> -->
                        </div>
                        <input type="hidden" name="redirect_to" value="https://modularmanagement.com/">
                    </p>
                    <?php
                        wp_nonce_field( 'ajax-login-nonce', 'security' );
                    ?>

                </form>



                <div id="login-form-buttons">
                    <a class="open-overlay overlay-link" href="#register"><?php _e('Register','modman'); ?></a>
                    <a class="lost small overlay-link" href="<?php echo wp_lostpassword_url(); ?>"><?php _e('Lost your password?' , 'modman'); ?></a>
                </div>
                <div class="login-error-msg">Error msg</div>
            </div>
        </div>
     </div>
<?php
}


// register a new user
function mm_add_new_member() {
    if (isset( $_POST["user_email"] ) && wp_verify_nonce($_POST['mm_register_nonce'], 'mm-register-nonce')) {

        $user_email     = wp_filter_kses($_POST["user_email"]);
        $user_login     = $_POST["user_login"] = $user_email;
        $user_first     = wp_filter_kses($_POST["first_name"]);
        $user_last      = wp_filter_kses($_POST["last_name"]);
        $company        = wp_filter_kses($_POST["company"]);
        $phone          = wp_filter_kses($_POST["phone"]);
        $user_pass      = wp_filter_kses($_POST["user_password"]);
        $pass_confirm   = wp_filter_kses($_POST["repeat-password"]);
        $country        = wp_filter_kses($_POST["country"]);
        $robot          = wp_filter_kses($_POST["robot2"]);

        // this is required for username checks
        require_once(ABSPATH . WPINC . '/registration.php');

        if(!is_email($user_email)) {
            //invalid email
            mm_errors()->add('email_invalid', __('Invalid email','modman'));
        }
        if(email_exists($user_email)) {
            //Email address already registered
            mm_errors()->add('email_used', __('Email already registered','modman'));
        }

        if($user_pass == '') {
            // passwords do not match
            mm_errors()->add('password_empty', __('Please enter a password','modman'));
        }

        if($user_pass != $pass_confirm) {
            // passwords do not match
            mm_errors()->add('password_mismatch', __('Passwords do not match','modman'));
        }

        if(empty($company)) {
            // passwords do not match
            mm_errors()->add('company_empty', __('No company entered','modman'));
        }

        if(empty($phone)) {
            // passwords do not match
            mm_errors()->add('phone_empty', __('No phone number entered','modman'));
        }

        if(!empty($robot)){
            mm_errors()->add('robot', __('Robot entered'));
        }

        $errors = mm_errors()->get_error_messages();

        // only create the user in if there are no errors
        if(empty($errors)) {

            $new_user_id = wp_insert_user(array(
                    'user_login'        => $user_login,
                    'user_pass'         => $user_pass,
                    'user_email'        => $user_email,
                    'first_name'        => $user_first,
                    'last_name'         => $user_last,
                    'user_pass'         => $user_pass,
                    'user_registered'   => date('Y-m-d H:i:s'),
                    'role'              => 'restricted',
                    'user_nicename'     => $user_first . ' ' . $user_last,
                )
            );

            //Update phone
            $phone = $country.$phone;

            if(!is_wp_error($new_user_id)) {

                update_user_meta($new_user_id, 'company', $company);
                update_user_meta($new_user_id, 'phone', $phone);

                // send an email to the admin alerting them of the registration
                wp_new_user_notification($new_user_id, $user_pass);

                /*$to = "harris.dailianis@dempsey.se";

                $email = $_POST['user_email'];
                $name = $_POST['first_name'];
                $lastname = $_POST['last_name'];
                $company = $_POST['company'];
                $phone = $_POST['phone'];

                $subject = "User registered (" . $email . ")";
                $message = "User with email " . $email . " has registered\n\n"
                         . "Name: " . $name . " " . $lastname
                         . "Email: " . $email
                         . "Company: " . $company
                         . "Phone: " . $phone;

                wp_mail($to, $subject, $message);*/

                $sites = wp_get_sites();

                foreach($sites as $site){
                     add_user_to_blog( $site['blog_id'], $new_user_id, 'restricted' ); //Add to all sites
                }

                // log the new user in
                $secure_cookie = is_ssl() ? true : false;
                wp_set_auth_cookie( $new_user_id, true, $secure_cookie );
                wp_set_current_user($new_user_id, $user_login);
                do_action('wp_login', $user_login);

                // send the newly created user to the home page after logging them in
                //wp_redirect(home_url( add_query_arg( NULL, NULL ) )); // This didn't seem to work in all occasions
                wp_redirect(home_url());
                exit;
            }

        } else {
            /*
            just testing some stuff...

            $errors_html = '';
            $errors_html .= '<div id="signup-errors">';
            $errors_html .= '<p>';
            $errors_html .= 'There are errors on the page';
            $errors_html .= '</p>';
            $errors_html .= '<ul>';

            foreach ($errors as $e) {
                $errors_html .= '<li>' . $e . '</li>';
            }
            $errors_html .= '</ul>';
            $errors_html .= '</div>';

            echo $errors_html;*/

        }

    }
}
add_action('init', 'mm_add_new_member');

function mm_errors(){
    static $wp_error; // Will hold global variable safely
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function mm_show_error_messages() {
    if($codes = mm_errors()->get_error_codes()) {
        echo '<div class="mm_errors">';
            // Loop error codes and display errors
           foreach($codes as $code){
                $message = mm_errors()->get_error_message($code);
                echo '<span class="error"><strong>' . __('Error','modman') . '</strong>: ' . $message . '</span><br/>';
            }
        echo '</div>';
    }
}

/**
 * Main registration form
 */
add_action( 'register_form', 'mm_main_registration_form' );
function mm_main_registration_form() {

    $first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
    $last_name = ( ! empty( $_POST['last_name'] ) ) ? trim( $_POST['last_name'] ) : '';
    $phone = ( ! empty( $_POST['phone'] ) ) ? trim( $_POST['phone'] ) : '';
    $company = ( ! empty( $_POST['company'] ) ) ? trim( $_POST['company'] ) : '';
    $user_pass = ( ! empty( $_POST['user_pass'] ) ) ? trim( $_POST['user_pass'] ) : '';

        ?>
         <p>
            <label for="user_pass"><?php _e( 'Password','modman') ?><br />
                <input type="password" name="user_pass" id="user_pass" class="input" value="<?php echo esc_attr( wp_unslash( $user_pass ) ); ?>" size="25" /></label>
            <label for="confirm_pass"><?php _e( 'Confirm Password','modman') ?><br />
                <input type="password" name="confirm_pass" id="confirm_pass" class="input" value="" size="25" /></label>
        </p>
        <p>
            <label for="first_name"><?php _e( 'First Name','modman') ?><br />
                <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25" /></label>
        </p>
         <p>
            <label for="last_name"><?php _e( 'Last Name','modman') ?><br />
                <input type="text" name="last_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="25" /></label>
        </p>
         <p>
            <label for="phone"><?php _e( 'Phone','modman') ?><br />
                <input type="text" name="phone" id="phone" class="input" value="<?php echo esc_attr( wp_unslash( $phone ) ); ?>" size="25" /></label>
        </p>
         <p>
            <label for="company"><?php _e( 'Company','modman') ?><br />
                <input type="text" name="company" id="company" class="input" value="<?php echo esc_attr( wp_unslash( $company ) ); ?>" size="25" /></label>
        </p>
        <?php
    }

//2. Add validation. In this case, we make sure first_name is required.
add_filter( 'registration_errors', 'mm_main_registration_errors', 10, 3 );
function mm_main_registration_errors( $errors, $sanitized_user_login, $user_email ) {

    if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
        $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.','modman') );
    }

    if ( empty( $_POST['last_name'] ) || ! empty( $_POST['last_name'] ) && trim( $_POST['last_name'] ) == '' ) {
        $errors->add( 'last_name_error', __( '<strong>ERROR</strong>: You must include a last name.','modman') );
    }

    if ( empty( $_POST['phone'] ) || ! empty( $_POST['phone'] ) && trim( $_POST['phone'] ) == '' ) {
        $errors->add( 'phone_error', __( '<strong>ERROR</strong>: You must include a phone Number.','modman') );
    }

    if ( empty( $_POST['company'] ) || ! empty( $_POST['company'] ) && trim( $_POST['company'] ) == '' ) {
        $errors->add( 'company_error', __( '<strong>ERROR</strong>: You must include a company.','modman') );
    }

     if ( ($_POST['user_pass'] != $_POST['confirm_pass']) || empty( $_POST['user_pass'] ) || (! empty( $_POST['user_pass'] ) && trim( $_POST['user_pass'] ) == '') ) {
        $errors->add( 'password_error', __( '<strong>ERROR</strong>: Password is empty.','modman') );
    }

    return $errors;
}

//3. Finally, save our extra registration user meta.
add_action( 'user_register', 'mm_user_register' );
function mm_user_register( $user_id ) {
    $fields = array('first_name','last_name','phone','company','user_pass');

    foreach($fields as $f){
        if ( ! empty( $_POST[$f] ) ) {
            update_user_meta( $user_id, $f, trim( $_POST[$f] ) );
        }
    }

    if(!empty($_POST['user_login']) && !empty($$_POST['user_pass'])){
        $creds['user_login'] = $_POST['user_login'];
        $creds['user_password'] = $_POST['user_pass'];
        wp_signon($creds,false);
    }
}




?>
