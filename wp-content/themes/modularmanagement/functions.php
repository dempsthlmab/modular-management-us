<?php
show_admin_bar(false);

$ds = DIRECTORY_SEPARATOR;
$base_dir = realpath(dirname(__FILE__)  . $ds . '..') . $ds;

//include_once($base_dir.'/modularmanagement/shortcodes.php');
include_once($base_dir.'/modularmanagement/mm_register.php');
include_once($base_dir.'/modularmanagement/acf.php');

/**
 * Some basic initializations
 */
if ( ! function_exists( 'modularmanagement_init' ) ) :
    function modularmanagement_init(){

        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 825, 510, true );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
            'primary' => __( 'Primary Menu'),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        load_theme_textdomain('modman', get_template_directory() . '/languages');



    }

endif;

add_action( 'after_setup_theme', 'modularmanagement_init' );


/**
 * Load scripts
 */
function modularmanagement_scripts() {

    //global $wp_styles;
    global $is_IE;

    // Load our main stylesheet.
    wp_enqueue_style( 'modularmanagement-style', get_stylesheet_directory_uri() . '/stylesheets/screen.css');
    if($is_IE) {
      wp_enqueue_style( 'modularmanagement-style-ie', get_stylesheet_directory_uri() . '/stylesheets/ie.css');
    }
    //$wp_styles->add_data( 'modularmanagement-style-ie', 'conditional', 'IE' );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    //wp_deregister_script( 'jquery' );
    //wp_register_script( 'jquery', "http://code.jquery.com/jquery-1.11.3.min.js", false, null );
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script( 'modularmanagement-script-analytics', get_template_directory_uri() . '/js/analyticsHelper.js', array(), '1.0', true );
    wp_enqueue_script( 'modularmanagement-script-placeholder', get_template_directory_uri() . '/js/placeholders.min.js', array(), '1.0', true );
    wp_enqueue_script( 'modularmanagement-script-modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js', array(), '1.0', true );
    wp_enqueue_script( 'modularmanagement-script-functions', get_template_directory_uri() . '/js/functions.js', array(), '1.0', true );
    wp_enqueue_script( 'modularmanagement-script-plugins', get_template_directory_uri() . '/js/plugins.js', array(), '1.0', true );

    wp_localize_script( 'modularmanagement-script-functions', 'ajax_login_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => $_SERVER['REQUEST_URI'],
        'path' => 'account',
        'loadingmessage' => __('Signing in, please wait...'),
    ));

    wp_localize_script( 'modularmanagement-script-functions', 'ajax_captcha_object', array(
        'ajaxurl' => get_template_directory_uri() . '/capcheck.php'
    ));

}

add_action( 'wp_enqueue_scripts', 'modularmanagement_scripts' );

/**
 * Load scripts for editor
 */

function mm_color_palette($init) {
  $default_colours = '"000000", "Black",
                      "993300", "Burnt orange",
                      "333300", "Dark olive",
                      "003300", "Dark green",
                      "003366", "Dark azure",
                      "000080", "Navy Blue",
                      "333399", "Indigo",
                      "333333", "Very dark gray",
                      "800000", "Maroon",
                      "FF6600", "Orange",
                      "808000", "Olive",
                      "008000", "Green",
                      "008080", "Teal",
                      "0000FF", "Blue",
                      "666699", "Grayish blue",
                      "808080", "Gray",
                      "FF0000", "Red",
                      "FF9900", "Amber",
                      "99CC00", "Yellow green",
                      "339966", "Sea green",
                      "33CCCC", "Turquoise",
                      "3366FF", "Royal blue",
                      "800080", "Purple",
                      "999999", "Medium gray",
                      "FF00FF", "Magenta",
                      "FFCC00", "Gold",
                      "FFFF00", "Yellow",
                      "00FF00", "Lime",
                      "00FFFF", "Aqua",
                      "00CCFF", "Sky blue",
                      "993366", "Red violet",
                      "FFFFFF", "White"';

  $custom_colours =  '"4c4a4a", "Main Color",
                      "2ebcd6", "Secondary Color",
                      "ffffff", "Background Color",
                      "fafafa", "Background Color 2"';

  // build colour grid default+custom colors
  $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';

  // enable 6th row for custom colours in grid
  $init['textcolor_rows'] = 5;

  return $init;
}
add_filter('tiny_mce_before_init', 'mm_color_palette');

function my_theme_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/stylesheets/screen.css');
}

/*
   Debug preview with custom fields
*/

add_filter('_wp_post_revision_fields', 'add_field_debug_preview');
function add_field_debug_preview($fields){
   $fields["debug_preview"] = "debug_preview";
   return $fields;
}

add_action( 'edit_form_after_title', 'add_input_debug_preview' );
function add_input_debug_preview() {
   echo '<input type="hidden" name="debug_preview" value="debug_preview">';
}


/**
 * Image sizes
 * add_image_size('name of image size', width, height, cropped (true or false))
 * @link  https://codex.wordpress.org/Function_Reference/add_image_size
 */

add_action( 'after_setup_theme', 'mm_image_sizes' );
function mm_image_sizes() {
    add_image_size( 'icon', 48,48);
    add_image_size( 'article', 510,400);
    add_image_size( 'header-full', 1200, 380);
    add_image_size( 'case', 510,340);
    add_image_size( 'product-sidebar', 515,520);
}

/**
 * Don't forget to add image sizes to admin interface
**/

add_filter( 'image_size_names_choose', 'mm_custom_sizes' );

function mm_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'article' => __( 'Article' ,'modman'),
        'header-full' => __( 'Header (full)' ),
        'case' => __( 'Case' ,'modman'),
        'product-sidebar' => __( 'Product (sidebar)' ),
    ) );
}

/**
 * Footer widget area
 */
add_action( 'widgets_init', 'mm_widget_area_init' );
function mm_widget_area_init() {
    register_sidebar( array(
        'name' => __( 'Footer widget area','modman'),
        'id' => 'footer-area',
        'description' => __( 'Footer widget area'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="widgettitle">',
        'after_title'   => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'Admin dashboard area'),
        'id' => 'admin-dashboard-area',
        'description' => __( 'Admin dashboard area','modman'),
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<div>',
        'after_title'   => '</div>',
    ) );
}

/**
 * This is the slider
 * It's a custom post type using the advanced custom fields-plugin with the repeater field
 */
function mm_slider() {

    $labels = array(
        'name'                => __( 'Slides'),
        'singular_name'       => __( 'Slide'),
        'add_new'             => __( 'Add New slide'),
        'add_new_item'        => __( 'Add New slide'),
        'edit_item'           => __( 'Edit slide'),
        'new_item'            => __( 'New slide'),
        'view_item'           => __( 'View slide'),
        'search_items'        => __( 'Search slides'),
        'not_found'           => __( 'No slides found'),
        'not_found_in_trash'  => __( 'No slides found in Trash'),
        'menu_name'           => __( 'Slider'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => false,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array('title')
    );

    register_post_type( 'slider', $args );
}

add_action( 'init', 'mm_slider' );

/**
 * Add an office
 */
function mm_office() {

    $labels = array(
        'name'                => __( 'Offices'),
        'singular_name'       => __( 'Office'),
        'add_new'             => __( 'Add New office'),
        'add_new_item'        => __( 'Add New office'),
        'edit_item'           => __( 'Edit office'),
        'new_item'            => __( 'New office'),
        'view_item'           => __( 'View office'),
        'search_items'        => __( 'Search offices'),
        'not_found'           => __( 'No offices found'),
        'not_found_in_trash'  => __( 'No offices found in Trash'),
        'menu_name'           => __( 'Offices'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array('title')
    );

    register_post_type( 'office', $args );
}

add_action( 'init', 'mm_office' );

/**
 * Add a case
 */
function mm_case() {

    $labels = array(
        'name'                => __( 'Cases'),
        'singular_name'       => __( 'Case'),
        'add_new'             => __( 'Add New case'),
        'add_new_item'        => __( 'Add New case'),
        'edit_item'           => __( 'Edit case'),
        'new_item'            => __( 'New case'),
        'view_item'           => __( 'View case'),
        'search_items'        => __( 'Search case'),
        'not_found'           => __( 'No cases found'),
        'not_found_in_trash'  => __( 'No cases found in Trash'),
        'menu_name'           => __( 'Cases'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'has_archive'         => true,
        'query_var'           => false,
        'can_export'          => true,
        'rewrite'             => false,
        'capability_type'     => 'post',
        'supports'            => array('title')
    );

    register_post_type( 'case', $args );
}

add_action( 'init', 'mm_case' );

/**
 * Add full case story
 */
function mm_case_story_post_type() {

    $labels = array(
        'name'                => __( 'Case Stories'),
        'singular_name'       => __( 'Case Story'),
        'add_new'             => __( 'Add New story'),
        'add_new_item'        => __( 'Add New story'),
        'edit_item'           => __( 'Edit story'),
        'new_item'            => __( 'New story'),
        'view_item'           => __( 'View story'),
        'search_items'        => __( 'Search story'),
        'not_found'           => __( 'No stories found'),
        'not_found_in_trash'  => __( 'No stories found in Trash'),
        'menu_name'           => __( 'Case Stories'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array('title','revisions')
    );

    register_post_type( 'story', $args );
}

add_action( 'init', 'mm_case_story_post_type' );

function mm_article() {

    $labels = array(
        'name'                => __( 'Articles'),
        'singular_name'       => __( 'Article'),
        'add_new'             => __( 'Add New article'),
        'add_new_item'        => __( 'Add New article'),
        'edit_item'           => __( 'Edit article'),
        'new_item'            => __( 'New article'),
        'view_item'           => __( 'View article'),
        'search_items'        => __( 'Search article'),
        'not_found'           => __( 'No articles found'),
        'not_found_in_trash'  => __( 'No articles found in Trash'),
        'menu_name'           => __( 'Articles'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        //'rewrite'             => array('slug' => 'article'),
        'supports'            => array('title', 'revisions')
    );

    register_post_type( 'article', $args );
    flush_rewrite_rules();
}

add_action( 'init', 'mm_article' );

/**
 * Add research
 */

 /*
function mm_research_post_type() {

    $labels = array(
        'name'                => __( 'Research'),
        'singular_name'       => __( 'Research'),
        'add_new'             => __( 'Add New research'),
        'add_new_item'        => __( 'Add New research'),
        'edit_item'           => __( 'Edit research'),
        'new_item'            => __( 'New research'),
        'view_item'           => __( 'View research'),
        'search_items'        => __( 'Search research'),
        'not_found'           => __( 'No research found'),
        'not_found_in_trash'  => __( 'No research found in Trash'),
        'menu_name'           => __( 'Research'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array('title','editor','excerpt','revisions')
    );

    register_post_type( 'research', $args );
}

add_action( 'init', 'mm_research_post_type' );
*/
/**
 * Add external research
 */

/*
function mm_ex_research_post_type() {

    $labels = array(
        'name'                => __( 'External Research'),
        'singular_name'       => __( 'External Research'),
        'add_new'             => __( 'Add New'),
        'add_new_item'        => __( 'Add New external research'),
        'edit_item'           => __( 'Edit research'),
        'new_item'            => __( 'New external research'),
        'view_item'           => __( 'View external research'),
        'search_items'        => __( 'Search external research'),
        'not_found'           => __( 'No external research found'),
        'not_found_in_trash'  => __( 'No external research found in Trash'),
        'menu_name'           => __( 'External Research'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array('title','editor','excerpt','revisions')
    );

    register_post_type( 'ex_research', $args );
}

add_action( 'init', 'mm_ex_research_post_type' );
*/
/**
 * Add book
 */

/*
function mm_book_post_type() {

    $labels = array(
        'name'                => __( 'Books'),
        'singular_name'       => __( 'Book'),
        'add_new'             => __( 'Add New'),
        'add_new_item'        => __( 'Add New book'),
        'edit_item'           => __( 'Edit book'),
        'new_item'            => __( 'New book'),
        'view_item'           => __( 'View book'),
        'search_items'        => __( 'Search books'),
        'not_found'           => __( 'No books found'),
        'not_found_in_trash'  => __( 'No books found in Trash'),
        'menu_name'           => __( 'Book'),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array('title','editor','excerpt','revisions')
    );

    register_post_type( 'book', $args );
}

add_action( 'init', 'mm_book_post_type' );
*/
/**
 * Quickly create pages
 */
function mm_create_pages(){
    global $user_ID;

    //Pages to create
    $pages_to_create = array('Your challenges','Publications','About us','Our expertise','Track record');

    foreach($pages_to_create as $ptc){

        //Load all pages
        $pages = get_pages();

        //Put all existing page titles in an array
        $page_titles = array();
        foreach($pages as $p){
            $page_titles[] = $p->post_title;
        }

        if(!in_array($ptc,$page_titles)){
            $page['post_type']    = 'page';
            $page['post_content'] = 'Put your page content here';
            $page['post_parent']  = 0;
            $page['post_author']  = $user_ID;
            $page['post_status']  = 'publish';
            $page['post_title']   = $ptc;
            $pageid = wp_insert_post ($page);

            unset($page);
        }
    }
}

//add_action('admin_init','mm_create_pages');

function newline_to_li($string=''){
    $explode = explode("\n", $string);
    $newstring = '';

    foreach($explode  as $bit){
        $newstring .= "<li>" . $bit . "</li>";
    }

    return $newstring;
}

/**
 * Detects shortcode
 * @return [type] [description]
 */
function mm_detect_shortcode(){
    global $post;
    if($post === null)
        return;

    $pattern = get_shortcode_regex();

    if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
        && array_key_exists( 2, $matches )
        && in_array( 'your-shortcode', $matches[2] ) )
    {
        // shortcode is being used
    }
}
add_action( 'wp', 'mm_detect_shortcode' );

/**
 * Top menu
 */
function mm_top_menu($themelocation='primary') {

    global $post;

    //Get the right menu
    $theme_locations = get_nav_menu_locations();
    $menu_obj = get_term( $theme_locations[$themelocation], 'nav_menu' );
    $menuitems = wp_get_nav_menu_items($menu_obj->slug);

    $menu = '<ul id="top-nav">';
    $counter = 0;

    $parentid = 0;

    if($post !== null && $menuitems) {
        foreach($menuitems as $t) {
            if($post->ID == $t->object_id && $t->menu_item_parent > 0) {
                $parentid = $t->menu_item_parent;
                break;
            }
        }
    }

    if($menuitems)foreach($menuitems as $i => $m) {

        $active= '';
        $top_active = '';

        if($post !== null && $post->ID == $m->object_id ) {
            $active = 'active';
            $top_active = 'active';
        }

        //Only get top levels
        if($m->menu_item_parent == 0){

            $has_children = false;

            if($parentid == $m->ID ){
                $top_active = 'active';
            }

            if(count($menuitems) < $i + 1 && $menuitems[$i+1]->menu_item_parent > 0){
                $has_children = ' has-menu ';
            }

            $menu .= '<li class="'.$top_active. $has_children;

            if($counter == 0){
                $menu .= ' first';
                //$submenuposition = 'right';
            }elseif($counter == count($menuitems)){
                 $menu .= ' last';
            }

            $menu .= '">'; //end of li

            $menu .= '<a class="desktop-href" href="'.$m->url.'"><span></span>'.$m->title.'</a>';

            //Check if NOT has submenu
            //if($menuitems[$i+1]->menu_item_parent == 0){
            //    $menu .= '<a class="mobile-href" href="'.$m->url.'">'.$m->title.'<span></span></a>';
            //}else{
                $menu .= '<a class="mobile-href" data-id="menu-item-'.$m->ID.'">'.$m->title.'<img src="'.get_template_directory_uri(). '/img/header/mobile-arrow.png" class="toggle-mobile"></a>';

                $menupos = '';
                if($counter == 0){
                    $menupos = 'right';
                }
                $menu .= ' <div class="submenu" id="menu-item-'.$m->ID.'">';
                $menu .= '<a class="submenuLink main '.$active.'" href="'.$m->url.'">'.$m->title.'</a>';
           // }


        }else{


            //Only add class main to the first one
            $menu .= '<a class="submenuLink '.$active.'"  href="'.$m->url.'">'.$m->title.'</a>';

            //Add the end of the submenu
            if(array_key_exists($i + 1, $menuitems) && $menuitems[$i+1]->menu_item_parent == 0){
                $menu .='</div>';
            }
        }

        if(@$menuitems[$i+1]->menu_item_parent == 0){
            $menu .= '</li>';
        }


        $counter++;

    }
    $menu .= '<div class="clear"></div>';
    $menu .= '</ul>';

    echo $menu;
}

/**
 * Footer Menu
 */
function mm_footer_menu($themelocation = 'primary'){

    //Get the right menu
    $theme_locations = get_nav_menu_locations();
    $menu_obj = get_term( $theme_locations[$themelocation], 'nav_menu' );
    $menuitems = wp_get_nav_menu_items($menu_obj->slug,array('order' => 'ASC'));



    $menu = '';
    $counter = 0;
    $parent_links = 0;
    $parent_count = 0;

    //Get how many parents in parents in menu
    if($menuitems){
        foreach($menuitems as $cm){
            if($cm->menu_item_parent == 0) $parent_links++;
        }

        foreach($menuitems as $m){

            $newulstart = false;

            //First wrapper
            if($counter == 0){
                $menu .= '<div class="left-uls uls" id="left-uls">';
                $newulstart = true;
            }elseif($parent_count+1 == round($parent_links/2)+1 && $m->menu_item_parent == 0){ //So it creates a div just before the split
                $menu .= '</ul></div><div class="right-uls uls" id="right-uls">';
                $newulstart = true;
            }

            //Only on parent and start
            if($newulstart == true && $m->menu_item_parent == 0){
                $menu .= '<ul><li class="title">';
                $parent_count++;
            }elseif($m->menu_item_parent == 0){
                $menu .= '</ul><ul><li class="title">';
                $parent_count++;
            }else{
                $menu .= '<li>';
            }

            $menu .= '<a href="'.$m->url.'">'.$m->title.'</a></li>';

            $counter++;

            //Last split
            if($counter == count($menuitems)){
                $menu .= '</ul></div>';
            }

        }
        $menu .= '<div class="clear"></div>';

        echo $menu;
    }

}

/**
 * Gets the current domain
 */
function current_domain(){
    //Get rid of wwww
    $domain_name =  preg_replace('/^www\./','',$_SERVER['SERVER_NAME']);

   //output the result
    return $domain_name;
}

/**
 * Redirect roles on login
 */
function mm_login_redirect( $redirect_to, $request, $user ) {
    global $user;

    if(empty($user->allcaps))
        return home_url() . '/account/';

    $edit_page = $user->allcaps['edit_pages'];

    if ( $edit_page ) {
        return admin_url('index.php');
    } else {
        return home_url() . '/account/';
    }
}

add_filter( 'login_redirect', 'mm_login_redirect', 10, 3 );

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function mm_add_dashboard_widgets() {

    wp_add_dashboard_widget(
        'Files',         // Widget slug.
        'Palma Software download page',         // Title.
        'mm_dashboard_widget_function' // Display function.
    );
}
add_action( 'wp_dashboard_setup', 'mm_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function mm_dashboard_widget_function() {


    // Display whatever it is you want to show.
    dynamic_sidebar('admin-dashboard-area');
}

add_filter('widget_text', 'do_shortcode');

/**
 * Possible to run a function once
 */
function run_once($key){
    $test_case = get_option('run_once');
    if (isset($test_case[$key]) && $test_case[$key]){
        return false;
    }else{
        $test_case[$key] = true;
        update_option('run_once',$test_case);
        return true;
    }
}

//Run add role once
if (run_once('add_role')){
    add_role('restricted','Restricted',array(
        'read' => true, 'read_private_pages' => true,'read_private_posts' => true
    ));
}

/**
 * Extra fields
 * @param  [type] $profile_fields [description]
 * @return [type]                 [description]
 */
function mm_contact_methods($profile_fields) {

    $profile_fields['company'] = __('Company','modman');
    $profile_fields['phone'] = __('Phone','modman');
    $profile_fields['found_us'] = __('Found us','modman');

    return $profile_fields;
}
add_filter('user_contactmethods', 'mm_contact_methods');

/**
 * Change loginlogo
 */
function mm_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/header/logo-wide.png);
            background-size: 80%;
            width: 100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'mm_login_logo' );

/*
    Disable Default Dashboard Widgets
    @ https://digwp.com/2014/02/disable-default-dashboard-widgets/
*/
function mm_disable_default_dashboard_widgets() {
    global $wp_meta_boxes;
    // wp..
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
}
add_action('wp_dashboard_setup', 'mm_disable_default_dashboard_widgets', 999);

/**
 * Disable wordpress update message
 */
add_action('init','mm_hide_core_update');

function mm_hide_core_update(){

    if(! current_user_can('update_core')){
        remove_action( 'init', 'wp_version_check');
        add_filter('pre_option_update_core','__return_null');
        add_filter('pre_site_transient_update_core','__return_null');
        add_filter('screen_options_show_screen', '__return_false');
    }
}

/**
 * Remove help tab
 */
add_filter( 'contextual_help', 'mm_remove_help_tabs', 999, 3 );
function mm_remove_help_tabs($old_help, $screen_id, $screen){
    $screen->remove_help_tabs();
    return $old_help;
}

/**
 * Returns the translated role of the current user. If that user has
 * no role for the current blog, it returns false.
 *
 * @return string The name of the current role
 **/
function get_current_user_role() {

    return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role] ) : false;
}

/**
 * Prevent downloads for most users
 */

function mm_restrict_download($can_download, $download){
    global $wp_roles;
    $current_user = wp_get_current_user();
    $roles = $current_user->roles;
    $role = array_shift($roles);
    $can_download = false;

    $blocked_roles = array('subscriber','restricted');

    if(is_user_logged_in() && !in_array($role,$blocked_roles) && $download->is_members_only()){
        $can_download = true;
    }

    return $can_download;

}
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if(is_plugin_active( 'download-monitor/download-monitor.php' ) ){
    add_filter( 'dlm_can_download', 'mm_restrict_download', 11, 2 );
}

/**
 * Replace Howdy
 */
function mm_replace_howdy( $wp_admin_bar ) {
    $my_account=$wp_admin_bar->get_node('my-account');
    $newtitle = str_replace( 'Howdy,', __('Welcome ','modman'), $my_account->title );
    $wp_admin_bar->add_node( array(
        'id' => 'my-account',
        'title' => __($newtitle),
    ) );
}
add_filter( 'admin_bar_menu', 'mm_replace_howdy',25 );

/**
 * Remove wp logo
 */
add_action( 'admin_bar_menu', 'mm_remove_wp_logo', 999 );

function mm_remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}

/**
 * Base urls for both regular and multisite
 */

function mm_base_url(){

    if(is_multisite()){
        $url =  network_site_url();
    }else{
        $url = site_url();
    }

    return $url;
}

/**
 * Admin css
 * - Always show image cntrols because of bug if file is deleted from media libraries
 * - Hide Download manager
 * - Hide messages
 */
add_action('admin_footer', function(){
    ?>
    <style type="text/css">
        .acf-image-uploader .hover{
            visibility: visible;
            opacity: 1;
        }

        .insert-download.dlm_insert_download{
          display: none;
        }

        .updated{
            display: none;
        }

    </style>
    <?php
}, 999);

/**
 * Array of all the countries in the world
 * @return array $countries
 */
function mm_countries(){
    $countries = array();
    $countries[] = array("code"=>"AF","name"=>"Afghanistan","d_code"=>"+93");
    $countries[] = array("code"=>"AL","name"=>"Albania","d_code"=>"+355");
    $countries[] = array("code"=>"DZ","name"=>"Algeria","d_code"=>"+213");
    $countries[] = array("code"=>"AS","name"=>"American Samoa","d_code"=>"+1");
    $countries[] = array("code"=>"AD","name"=>"Andorra","d_code"=>"+376");
    $countries[] = array("code"=>"AO","name"=>"Angola","d_code"=>"+244");
    $countries[] = array("code"=>"AI","name"=>"Anguilla","d_code"=>"+1");
    $countries[] = array("code"=>"AG","name"=>"Antigua","d_code"=>"+1");
    $countries[] = array("code"=>"AR","name"=>"Argentina","d_code"=>"+54");
    $countries[] = array("code"=>"AM","name"=>"Armenia","d_code"=>"+374");
    $countries[] = array("code"=>"AW","name"=>"Aruba","d_code"=>"+297");
    $countries[] = array("code"=>"AU","name"=>"Australia","d_code"=>"+61");
    $countries[] = array("code"=>"AT","name"=>"Austria","d_code"=>"+43");
    $countries[] = array("code"=>"AZ","name"=>"Azerbaijan","d_code"=>"+994");
    $countries[] = array("code"=>"BH","name"=>"Bahrain","d_code"=>"+973");
    $countries[] = array("code"=>"BD","name"=>"Bangladesh","d_code"=>"+880");
    $countries[] = array("code"=>"BB","name"=>"Barbados","d_code"=>"+1");
    $countries[] = array("code"=>"BY","name"=>"Belarus","d_code"=>"+375");
    $countries[] = array("code"=>"BE","name"=>"Belgium","d_code"=>"+32");
    $countries[] = array("code"=>"BZ","name"=>"Belize","d_code"=>"+501");
    $countries[] = array("code"=>"BJ","name"=>"Benin","d_code"=>"+229");
    $countries[] = array("code"=>"BM","name"=>"Bermuda","d_code"=>"+1");
    $countries[] = array("code"=>"BT","name"=>"Bhutan","d_code"=>"+975");
    $countries[] = array("code"=>"BO","name"=>"Bolivia","d_code"=>"+591");
    $countries[] = array("code"=>"BA","name"=>"Bosnia and Herzegovina","d_code"=>"+387");
    $countries[] = array("code"=>"BW","name"=>"Botswana","d_code"=>"+267");
    $countries[] = array("code"=>"BR","name"=>"Brazil","d_code"=>"+55");
    $countries[] = array("code"=>"IO","name"=>"British Indian Ocean Territory","d_code"=>"+246");
    $countries[] = array("code"=>"VG","name"=>"British Virgin Islands","d_code"=>"+1");
    $countries[] = array("code"=>"BN","name"=>"Brunei","d_code"=>"+673");
    $countries[] = array("code"=>"BG","name"=>"Bulgaria","d_code"=>"+359");
    $countries[] = array("code"=>"BF","name"=>"Burkina Faso","d_code"=>"+226");
    $countries[] = array("code"=>"MM","name"=>"Burma Myanmar" ,"d_code"=>"+95");
    $countries[] = array("code"=>"BI","name"=>"Burundi","d_code"=>"+257");
    $countries[] = array("code"=>"KH","name"=>"Cambodia","d_code"=>"+855");
    $countries[] = array("code"=>"CM","name"=>"Cameroon","d_code"=>"+237");
    $countries[] = array("code"=>"CA","name"=>"Canada","d_code"=>"+1");
    $countries[] = array("code"=>"CV","name"=>"Cape Verde","d_code"=>"+238");
    $countries[] = array("code"=>"KY","name"=>"Cayman Islands","d_code"=>"+1");
    $countries[] = array("code"=>"CF","name"=>"Central African Republic","d_code"=>"+236");
    $countries[] = array("code"=>"TD","name"=>"Chad","d_code"=>"+235");
    $countries[] = array("code"=>"CL","name"=>"Chile","d_code"=>"+56");
    $countries[] = array("code"=>"CN","name"=>"China","d_code"=>"+86");
    $countries[] = array("code"=>"CO","name"=>"Colombia","d_code"=>"+57");
    $countries[] = array("code"=>"KM","name"=>"Comoros","d_code"=>"+269");
    $countries[] = array("code"=>"CK","name"=>"Cook Islands","d_code"=>"+682");
    $countries[] = array("code"=>"CR","name"=>"Costa Rica","d_code"=>"+506");
    $countries[] = array("code"=>"CI","name"=>"Côte d'Ivoire" ,"d_code"=>"+225");
    $countries[] = array("code"=>"HR","name"=>"Croatia","d_code"=>"+385");
    $countries[] = array("code"=>"CU","name"=>"Cuba","d_code"=>"+53");
    $countries[] = array("code"=>"CY","name"=>"Cyprus","d_code"=>"+357");
    $countries[] = array("code"=>"CZ","name"=>"Czech Republic","d_code"=>"+420");
    $countries[] = array("code"=>"CD","name"=>"Democratic Republic of Congo","d_code"=>"+243");
    $countries[] = array("code"=>"DK","name"=>"Denmark","d_code"=>"+45");
    $countries[] = array("code"=>"DJ","name"=>"Djibouti","d_code"=>"+253");
    $countries[] = array("code"=>"DM","name"=>"Dominica","d_code"=>"+1");
    $countries[] = array("code"=>"DO","name"=>"Dominican Republic","d_code"=>"+1");
    $countries[] = array("code"=>"EC","name"=>"Ecuador","d_code"=>"+593");
    $countries[] = array("code"=>"EG","name"=>"Egypt","d_code"=>"+20");
    $countries[] = array("code"=>"SV","name"=>"El Salvador","d_code"=>"+503");
    $countries[] = array("code"=>"GQ","name"=>"Equatorial Guinea","d_code"=>"+240");
    $countries[] = array("code"=>"ER","name"=>"Eritrea","d_code"=>"+291");
    $countries[] = array("code"=>"EE","name"=>"Estonia","d_code"=>"+372");
    $countries[] = array("code"=>"ET","name"=>"Ethiopia","d_code"=>"+251");
    $countries[] = array("code"=>"FK","name"=>"Falkland Islands","d_code"=>"+500");
    $countries[] = array("code"=>"FO","name"=>"Faroe Islands","d_code"=>"+298");
    $countries[] = array("code"=>"FM","name"=>"Federated States of Micronesia","d_code"=>"+691");
    $countries[] = array("code"=>"FJ","name"=>"Fiji","d_code"=>"+679");
    $countries[] = array("code"=>"FI","name"=>"Finland","d_code"=>"+358");
    $countries[] = array("code"=>"FR","name"=>"France","d_code"=>"+33");
    $countries[] = array("code"=>"GF","name"=>"French Guiana","d_code"=>"+594");
    $countries[] = array("code"=>"PF","name"=>"French Polynesia","d_code"=>"+689");
    $countries[] = array("code"=>"GA","name"=>"Gabon","d_code"=>"+241");
    $countries[] = array("code"=>"GE","name"=>"Georgia","d_code"=>"+995");
    $countries[] = array("code"=>"DE","name"=>"Germany","d_code"=>"+49");
    $countries[] = array("code"=>"GH","name"=>"Ghana","d_code"=>"+233");
    $countries[] = array("code"=>"GI","name"=>"Gibraltar","d_code"=>"+350");
    $countries[] = array("code"=>"GR","name"=>"Greece","d_code"=>"+30");
    $countries[] = array("code"=>"GL","name"=>"Greenland","d_code"=>"+299");
    $countries[] = array("code"=>"GD","name"=>"Grenada","d_code"=>"+1");
    $countries[] = array("code"=>"GP","name"=>"Guadeloupe","d_code"=>"+590");
    $countries[] = array("code"=>"GU","name"=>"Guam","d_code"=>"+1");
    $countries[] = array("code"=>"GT","name"=>"Guatemala","d_code"=>"+502");
    $countries[] = array("code"=>"GN","name"=>"Guinea","d_code"=>"+224");
    $countries[] = array("code"=>"GW","name"=>"Guinea-Bissau","d_code"=>"+245");
    $countries[] = array("code"=>"GY","name"=>"Guyana","d_code"=>"+592");
    $countries[] = array("code"=>"HT","name"=>"Haiti","d_code"=>"+509");
    $countries[] = array("code"=>"HN","name"=>"Honduras","d_code"=>"+504");
    $countries[] = array("code"=>"HK","name"=>"Hong Kong","d_code"=>"+852");
    $countries[] = array("code"=>"HU","name"=>"Hungary","d_code"=>"+36");
    $countries[] = array("code"=>"IS","name"=>"Iceland","d_code"=>"+354");
    $countries[] = array("code"=>"IN","name"=>"India","d_code"=>"+91");
    $countries[] = array("code"=>"ID","name"=>"Indonesia","d_code"=>"+62");
    $countries[] = array("code"=>"IR","name"=>"Iran","d_code"=>"+98");
    $countries[] = array("code"=>"IQ","name"=>"Iraq","d_code"=>"+964");
    $countries[] = array("code"=>"IE","name"=>"Ireland","d_code"=>"+353");
    $countries[] = array("code"=>"IL","name"=>"Israel","d_code"=>"+972");
    $countries[] = array("code"=>"IT","name"=>"Italy","d_code"=>"+39");
    $countries[] = array("code"=>"JM","name"=>"Jamaica","d_code"=>"+1");
    $countries[] = array("code"=>"JP","name"=>"Japan","d_code"=>"+81");
    $countries[] = array("code"=>"JO","name"=>"Jordan","d_code"=>"+962");
    $countries[] = array("code"=>"KZ","name"=>"Kazakhstan","d_code"=>"+7");
    $countries[] = array("code"=>"KE","name"=>"Kenya","d_code"=>"+254");
    $countries[] = array("code"=>"KI","name"=>"Kiribati","d_code"=>"+686");
    $countries[] = array("code"=>"XK","name"=>"Kosovo","d_code"=>"+381");
    $countries[] = array("code"=>"KW","name"=>"Kuwait","d_code"=>"+965");
    $countries[] = array("code"=>"KG","name"=>"Kyrgyzstan","d_code"=>"+996");
    $countries[] = array("code"=>"LA","name"=>"Laos","d_code"=>"+856");
    $countries[] = array("code"=>"LV","name"=>"Latvia","d_code"=>"+371");
    $countries[] = array("code"=>"LB","name"=>"Lebanon","d_code"=>"+961");
    $countries[] = array("code"=>"LS","name"=>"Lesotho","d_code"=>"+266");
    $countries[] = array("code"=>"LR","name"=>"Liberia","d_code"=>"+231");
    $countries[] = array("code"=>"LY","name"=>"Libya","d_code"=>"+218");
    $countries[] = array("code"=>"LI","name"=>"Liechtenstein","d_code"=>"+423");
    $countries[] = array("code"=>"LT","name"=>"Lithuania","d_code"=>"+370");
    $countries[] = array("code"=>"LU","name"=>"Luxembourg","d_code"=>"+352");
    $countries[] = array("code"=>"MO","name"=>"Macau","d_code"=>"+853");
    $countries[] = array("code"=>"MK","name"=>"Macedonia","d_code"=>"+389");
    $countries[] = array("code"=>"MG","name"=>"Madagascar","d_code"=>"+261");
    $countries[] = array("code"=>"MW","name"=>"Malawi","d_code"=>"+265");
    $countries[] = array("code"=>"MY","name"=>"Malaysia","d_code"=>"+60");
    $countries[] = array("code"=>"MV","name"=>"Maldives","d_code"=>"+960");
    $countries[] = array("code"=>"ML","name"=>"Mali","d_code"=>"+223");
    $countries[] = array("code"=>"MT","name"=>"Malta","d_code"=>"+356");
    $countries[] = array("code"=>"MH","name"=>"Marshall Islands","d_code"=>"+692");
    $countries[] = array("code"=>"MQ","name"=>"Martinique","d_code"=>"+596");
    $countries[] = array("code"=>"MR","name"=>"Mauritania","d_code"=>"+222");
    $countries[] = array("code"=>"MU","name"=>"Mauritius","d_code"=>"+230");
    $countries[] = array("code"=>"YT","name"=>"Mayotte","d_code"=>"+262");
    $countries[] = array("code"=>"MX","name"=>"Mexico","d_code"=>"+52");
    $countries[] = array("code"=>"MD","name"=>"Moldova","d_code"=>"+373");
    $countries[] = array("code"=>"MC","name"=>"Monaco","d_code"=>"+377");
    $countries[] = array("code"=>"MN","name"=>"Mongolia","d_code"=>"+976");
    $countries[] = array("code"=>"ME","name"=>"Montenegro","d_code"=>"+382");
    $countries[] = array("code"=>"MS","name"=>"Montserrat","d_code"=>"+1");
    $countries[] = array("code"=>"MA","name"=>"Morocco","d_code"=>"+212");
    $countries[] = array("code"=>"MZ","name"=>"Mozambique","d_code"=>"+258");
    $countries[] = array("code"=>"NA","name"=>"Namibia","d_code"=>"+264");
    $countries[] = array("code"=>"NR","name"=>"Nauru","d_code"=>"+674");
    $countries[] = array("code"=>"NP","name"=>"Nepal","d_code"=>"+977");
    $countries[] = array("code"=>"NL","name"=>"Netherlands","d_code"=>"+31");
    $countries[] = array("code"=>"AN","name"=>"Netherlands Antilles","d_code"=>"+599");
    $countries[] = array("code"=>"NC","name"=>"New Caledonia","d_code"=>"+687");
    $countries[] = array("code"=>"NZ","name"=>"New Zealand","d_code"=>"+64");
    $countries[] = array("code"=>"NI","name"=>"Nicaragua","d_code"=>"+505");
    $countries[] = array("code"=>"NE","name"=>"Niger","d_code"=>"+227");
    $countries[] = array("code"=>"NG","name"=>"Nigeria","d_code"=>"+234");
    $countries[] = array("code"=>"NU","name"=>"Niue","d_code"=>"+683");
    $countries[] = array("code"=>"NF","name"=>"Norfolk Island","d_code"=>"+672");
    $countries[] = array("code"=>"KP","name"=>"North Korea","d_code"=>"+850");
    $countries[] = array("code"=>"MP","name"=>"Northern Mariana Islands","d_code"=>"+1");
    $countries[] = array("code"=>"NO","name"=>"Norway","d_code"=>"+47");
    $countries[] = array("code"=>"OM","name"=>"Oman","d_code"=>"+968");
    $countries[] = array("code"=>"PK","name"=>"Pakistan","d_code"=>"+92");
    $countries[] = array("code"=>"PW","name"=>"Palau","d_code"=>"+680");
    $countries[] = array("code"=>"PS","name"=>"Palestine","d_code"=>"+970");
    $countries[] = array("code"=>"PA","name"=>"Panama","d_code"=>"+507");
    $countries[] = array("code"=>"PG","name"=>"Papua New Guinea","d_code"=>"+675");
    $countries[] = array("code"=>"PY","name"=>"Paraguay","d_code"=>"+595");
    $countries[] = array("code"=>"PE","name"=>"Peru","d_code"=>"+51");
    $countries[] = array("code"=>"PH","name"=>"Philippines","d_code"=>"+63");
    $countries[] = array("code"=>"PL","name"=>"Poland","d_code"=>"+48");
    $countries[] = array("code"=>"PT","name"=>"Portugal","d_code"=>"+351");
    $countries[] = array("code"=>"PR","name"=>"Puerto Rico","d_code"=>"+1");
    $countries[] = array("code"=>"QA","name"=>"Qatar","d_code"=>"+974");
    $countries[] = array("code"=>"CG","name"=>"Republic of the Congo","d_code"=>"+242");
    $countries[] = array("code"=>"RE","name"=>"Réunion" ,"d_code"=>"+262");
    $countries[] = array("code"=>"RO","name"=>"Romania","d_code"=>"+40");
    $countries[] = array("code"=>"RU","name"=>"Russia","d_code"=>"+7");
    $countries[] = array("code"=>"RW","name"=>"Rwanda","d_code"=>"+250");
    $countries[] = array("code"=>"BL","name"=>"Saint Barthélemy" ,"d_code"=>"+590");
    $countries[] = array("code"=>"SH","name"=>"Saint Helena","d_code"=>"+290");
    $countries[] = array("code"=>"KN","name"=>"Saint Kitts and Nevis","d_code"=>"+1");
    $countries[] = array("code"=>"MF","name"=>"Saint Martin","d_code"=>"+590");
    $countries[] = array("code"=>"PM","name"=>"Saint Pierre and Miquelon","d_code"=>"+508");
    $countries[] = array("code"=>"VC","name"=>"Saint Vincent and the Grenadines","d_code"=>"+1");
    $countries[] = array("code"=>"WS","name"=>"Samoa","d_code"=>"+685");
    $countries[] = array("code"=>"SM","name"=>"San Marino","d_code"=>"+378");
    $countries[] = array("code"=>"ST","name"=>"São Tomé and Príncipe" ,"d_code"=>"+239");
    $countries[] = array("code"=>"SA","name"=>"Saudi Arabia","d_code"=>"+966");
    $countries[] = array("code"=>"SN","name"=>"Senegal","d_code"=>"+221");
    $countries[] = array("code"=>"RS","name"=>"Serbia","d_code"=>"+381");
    $countries[] = array("code"=>"SC","name"=>"Seychelles","d_code"=>"+248");
    $countries[] = array("code"=>"SL","name"=>"Sierra Leone","d_code"=>"+232");
    $countries[] = array("code"=>"SG","name"=>"Singapore","d_code"=>"+65");
    $countries[] = array("code"=>"SK","name"=>"Slovakia","d_code"=>"+421");
    $countries[] = array("code"=>"SI","name"=>"Slovenia","d_code"=>"+386");
    $countries[] = array("code"=>"SB","name"=>"Solomon Islands","d_code"=>"+677");
    $countries[] = array("code"=>"SO","name"=>"Somalia","d_code"=>"+252");
    $countries[] = array("code"=>"ZA","name"=>"South Africa","d_code"=>"+27");
    $countries[] = array("code"=>"KR","name"=>"South Korea","d_code"=>"+82");
    $countries[] = array("code"=>"ES","name"=>"Spain","d_code"=>"+34");
    $countries[] = array("code"=>"LK","name"=>"Sri Lanka","d_code"=>"+94");
    $countries[] = array("code"=>"LC","name"=>"St. Lucia","d_code"=>"+1");
    $countries[] = array("code"=>"SD","name"=>"Sudan","d_code"=>"+249");
    $countries[] = array("code"=>"SR","name"=>"Suriname","d_code"=>"+597");
    $countries[] = array("code"=>"SZ","name"=>"Swaziland","d_code"=>"+268");
    $countries[] = array("code"=>"SE","name"=>"Sweden","d_code"=>"+46");
    $countries[] = array("code"=>"CH","name"=>"Switzerland","d_code"=>"+41");
    $countries[] = array("code"=>"SY","name"=>"Syria","d_code"=>"+963");
    $countries[] = array("code"=>"TW","name"=>"Taiwan","d_code"=>"+886");
    $countries[] = array("code"=>"TJ","name"=>"Tajikistan","d_code"=>"+992");
    $countries[] = array("code"=>"TZ","name"=>"Tanzania","d_code"=>"+255");
    $countries[] = array("code"=>"TH","name"=>"Thailand","d_code"=>"+66");
    $countries[] = array("code"=>"BS","name"=>"The Bahamas","d_code"=>"+1");
    $countries[] = array("code"=>"GM","name"=>"The Gambia","d_code"=>"+220");
    $countries[] = array("code"=>"TL","name"=>"Timor-Leste","d_code"=>"+670");
    $countries[] = array("code"=>"TG","name"=>"Togo","d_code"=>"+228");
    $countries[] = array("code"=>"TK","name"=>"Tokelau","d_code"=>"+690");
    $countries[] = array("code"=>"TO","name"=>"Tonga","d_code"=>"+676");
    $countries[] = array("code"=>"TT","name"=>"Trinidad and Tobago","d_code"=>"+1");
    $countries[] = array("code"=>"TN","name"=>"Tunisia","d_code"=>"+216");
    $countries[] = array("code"=>"TR","name"=>"Turkey","d_code"=>"+90");
    $countries[] = array("code"=>"TM","name"=>"Turkmenistan","d_code"=>"+993");
    $countries[] = array("code"=>"TC","name"=>"Turks and Caicos Islands","d_code"=>"+1");
    $countries[] = array("code"=>"TV","name"=>"Tuvalu","d_code"=>"+688");
    $countries[] = array("code"=>"UG","name"=>"Uganda","d_code"=>"+256");
    $countries[] = array("code"=>"UA","name"=>"Ukraine","d_code"=>"+380");
    $countries[] = array("code"=>"AE","name"=>"United Arab Emirates","d_code"=>"+971");
    $countries[] = array("code"=>"GB","name"=>"United Kingdom","d_code"=>"+44");
    $countries[] = array("code"=>"US","name"=>"United States","d_code"=>"+1");
    $countries[] = array("code"=>"UY","name"=>"Uruguay","d_code"=>"+598");
    $countries[] = array("code"=>"VI","name"=>"US Virgin Islands","d_code"=>"+1");
    $countries[] = array("code"=>"UZ","name"=>"Uzbekistan","d_code"=>"+998");
    $countries[] = array("code"=>"VU","name"=>"Vanuatu","d_code"=>"+678");
    $countries[] = array("code"=>"VA","name"=>"Vatican City","d_code"=>"+39");
    $countries[] = array("code"=>"VE","name"=>"Venezuela","d_code"=>"+58");
    $countries[] = array("code"=>"VN","name"=>"Vietnam","d_code"=>"+84");
    $countries[] = array("code"=>"WF","name"=>"Wallis and Futuna","d_code"=>"+681");
    $countries[] = array("code"=>"YE","name"=>"Yemen","d_code"=>"+967");
    $countries[] = array("code"=>"ZM","name"=>"Zambia","d_code"=>"+260");
    $countries[] = array("code"=>"ZW","name"=>"Zimbabwe","d_code"=>"+263");
    
    return $countries;
}

function get_default_country_code() {
  $path = 'us';//str_replace('/','',get_blog_details(get_current_blog_id())->path);

  $default = '+46';
  if($path == 'de'){
      $default = '+49';
  }elseif($path == 'us'){
      $default = '+1';
  }elseif($path == 'zh'){
      $default = '+86';
  }
  return $default;
}
/**
 * Adds a shortcode to contact form 7
 */
function mm_contact_form_phone_field($tag){

    $name = 'country';
    /*if(!empty($tag['name'])){
        $name = $tag['name'];
    }*/

    /**
     * Different default by path
     */
    $default = get_default_country_code();


    $content = '<div  style="position: relative; width: 56px; float: left;">
    <div id="default-value"><span>'.$default.'</span> <img id="default-arrow" src="'. get_template_directory_uri() . '/img/arrow-down.png"></div>
    <div class="phone-field">
    <select name="'.$name.'" class="country" id="country" style="width: 55px;">';

        foreach(mm_countries() as $c):
            $selected = '';
            if($default == $c['d_code']){
                $selected = 'selected="selected"';
            }
            $content .= '<option '.$selected.' value="'.$c['d_code']. '">'.$c['name'] . '</option>';
        endforeach;

    $content .= '</select></div></div>';

    return $content;

}

function mm_wpcf7_load(){
    wpcf7_add_shortcode('countrylist', 'mm_contact_form_phone_field',true);
}

add_action( 'wpcf7_init', 'mm_wpcf7_load' );

/**
 * REMOVE COMMENTS
 */
// Removes from admin menu
add_action( 'admin_menu', 'mm_remove_admin_menus' );
function mm_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page('edit.php');
}
// Removes from post and pages
add_action('init', 'mm_remove_comment_support', 100);

function mm_remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mm_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mm_admin_bar_render' );


/**
 * ACF Saving
 */
add_filter('acf/settings/load_json', 'mm_acf_json_load_point');

function mm_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);


    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';


    // return
    return $paths;

}

add_filter('acf/settings/save_json', 'mm_acf_json_save_point');

function mm_acf_json_save_point( $path ) {

    // update path
    $path = get_stylesheet_directory() . '/acf-json';


    // return
    return $path;

}

function mm_custom_admin_menu() {

    $role = get_role('editor');
    $role->add_cap('edit_theme_options');

    $user = new WP_User(get_current_user_id());
    if (!empty( $user->roles) && is_array($user->roles)) {
        foreach ($user->roles as $role)
            $role = $role;
    }

    if($role == "editor") {
       remove_submenu_page( 'themes.php', 'themes.php' );
       remove_submenu_page( 'themes.php', 'nav-menus.php' );
    }
}

add_action('admin_menu', 'mm_custom_admin_menu');

/**
 * Add a small intruction on the publications
 */
add_action( "add_meta_boxes", "mm_add_meta_boxes_publication" );

// Register Your Meta box
function mm_add_meta_boxes_publication( $post ){

    foreach(array('article','research','ex_research','book') as $p){
        add_meta_box(
           'mm_instructions', // this is HTML id
           'Instructions',
           'mm_custom_meta_box', // the callback function
           $p, // register on post type = page
           'side', //
           'core'
        );
    }
}

function mm_custom_meta_box( $post ){

   echo '
   <h2>The Content</h2>
   The big textarea will be unavailable to non registered users if visibility is set to private.
   <br /><br />
   <h2>The Excerpt</h2>
   The area called <strong>excerpt</strong> is an introduction to the content and will be available even if set to private ';

}

/**
 * Force all network uploads to reside in "wp-content/uploads", and by-pass
 * "files" URL rewrite for site-specific directories.
 *
 * @link    http://wordpress.stackexchange.com/q/147750/1685
 *
 * @param   array   $dirs
 * @return  array
 */
function mm_upload_dir( $dirs ) {

    $dirs['baseurl'] = network_site_url( '/wp-content/uploads' );
    $dirs['basedir'] = ABSPATH . 'wp-content/uploads';
    $dirs['path'] = $dirs['basedir'] . $dirs['subdir'];
    $dirs['url'] = $dirs['baseurl'] . $dirs['subdir'];

    return $dirs;
}

add_filter( 'upload_dir', 'mm_upload_dir' );

/**
 * Change signup location from default multisite
 */

add_filter( 'wp_signup_location', 'mm_custom_signup_location' );
function mm_custom_signup_location( $url ) {

    $http_post = ('POST' == $_SERVER['REQUEST_METHOD']);

    if ( !get_option('users_can_register') ) {
        wp_redirect( site_url('wp-login.php?registration=disabled') );
        exit();
    }

    $user_login = '';
    $user_email = '';
    if ( $http_post ) {
        $user_login = $_POST['user_login'];
        $user_email = $_POST['user_email'];
        $errors_or_userid = register_new_user($user_login, $user_email);
        if ( !is_wp_error($errors_or_userid) ) {

            $secure_cookie = is_ssl() ? true : false;
            wp_set_auth_cookie( $errors_or_userid, true, $secure_cookie );
            $sites = wp_get_sites();

            foreach($sites as $site){
                 add_user_to_blog( $site['blog_id'], $errors_or_userid, 'restricted' ); //Add to all sites
            }

            wp_safe_redirect( home_url( '/' ) );
            exit;
        }
    }

    $registration_redirect = ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
    /**
     * Filter the registration redirect URL.
     *
     * @since 3.0.0
     *
     * @param string $registration_redirect The redirect destination URL.
     */
    $redirect_to = apply_filters( 'registration_redirect', $registration_redirect );
    login_header(__('Registration Form'), '<p class="message register">' . __('Register For This Site') . '</p>', $errors);
?>

<form name="registerform" id="registerform" action="<?php echo esc_url( site_url('wp-login.php?action=register', 'login_post') ); ?>" method="post" novalidate="novalidate">
    <p>
        <label for="user_login"><?php _e('Username','modman') ?><br />
        <input type="text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr(wp_unslash($user_login)); ?>" size="20" /></label>
    </p>
    <p>
        <label for="user_email"><?php _e('E-mail','modman') ?><br />
        <input type="email" name="user_email" id="user_email" class="input" value="<?php echo esc_attr( wp_unslash( $user_email ) ); ?>" size="25" /></label>
    </p>
    <?php
    /**
     * Fires following the 'E-mail' field in the user registration form.
     *
     * @since 2.1.0
     */
    do_action( 'register_form' );
    ?>
    <br class="clear" />
    <input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />
    <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Register'); ?>" /></p>
</form>

<p id="nav">
<a href="<?php echo esc_url( wp_login_url() ); ?>"><?php _e( 'Log in' ,'modman'); ?></a> |
<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" title="<?php esc_attr_e( 'Password Lost and Found' ) ?>"><?php _e( 'Lost your password?' ,'modman'); ?></a>
</p>

<?php
login_footer('user_login');
}

/**
 * Hides all files that ends with .exe
 */
//add_filter( 'posts_where', 'mm_wpquery_where' );
function mm_wpquery_where( $where ){
    global $current_user;

    if( is_user_logged_in() ){
         // logged in user, but are we viewing the library?
         if( isset( $_POST['action'] ) && ( $_POST['action'] == 'query-attachments' ) ){
            // here you can add some extra logic if you'd want to.
            $where .= ' AND guid NOT LIKE "%.exe"';
        }
    }

    return $where;
}

/**
 * Hides acf for editors
 */

add_action( 'body_class', 'mm_bodyclass');
function mm_bodyclass( $classes ) {

    global $blog_id;
    //$current_blog_details = get_blog_details( array( 'blog_id' => $blog_id ) );
    $classes[] = 'us'; //str_replace('/','',$current_blog_details->path);
     return $classes;
}

/**
 * Something in wordpress doesn't want to translate read more
 */
function _mm($string=''){

    $lang=get_bloginfo("language");
    if($lang=='de-DE'){

        switch ($string){

            case trim('Read more'):
                echo 'Lesen sie mehr';
                break;
            default:
            echo 'Read more';
        }

    }else{
        echo $string;
    }

}

/**
 * Allow file endings
 * @param  array  $mime_types [description]
 * @return [type]             [description]
 */
function mm_enable_extended_upload ( $mime_types =array() ) {

    $mime_types['exe'] = 'application/x-msdownload' ;

   return $mime_types;
}

add_filter('upload_mimes', 'mm_enable_extended_upload');

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

/**
 * Get ip information
 * @param  [type]  $ip          [description]
 * @param  string  $purpose     [description]
 * @param  boolean $deep_detect [description]
 * @return [type]               [description]
 */
function mm_ip_info() {
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    $country  = "Unknown";

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $ip_data_in = curl_exec($ch); // string
    curl_close($ch);

    $ip_data = json_decode($ip_data_in,true);
    $ip_data = str_replace('&quot;', '"', $ip_data); // for PHP 5.2 see stackoverflow.com/questions/3110487/

    if($ip_data && $ip_data['geoplugin_countryCode'] != null) {
        $country = $ip_data['geoplugin_countryCode'];
    }
    return $country;
}


/**
 * Disable emojis
 * @link( http://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2, link)
 */

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

remove_action("admin_color_scheme_picker", "admin_color_scheme_picker");

/**
 * Ajax login popup
 *
 * @link  http://www.blueleafstudio.net/make-wordpress-ajax-login-form/
 */
add_action( 'wp_ajax_nopriv_ajaxlogin', 'mm_ajax_login' );

function mm_ajax_login(){
    /**
      Captcha should exist for login, only signup
    */

    //error_log('my 0');
    //session_start();
    //include_once($_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins/si-captcha-for-wordpress/captcha/securimage.php');


    //error_log('my 1');


    //error_log($_SERVER['DOCUMENT_ROOT']);


    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;


    //$securimage = new Securimage_Captcha_si();
    //error_log('Captcha:');
    //error_log($_POST['captcha_code']);
    /*if ($securimage->check($_POST['captcha_code']) == false) {
        error_log('error in captcha code');
        echo json_encode(array('loggedin'=>false, 'message'=>__('Incorrect CAPTCHA')));
        die();
    }*/

    //error_log('my 2');

    if(WP_ENV == 'development'){
        $user_signon = wp_signon( $info, false );
    }else{
        $user_signon = wp_signon( $info, true );
    }

    //error_log('my 3');
    if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
        //wp_set_auth_cookie($user_signon->ID);
    }
    //error_log('my 4');
    die();
}


// Register a URL that will set this variable to true
add_action( 'init', 'mm_account_init' );
function mm_account_init() {
    add_rewrite_rule( '^account$', 'index.php?mm_account=true', 'top' );
}

// But WordPress has a whitelist of variables it allows, so we must put it on that list
add_action( 'query_vars', 'mm_account_query_vars' );
function mm_account_query_vars( $query_vars )
{
    $query_vars[] = 'mm_account';
    $query_vars[] = 'password-reset';
    return $query_vars;
}

// Add specific CSS class by filter
function mm_account_class_names( $classes ) {
    // add 'class-name' to the $classes array
    $classes[] = 'account';
    // return the $classes array
    return $classes;
}

// If this is done, we can access it later
// This example checks very early in the process:
// if the variable is set, we include our page and stop execution after it
add_action( 'parse_request', 'mm_account_parse_request' );
function mm_account_parse_request( &$wp )
{
    if ( array_key_exists( 'mm_account', $wp->query_vars ) ) {

        if(is_user_logged_in()){
            add_filter( 'body_class', 'mm_account_class_names' );
            include( get_template_directory() . '/account.php' );
        }

        //exit();
    }
}

function mm_logout_shortcode( $atts, $content = null, $shortcode_name = null ) {
    $defaults = array(
            'text'     => __( 'Log out' ), // This is the default log out text from wp-includes/general-template.php
            'redirect' => home_url(), // path/URL to redirect to after logging out
            'class'    => 'logout', // CSS Class(es) to use in link.
    );

    if ( ! is_user_logged_in() ) {
        return '';
    }

    $atts = shortcode_atts( $defaults, $atts );

    if ( 'logout_to_home' == $shortcode_name ) {
        $atts['redirect'] = 'home';
    } else if ( 'logout_to_current' == $shortcode_name ) {
        $atts['redirect'] = 'current';
    }

    if ( 'home' == $atts['redirect'] ) {
            $atts['redirect'] = home_url();
    } else if ( 'current' == $atts['redirect'] ) {
        $atts['redirect'] = get_the_permalink();
    }

    $url = str_replace(array('http://','https://'),'',esc_url( wp_logout_url( $atts['redirect'] ) ));

    return $url;

}
add_shortcode( 'logout', 'mm_logout_shortcode' );
add_shortcode( 'logout_to_current', 'mm_logout_shortcode' );
add_shortcode( 'logout_to_home', 'mm_logout_shortcode' );

function mm_reset_password() {
    // reset a users password
    if(isset($_POST['mm_change_nonce'])) {

        global $user_ID;

        if(!is_user_logged_in()){
            return;
        }

        $user = get_user_by( 'id', $user_ID );
        $pass = $_POST['old_password'];

        if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID) ){
           //Continue
        }else{
           mm_errors()->add('password_wrong', __('Wrong current password', 'modman'));
       }

        if(wp_verify_nonce($_POST['mm_change_nonce'], 'mm-password-nonce')) {

            if($_POST['new_password'] == '' || $_POST['repeat_password'] == '') {
                // password(s) field empty
                mm_errors()->add('password_empty', __('Please enter a password, and confirm it', 'modman'));
            }
            if($_POST['new_password'] != $_POST['repeat_password']) {
                // passwords do not match
                mm_errors()->add('password_mismatch', __('Passwords do not match', 'modman'));
            }

            // retrieve all error messages, if any
            $errors = mm_errors()->get_error_messages();

            if(empty($errors)) {
                // change the password here
                $user_data = array(
                    'ID' => $user_ID,
                    'user_pass' => $_POST['new_password']
                );
                wp_update_user($user_data);

                wp_redirect(add_query_arg('password-reset', 'true',get_bloginfo('url') . '/account/'));
                exit;
            }
        }
    }
}
add_action('init', 'mm_reset_password');

if(!function_exists('mm_error_messages')) {
    // displays error messages from form submissions
    function mm_error_messages() {
        if($codes = mm_errors()->get_error_codes()) {
            echo '<div class="mm_message error">';
                // Loop error codes and display errors
               foreach($codes as $code){
                    $message = mm_errors()->get_error_message($code);
                    echo '<span class="mm_error"><strong>' . __('Error', 'rcp') . '</strong>: ' . $message . '</span><br/>';
                }
            echo '</div>';
        }
    }
}

if(!function_exists('mm_errors')) {
    // used for tracking error messages
    function mm_errors(){
        static $wp_error; // Will hold global variable safely
        return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
    }
}

add_action('wp_logout','mm_go_home');
function mm_go_home(){
  wp_redirect( home_url() );
  exit();
}


// Only display to administrators
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}


/* Changes from "username" to "email" in the login form
    Will affect this html:
    <label for="user_login">Username or Email</label>
*/
function mm_change_username_label( $defaults ){
    $defaults['label_username'] = __( 'Email' );
    return $defaults;
}
add_filter( 'login_form_defaults', 'mm_change_username_label' );

function custom_fields_to_excerpts($content, $post, $query) {
    $images = get_post_meta($post->ID, 'gallery', true);
    if( $images ) {
        foreach( $images as $image ) {
            $content .= " " . $image['title'];
            $content .= " " . $image['caption'];
        }
        return $content;
    }
}

// Get Relevanssi to display excerpts from your custom fields
function excerpt_function($content, $post, $query) {

    global $wpdb;
    $fields = $wpdb->get_col("SELECT meta_value FROM $wpdb->postmeta WHERE post_id = $post->ID AND meta_value LIKE '%$query%' LIMIT 1");

    return implode(' ', $fields);
}
add_filter('relevanssi_excerpt_content', 'excerpt_function', 10, 3);