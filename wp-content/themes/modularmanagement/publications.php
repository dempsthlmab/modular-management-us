<?php
/*

*/
	get_header();
?>
	
	<div id="content">
		<!-- hero -->

		<div id="hero-image" style="background: url('<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg'); background-size: cover;">
			<img src="<?php echo get_template_directory_uri(); ?>/img/hero-example.jpg">
		</div>

		<div id="publications-wrapper">

			<!-- qoute section -->
		  	<div class="qoute-section">
		  		<div class="inner-section">
			  		<p class="qoute-title">
			  			Publications
			  		</p>

					<p class="text">		
						Here you can find a variety of publications on the topics of modularity and product architecture. From research papers and theses by Modular Management associates, to case studies and external publications, you can find out more about the theory and practice of modularization.
					</p>
				</div>
		  	</div>


		  	<div id="publications">
		  		<div class="left-column">

		  			<div class="publication grey">
		  				<div class="publication-content">
					    	
					    	<strong>Research</strong>
					    	<p class="italic">- Research papers and theses by Modular Management associates.</p>
							
							<!-- 
								data id on button to know wich article to open
							-->

							<span class="cta-button case-button" data-id="research">Read more</span>

							<div class="full-publication" id="research">
								<div class="pdf-publication">
									<p class="italic">Research</p>
									<p class="bold">Part Number Count is a Useful Indicator for Complexity Cost</p>
									<p class="light">By John Niethammer, Ph.D.</p>

									<p><a href="link-to-pdf.pdf" target="_blank">Download</a> or <a href="link-to-pdf.pdf" target="_blank">Read full article</a></p>

								</div>
							
								<div class="full-publication-footer">
									<div class="close-article">
			  							<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
			  						</div>
		  						</div>

							</div>
							
				    	</div>

		  			</div>
		  		</div>

				<div class="right-column">
					<div class="publication">

						<div class="publication-content">
					    	<strong>Research</strong>
					    	<p class="italic">- Research papers and theses by Modular Management associates.</p>
							
							<!-- 
								data id on button to know wich article to open
							-->

							<span class="cta-button case-button" data-id="research-2">Read more</span>

							<div class="full-publication" id="research-2">
								<div class="pdf-publication">
									<p class="italic">Research</p>
									<p class="bold">Part Number Count is a Useful Indicator for Complexity Cost</p>
									<p class="light">By John Niethammer, Ph.D.</p>

									<p><a href="link-to-pdf.pdf" target="_blank">Download</a> or <a href="link-to-pdf.pdf" target="_blank">Read full article</a></p>

								</div>
							
								<div class="full-publication-footer">
									<div class="close-article">
			  							<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-up.png">
			  						</div>
		  						</div>

							</div>
					    </div>

		  			</div>
				</div>

				<div class="clear"></div>

		  	</div>



		 </div>
	</div>


<?php	

	get_footer();
?>










